import pandas as pd
from helper_func import HelperFunc

class Cluster:
    def __init__(self, clusterName, tickers = [], df_ratios_ann = pd.DataFrame(), df_ratios_qtr = pd.DataFrame()):
        self.clusterName = clusterName
        self.tickers = tickers
        self.df_ratios_ann = df_ratios_ann
        self.df_ratios_qtr = df_ratios_qtr

        self.helper = HelperFunc()

    def add_tickers(self, list_tickers):
        n = len(self.tickers)
        new_n = n + len(list_tickers)

        if new_n == 0:
            return

        list_df_qtr = [self.df_ratios_qtr * n]
        list_df_ann = [self.df_ratios_ann * n]

        #normalise the dates and put the df into a list
        for ticker in list_tickers:
            df_ratios_ann = ticker['ratios_ann']
            df_ratios_ann.index = df_ratios_ann.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'ann'), axis = 1)
            list_df_ann.append(df_ratios_ann)

            df_ratios_qtr = ticker['ratios_qtr']
            df_ratios_qtr.index = df_ratios_qtr.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'qtr'), axis = 1)
            list_df_qtr.append(df_ratios_qtr)

        df_ann_avg = pd.concat(list_df_ann)
        df_qtr_avg = pd.concat(list_df_qtr)
        df_ann_avg = df_ann_avg.groupby(df_ann_avg.index).sum(min_count=1) #sum ignores NaN
        df_qtr_avg = df_qtr_avg.groupby(df_qtr_avg.index).sum(min_count=1)
        df_ann_avg = df_ann_avg.div(new_n)
        df_qtr_avg = df_qtr_avg.div(new_n)

        list_ticker_names = [ticker['ticker'] for ticker in list_tickers]
        self.tickers = self.tickers + list_ticker_names

        self.df_ratios_ann = df_ann_avg
        self.df_ratios_qtr = df_qtr_avg

    def remove_ticker(self, list_tickers):
        pass

    def update_db(self):
        pass

    def recalculate(self):
        #get the latest data for all tickers and update group's ratios
        pass

    #TODO - to string