class Illustrator:
    def __init__(self):
        pass

    # could include another paramater called 'table'
    # and 'list_ratio_names' becomes 'List_of_fields'
    def plot_group(self, group_name, table_name, list_field_names):
        # list_ratio_names
        # table_name

        # fig = plt.figure(figsize=(10, 6))
        fig = plt.figure()

        #len_tickers = len(self.groups[group_name]['tickers']) + 1
        len_ratios = len(list_field_names) 
        x_axes_grid = math.floor(math.sqrt(len_ratios))
        y_axes_grid = str(x_axes_grid + math.ceil((len_ratios - x_axes_grid**2)/x_axes_grid))
        x_axes_grid = str(x_axes_grid)

        dict_axes = {}

        formatter = matplotlib.dates.DateFormatter("%Y-%m-%d")
        locator = matplotlib.dates.YearLocator(base=1, month=1, day=1)
        
        idx = 0
        for ratio in list_field_names:
            idx += 1

            axes_curr = fig.add_subplot(y_axes_grid + x_axes_grid + str(idx)) 
            dict_axes['axes' + y_axes_grid + x_axes_grid + str(idx)] = axes_curr
            
            axes_curr.xaxis.set_major_formatter(formatter)
            axes_curr.xaxis.set_major_locator(locator)
            axes_curr.xaxis.set_tick_params(rotation=45)

            lns = []
            idx_ticker = 0
            for ticker in self.groups[group_name]['tickers']:
                idx_ticker += 1

                df_ratios_qtr = self.tickers[ticker][table_name]
                data = df_ratios_qtr[ratio]
                
                p1, = axes_curr.plot(data, label=ticker)
                lns.append(p1)
                
            idx_ticker += 1
            df_ratios_qtr = self.groups[group_name][table_name + '_mean']
            data = df_ratios_qtr[ratio]
            
            p1, = axes_curr.plot(data, label='Group Mean')
            lns.append(p1)
            
            axes_curr.legend(handles=lns, loc='best')

        plt.show()