import sqlite3
import os
import pytest
from pymongo import MongoClient


@pytest.fixture(scope="module") # session
def db_conn():
    data_base_name = 'local_market.db'

    # client = MongoClient('localhost', 27017)
    # self.db = client['TLYS_db']
    # self.IncomeStatement = self.db.IncomeStatement
    # self.BalanceSheet = self.db.BalanceSheet
    # self.CashFlow = self.db.CashFlow

    # with sqlite3.connect(os.path.abspath(data_base_name)) as conn:
    #     yield conn
    #     # conn.close()

    conn = sqlite3.connect(os.path.abspath(data_base_name))
    yield conn
    conn.close()

@pytest.fixture
def cursor(db_conn):
    cursor = db_conn.cursor()
    yield cursor
    db_conn.rollback()

@pytest.fixture
def tdf_create_simple_network(cursor):
    
    sql_stmt = '''
        INSERT INTO test_Element
            (elementId, elementName, balance, periodType, abstract)
        VALUES
            ('A', 'A', 'credit', NULL, 0), ('B', 'B', 'credit', NULL, 0),
            ('C', 'C', 'credit', NULL, 0), ('D', 'D', 'credit', NULL, 0),
            ('E', 'E', 'credit', NULL, 0), ('F', 'F', 'credit', NULL, 0),
            ('G', 'G', 'credit', NULL, 0), ('I', 'I', 'credit', NULL, 0),
            ('J', 'J', 'credit', NULL, 0), ('X', 'X', 'credit', NULL, 0);
    '''
    cursor.execute(sql_stmt)



    """
                G
            C
                F
                    I
        A
                E
            B
                    X
                D
                    J
    """
    sql_stmt = '''
        INSERT INTO test_Network
            (nodeId, networkName, parentNodeId, weight, originalParentNodeId)
        VALUES
            ('test', 'test', NULL, 1, NULL), ('A', 'test', 'test', 1, 'test'),
            ('B', 'test', 'A', 1, 'A'), ('C', 'test', 'A', 1, 'A'), 
            ('D', 'test', 'B', 1, 'B'), ('E', 'test', 'B', 1, 'B'), 
            ('F', 'test', 'C', 1, 'C'), ('G', 'test', 'C', 1, 'C'), 
            ('I', 'test', 'F', 1, 'F'), ('J', 'test', 'D', 1, 'D'), 
            ('X', 'test', 'D', 1, 'D');
    '''
    cursor.execute(sql_stmt)


# https://www.youtube.com/watch?v=DhUpxWjOhME&t=789s
# https://www.youtube.com/watch?v=c9oeoN1AnUM