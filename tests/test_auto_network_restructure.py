from margay.dataloader.dataloaderEdgar import DataloaderEdgar
import pandas as pd
import pytest

def test_old_node_diff_parent(cursor, tdf_create_simple_network):
    """X's parent is reassigned to be a node indicated by the rule, while X is the leaf node.
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('X', 'test', 'G', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    assert list_network_update[0]['update']['parentNodeId'] == 'G'



def test_old_node_diff_parent_2(cursor, tdf_create_simple_network):
    """D's parent is reassigned to be a node indicated by the rule, while D has children.
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('D', 'test', 'G', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    assert list_network_update[0]['update']['parentNodeId'] == 'G'


def test_old_node_contridicting_parents(cursor, tdf_create_simple_network):
    """There are 2 contridicting rules (X has parent of G and F, same level), and there is another rule
    that solidifies the position of F in the network, so G should be restructured.

    Outcome: X is restructured to be child of G, and G is restructured to be child of F.
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('X', 'test', 'G', 1),
            ('X', 'test', 'F', 1),
            ('F', 'test', 'C', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    print(list_network_update)

    assert list_network_update[0]['update']['parentNodeId'] == 'A'



def test_old_node_contridicting_parents_2(cursor, tdf_create_simple_network):
    """There are 2 contridicting rules (X has parent of G and F, same level), and there is another rule
    that solidifies the position of F in the network, so G should be restructured.

    Outcome: A <- G <- C <- F <- X 
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('X', 'test', 'F', 1),
            ('X', 'test', 'G', 1),
            ('F', 'test', 'C', 1);
            ('G', 'test', 'A', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    print(list_network_update)

    assert list_network_update[0]['update']['parentNodeId'] == 'A'


@pytest.mark.skip(reason="currently func is not able to deal with this case")
def test_old_node_contridicting_parents_3(cursor, tdf_create_simple_network):
    """There are 2 contridicting rules

    Outcome: ...
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('D', 'test', 'G', 1),
            ('D', 'test', 'F', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    print(list_network_update)

    assert list_network_update[0]['update']['parentNodeId'] == 'A'



@pytest.mark.skip(reason="currently func is not able to deal with this case")
def test_old_node_contridicting_parents_4(cursor, tdf_create_simple_network):
    """There are 3 or more contridicting rules

    Outcome: ...
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('D', 'test', 'G', 1),
            ('D', 'test', 'F', 1);
            ('D', 'test', 'E', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    print(list_network_update)

    assert list_network_update[0]['update']['parentNodeId'] == 'A'


@pytest.mark.skip(reason="currently func is not able to deal with this case")
def test_resulting_network_is_valid(cursor, tdf_create_simple_network):
    """There are 3 or more contridicting rules

    Outcome: ...
    """

    # sql_stmt = '''
    #     INSERT INTO test_NetworkRollUpRule
    #         (nodeId, networkName, parentNodeId, weight)
    #     VALUES
    #         ('D', 'test', 'G', 1),
    #         ('D', 'test', 'F', 1);
    #         ('D', 'test', 'E', 1);
    # '''
    # cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    print(list_network_update)

    assert list_network_update[0]['update']['parentNodeId'] == 'A'




def test_old_node_rule_is_subpath(cursor, tdf_create_simple_network):
    """There is a rule: X has parent of B. Though, X is already part of network with parent D,
    and path of B is sub path of D, so X should keep current parent even though it's not a rule.
    
    Outcome: X's parent is not changed since the parent in rule has sub path of current path
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('X', 'test', 'B', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    print(list_network_update)

    assert len(list_network_update) == 0



def test_old_node_multiple_rules(cursor, tdf_create_simple_network):
    """X's parent is not changed since the parent in rule has sub path of current path
    """

    sql_stmt = '''
        INSERT INTO test_NetworkRollUpRule
            (nodeId, networkName, parentNodeId, weight)
        VALUES
            ('X', 'test', 'C', 1),
            ('X', 'test', 'G', 1);
    '''
    cursor.execute(sql_stmt)


    dl_edgar = DataloaderEdgar(cursor.connection, None)

    network_name = 'test'
    df = dl_edgar.get_rule_node_path(network_name)
    # breakpoint()

    _, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)

    print(list_network_update)

    assert list_network_update[0]['update']['parentNodeId'] == 'G'






"""
- What happens when the node is not in the network?
- What happens when the parent node within the rule is not in the network?
- What happens when there are two rules with differnet parent nodes, 
where the parents are of the same level?
- 
"""


    




# def test_test_two():
#     dl_edgar = DataloaderEdgar()

#     dl_edgar.automatically_restructure_network()


def test_sub_path():
    dl_edgar = DataloaderEdgar(None, None)
    result = dl_edgar.is_subpath(None, 'a/b', ['a/b/c'])
    assert result == True