# from edgar.stock import Stock
# from edgar.financials import FinancialReportEncoder
# import json
# import pathlib


# def testSecEdgarFinancials(ticker, year):
#     stock = Stock(ticker)
#     period = 'annual'
#     #year = year
#     filing = stock.get_filing(period, year)
#     income_statements = filing.get_income_statements()
#     balance_sheets = filing.get_balance_sheets()
#     cash_flows = filing.get_cash_flows()


#     pathlib.Path('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/').mkdir(parents=True, exist_ok=True) 


#     with open('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/income_statements.json', 'w') as json_file:
#         json.dump(json.loads(FinancialReportEncoder().encode(income_statements)), json_file)

#     with open('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/balance_sheets.json', 'w') as json_file:
#         json.dump(json.loads(FinancialReportEncoder().encode(balance_sheets)), json_file)

#     with open('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/cash_flows.json', 'w') as json_file:
#         json.dump(json.loads(FinancialReportEncoder().encode(cash_flows)), json_file)

# testSecEdgarFinancials('DBX', 2019)


import sys
sys.path.append("..")  

from src.connectEdgar.stock import Stock
from src.connectEdgar.financials import FinancialReportEncoder
import json
import pathlib

def testSecEdgarFinancials(ticker, year):

    stock = Stock(ticker)
    period = 'annual'
    #year = year
    filing = stock.get_filing(period, year)
    income_statements = filing.get_income_statements()
    balance_sheets = filing.get_balance_sheets()
    cash_flows = filing.get_cash_flows()

    filePath = './scripts/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/'
    pathlib.Path(filePath).mkdir(parents=True, exist_ok=True) 


    with open(filePath + 'income_statements.json', 'w') as json_file:
        json.dump(json.loads(FinancialReportEncoder().encode(income_statements)), json_file)

    with open(filePath + 'balance_sheets.json', 'w') as json_file:
        json.dump(json.loads(FinancialReportEncoder().encode(balance_sheets)), json_file)

    with open(filePath + 'cash_flows.json', 'w') as json_file:
        json.dump(json.loads(FinancialReportEncoder().encode(cash_flows)), json_file)




testSecEdgarFinancials('DBX', 2020)
