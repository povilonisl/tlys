""" 
Get the Linkbase XML file from ./data
Traverse the links, creating a dict, where children are nested.

Output the dict as JSON
Add the nodes referencing the parents to StatementElement Table.
"""

from bs4 import BeautifulSoup
import json
import pathlib
import re

# import sys
# sys.path.append("..")  
from margay.dataloader import dataloaderDB, dataloaderEdgar

dlDB = dataloaderDB()



dataDirPath = './data/us-gaap-2020-01-03/'
scriptsDataDirPath = './scripts/data/us-gaap-2020-01-03/stm/'

ADDTODB = True
ROOTELEMENT = 'sfp-cls-cal' # sfp-cls-cal    soi-cal       scf-indir-cal
FILENAME = 'us-gaap-stm-' + ROOTELEMENT + '-2020-01-31'
arcType = ROOTELEMENT.split('-')[-1]
if arcType == 'pre':
    arcType = 'presentationArc'
elif arcType == 'cal':
    arcType = 'calculationArc'


def parseLinkBase(dict, currArc, soup, dictLinkLoc):

    # loc_ -> us-gaap_    
    idCurrFrom = dictLinkLoc[currArc['xlink:from']]['elementId']
    idCurrTo = dictLinkLoc[currArc['xlink:to']]['elementId']
    # name = dictLinkLoc[currArc['xlink:to']]['elementName']
    # balance = dictLinkLoc[currArc['xlink:to']]['balance']

    if idCurrFrom  not in dict:
        dict[idCurrFrom] = {}

    if ADDTODB:
        # insertIntoDB({
        #     "elementId": idCurrTo,
        #     'elementName': name,
        #     'parentElementId': idCurrFrom, 
        #     'balance': balance})
        # update_element_record(idCurrTo, {'parentElementId': idCurrFrom})
        insertIntoDB('Network', {
            "nodeId": idCurrTo, 
            'parentNodeId': idCurrFrom,
            'originalParentNodeId': idCurrFrom,
            'networkName': ROOTELEMENT, 
            'elementId': idCurrTo
        })


    count = 0
    listArc = soup.find_all(f'link:{arcType}', attrs={"xlink:from" : currArc['xlink:to']})
    if len(listArc) > 0:
        for arc in listArc:
            idFrom = dictLinkLoc[arc['xlink:from']]['elementId']
            idTo = dictLinkLoc[arc['xlink:to']]['elementId']
            # balance = dictLinkLoc[arc['xlink:to']]['balance']
            if ADDTODB:
                # dlDB.sql_insert(f"""INSERT INTO StatementElement (element, parent_element, ordinal) VALUES ("{idTo}", "{idFrom}", {count});""", False)
                # insertIntoDB({"element": idTo, 'parentElement': idFrom, 'ordinal': count, 'balance': balance})
                count += 1
            parseLinkBase(dict[idCurrFrom], arc, soup, dictLinkLoc)
    else:  
        dict[idCurrFrom][idCurrTo] = {}



def insertIntoDB(table_name, newRow):
    """Add a new row into StatementElement
    TODO: Is the insert SQL INJECTION SAFE????!!!!!

    Args:
        newRow (dict): Id is the field and value is the value of the field.
    """
    # if original:
    #     newRow['hasOriginalParent'] = 1
    #     newRow['originalParentElementId'] = newRow['parentElementId']
    # else:
    #     newRow['hasOriginalParent'] = 0

    sqlStatement = f'INSERT INTO {table_name} '
    sqlFields = '('
    sqlValues = '('

    for field in newRow:
        sqlFields += field + ', '

        if type(newRow[field]) is str:
            sqlValues += f'"{newRow[field]}", '
        elif newRow[field] is None:
            sqlValues += 'null, '
        else:
            sqlValues += f'{newRow[field]}, '

    sqlFields = sqlFields[:-2] + ')'
    sqlValues = sqlValues[:-2] + ')'
    sqlStatement += sqlFields + ' VALUES ' + sqlValues + ';'

    # print(sqlStatement)
    dlDB.sql_execute_statement(sqlStatement, False)
    #conn.execute("INSERT INTO " + dbutils.table + " (PkgID, Package, PkgParentID) VALUES (?,?,?)", (PkgID, Package, PkgParentID))


def update_element_record(elementId, update, original = True):
    # Need to change originalParentElementId, hasOriginalParent, parentElementId 

    if original:
        update['hasOriginalParent'] = 1
        update['originalParentElementId'] = elementId
    else:
        update['hasOriginalParent'] = 0
        update['originalParentElementId'] = None

    sqlStatement = 'UPDATE StatementElement SET'
    sql_update = ''
    whereClause = f'WHERE elementId = "{elementId}"'

    for field in update:
        sql_update += f'{field} = '
        if type(update[field]) is str:
            sql_update += f'"{update[field]}", '
        elif update[field] is None:
            sql_update += 'null, '
        else:
            sql_update += f'{update[field]}, '

    sql_update = sql_update[:-2] 
        
    sqlStatement += ' ' + sql_update + ' ' + whereClause + ';'

    dlDB.sql_execute_statement(sqlStatement, False)

    


def parseStatementXMLtoJSON(): 
    # Open Linkbase file which basically contains edges between all nodes of the statement.
    with open(dataDirPath + 'stm/' + FILENAME + '.xml', 'r') as xml_file:
        soup = BeautifulSoup(xml_file, 'lxml-xml')
    
        # df_outcast = dlDB.df_outcast_element
        # list_accepted_outcast = []


        # create a map from id with loc_  prefix to gaap_ prefix
        dictLinkLoc = {}
        soupLinkLoc = soup.find_all('link:loc')
        for item in soupLinkLoc:
            label = item.get('xlink:label')
            fileloc, elementId = item.get('xlink:href').split('#')
            dictLinkLoc[label] = {'fileloc': fileloc, 'label': label, 'elementId': elementId, 'balance': None}


 
        # Open Element file which contains information about each Element
        # Get the balance info and store it in dict
        # with open(dataDirPath + 'elts/us-gaap-2020-01-31.xsd', 'r') as elementFile:
        #     soupElement = BeautifulSoup(elementFile, 'lxml-xml')
        #     listElement = soupElement.find_all('xs:element')

        #     for element in listElement:
        #         name = element.get('name')
        #         elementLoc = 'loc_' + name
        #         if name:
        #             dictLinkLoc[elementLoc] = {'elementId': element.get('id'), 'elementName': name, 'balance': element.get('xbrli:balance')}

        setTo = set()
        setFrom = set()
        count = 0
        dictNetwork = {}
        soupLinkArk = soup.find_all(f'link:{arcType}')

        # create a set of ids that have parents.
        for linkArc in soupLinkArk:
            idTo = dictLinkLoc[linkArc['xlink:to']]['elementId']
            setTo.add(idTo)

        # for each node that doesn't have a parent: find it, and then traverse down the leaves.
        for linkArc in soupLinkArk:
            idFrom = dictLinkLoc[linkArc['xlink:from']]['elementId']
            # if it's one of the roots.
            if idFrom not in setTo:
                # if it's a root that we haven't seen before then add it to DB.
                if idFrom not in setFrom and ADDTODB:  
                    # balance = dictLinkLoc[linkArc['xlink:from']]['balance']
                    # elementName = dictLinkLoc[linkArc['xlink:from']]['elementName']
                    
                    insertIntoDB('Network', {
                        "nodeId": idFrom, 
                        'parentNodeId': ROOTELEMENT,
                        'originalParentNodeId': ROOTELEMENT,
                        'networkName': ROOTELEMENT, 
                        'elementId': idFrom,
                        'ordinal': count
                    })

                    # update_element_record(idFrom, {'parentElementId': ROOTELEMENT, 'ordinal': count})
                    # insertIntoDB({"elementId": idFrom, 'elementName': elementName, 'parentElementId': ROOTELEMENT, 'ordinal': count, 'balance': balance})
                    count += 1
                    setFrom.add(idFrom)
                # traverse downwards recursively.
                parseLinkBase(dictNetwork, linkArc, soup, dictLinkLoc)
        
        # dlDB.commit_changes()

        # Export the network to JSON file.
        pathlib.Path(scriptsDataDirPath).mkdir(parents=True, exist_ok=True) 
        with open(scriptsDataDirPath + FILENAME + '.json', 'w') as json_file:
            json.dump(dictNetwork, json_file)


        # print(dictLinkLoc)
        # print(setTo)
        print(setFrom)
        # print(dictNetwork)

def inital_populate_outcast_network():
    # Open Element file which contains information about each Element
    # Get the balance info and store it in dict
    with open(dataDirPath + 'elts/us-gaap-2020-01-31.xsd', 'r') as elementFile:
        soupElement = BeautifulSoup(elementFile, 'lxml-xml')
        listElement = soupElement.find_all('xs:element')

        for element in listElement:
            name = element.get('name')
            if name:
                insertIntoDB('Element',{
                    "elementId": element.get('id'), 
                    'elementName': name, 
                    'balance': element.get('xbrli:balance')
                })



# inital_populate_outcast_network()
insertIntoDB('Network', {"nodeId": ROOTELEMENT, 'networkName': ROOTELEMENT, 'elementId': ROOTELEMENT})
parseStatementXMLtoJSON()
dlDB.commit_changes()


dl_edgar = dataloaderEdgar()
# rollup network
with open(dataDirPath + 'stm/' + FILENAME + '.xml', 'r') as xml_file:
    soup = BeautifulSoup(xml_file, 'html.parser')
    print(len(soup.find_all('link:loc')))
    dl_edgar._insert_network_rules_to_DB_from_soup(soup, ROOTELEMENT, 'us-gaap-2020-01-03/stm/us-gaap-stm-' + ROOTELEMENT + '-2020-01-31.xml')

