'''
Outline:
    Iterate over every node in network, starting with root. 
    At each node, get all of the current children, also get all rules where the current node is a child of rule.



Input: 
    Rules:
        Node    Parent  RulePath    CurrPath
        E       B       A/B/D/E     A/B/D/E
        D       C       A/C/D       A/B/D
'''


import sys
sys.path.append('.')

from margay.visualise import Visualise
from margay.dataloader.dataloaderDB import DataloaderDB

import pandas as pd
import igraph
import plotly.graph_objects as go




list_rule = [
    ('A', 'D'),
    ('D', 'E'),
    ('E', 'F'),
    ('A', 'B'),
    ('B', 'C'),
    ('E', 'C'),
    ('F', 'C'),
]

df_rule = pd.DataFrame(list_rule, columns=['parentNodeId', 'nodeId'])

vis =  Visualise()
g = vis.create_igraph(df_rule)




# dict_node = {}
# # for rule in df_rule.itertuples():
# for index, row in df_rule.iterrows():
#     if row['nodeId'] in dict_node:
#         dict_node[row['nodeId']].append(row['parentNodeId'])
#     else:
#         dict_node[row['nodeId']] = [row['parentNodeId']]

#     if row['parentNodeId'] not in dict_node:
#         dict_node[row['parentNodeId']] = []
        

# g = igraph.Graph(directed=True)
# # g.add_vertices(list(dict_node.keys()))



# def is_sub_path(parent_info, path):
#     for other_parent_v_name in list(parent_info):
#         other_parent_path = parent_info[other_parent_v_name]

#         set_other_path = set(other_parent_path)
#         set_path = set(path)

#         if set_path.issubset(set_other_path):
#             return True
#         elif set_other_path.issubset(path):
#             del parent_info[other_parent_v_name]
        
#     return False

# def add_to_graph(v_name, dict_path):
#     list_v_name = []
#     set_v_name = set()
#     if len(g.vs) > 0:
#         list_v_name = g.vs['name']
#         set_v_name = set(list_v_name)

#     # if the vertex alrady exists then it has been created and connected
#     if v_name in set_v_name:
#         return

#     # get parent vertex ids
#     list_parent_v_name = dict_node[v_name]

#     # if we have not created parent vertecies yet then create them first
#     set_new_v_name = set(list_parent_v_name).difference(set_v_name)
#     for new_v_name in set_new_v_name:
#         add_to_graph(new_v_name, dict_path)


#     g.add_vertices(v_name)
#     if len(list_parent_v_name) == 0:
#         dict_path[v_name] = []
#         return

#     # iterate over parent vertecies and remove them if they are a sub path of another vertex
#     # as we do not have to worry about them anymore
#     # Also, identify which vertex has the longest path
#     parent_info = {}
#     l_p_v_name = None
#     for parent_v_name in list_parent_v_name:
#         parent_path = dict_path[parent_v_name]

#         # check if vertex is a sub path
#         # the function will also check if another path is a sub path of 'parent_path' and remove it from dictionary
#         if is_sub_path(parent_info, parent_path):
#             continue

#         if (
#             (l_p_v_name == None) or 
#             (not l_p_v_name in parent_info) or 
#             (len(parent_path) > len(parent_info[l_p_v_name]))
#         ):
#             # vertex qualifies to be counted as the vertex with longest path
#             l_p_v_name = parent_v_name

#         # vertex is not a sub path so add it to dictionary
#         parent_info[parent_v_name] = parent_path
    
#     # list parent names that are not sub-vertecies of other parent verticies 
#     for parent_v_name in list(parent_info):
#         if l_p_v_name == parent_v_name or not parent_v_name in parent_info:
#             continue

#         # identify where the longest path and iterated parent path intersect
#         joining_v_id = None
#         joining_path_idx = -1
#         for temp_v_id in parent_info[parent_v_name]:
#             joining_v_id = temp_v_id
#             joining_path_idx += 1

#             if temp_v_id in parent_info[l_p_v_name]:
#                 break

#         # get the first vertex that diverges from the longest path
#         first_v_id = parent_info[parent_v_name][joining_path_idx - 1]

#         # delete the first edge that diverges from longest path
#         g.delete_edges([(first_v_id, joining_v_id)])
        
#         # create a new edge between first diverging vertex and last vertex of longest path
#         g.add_edges([(first_v_id, l_p_v_name)])
        
#         # update our dictionary that tracks the paths
#         parent_info[parent_v_name] = parent_info[l_p_v_name] + parent_info[parent_v_name][joining_path_idx + 1: ]
#         del parent_info[l_p_v_name]  
#         l_p_v_name = parent_v_name
        
    
#     g.add_edges([(v_name, l_p_v_name)])
#     # keep track of paths to the root from each node
#     dict_path[v_name] = g.subcomponent(g.vs.find(name=v_name), mode="out")
    


        
    
    
# dict_path = {}
# for v_name in dict_node.keys():
#     add_to_graph(v_name, dict_path)













# df_edge = df_rule[['nodeId', 'parentNodeId']]

# g = igraph.Graph.DataFrame(df_edge, directed=True)

for v in g.vs:
    v['label'] = v['name']

# Visualise the graph
layout = g.layout_reingold_tilford(mode="in", root=[0])
layout.mirror(1)

x_node, y_node = list(zip(*layout.coords))
labels = [v['label'] for v in g.vs]

x_network_edge = []
y_network_edge = []
set_values = []
calc_values = []


for e in g.es:
    edge = e.tuple
    x_network_edge += [layout.coords[edge[0]][0], layout.coords[edge[1]][0], None]
    y_network_edge += [layout.coords[edge[0]][1], layout.coords[edge[1]][1], None]


def get_annotations(pos, labels, set_values, calc_values, font_size=18, font_color='rgb(0,0,0)'):
    L=len(pos)
    if len(labels)!=L:
        raise ValueError('The lists pos and text must have the same len')
    annotations = []

    for k in range(L):
        text = (labels[k] if len(labels[k]) < 40 else labels[k][:40] + '...')


        annotations.append(
            dict(
                text= text, # or replace labels with a different list for the text within the circle
                x=pos[k][0], y=pos[k][1]-0.05,
                xref='x1', yref='y1',
                font=dict(color=font_color, size=font_size),
                showarrow=False)
        )
    return annotations



fig = go.Figure()
fig.add_trace(go.Scatter(
    x=x_network_edge,
    y=y_network_edge,
    mode='lines',
    line=dict(color='rgb(210,210,210)', width=3),
    hoverinfo='none'
))


fig.add_trace(go.Scatter(
    x=x_node,
    y=y_node,
    mode='markers',
    name='Element',
    marker=dict(
        symbol='circle-dot',
        size=18,
        color='#6175c1',    #'#DB4551',
        line=dict(color='rgb(50,50,50)', width=1)
    ),
    text=labels,
    hoverinfo='text',
    opacity=0.8,
))

fig.update_layout(
    title = 'Tree....',
    annotations = get_annotations(layout.coords, labels, set_values, calc_values)
)

fig.show()