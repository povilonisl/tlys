import sys

sys.path.append('.')

from margay.dataloader_old import DataloaderEdgar

dl_edgar = DataloaderEdgar()
dl_edgar.edgar_into_db('TSLA', 'IncomeStatement', form_type = '10-K', year=2020)