
# https://github.com/jadchaar/sec-edgar-downloader
from sec_edgar_downloader import Downloader
def secEdgarDownloader():
    dl = Downloader("./TestSecEdgarDownloaded/DBX/")
    dl.get("10-K", "DBX")
#secEdgarDownloader()

from edgar.stock import Stock
from edgar.financials import FinancialReportEncoder
import json
import pathlib


def testSecEdgarFinancials(ticker, year):
    stock = Stock(ticker)
    period = 'annual'
    #year = year
    filing = stock.get_filing(period, year)
    income_statements = filing.get_income_statements()
    balance_sheets = filing.get_balance_sheets()
    cash_flows = filing.get_cash_flows()


    pathlib.Path('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/').mkdir(parents=True, exist_ok=True) 


    with open('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/income_statements.json', 'w') as json_file:
        json.dump(json.loads(FinancialReportEncoder().encode(income_statements)), json_file)

    with open('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/balance_sheets.json', 'w') as json_file:
        json.dump(json.loads(FinancialReportEncoder().encode(balance_sheets)), json_file)

    with open('./Tests/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/cash_flows.json', 'w') as json_file:
        json.dump(json.loads(FinancialReportEncoder().encode(cash_flows)), json_file)

testSecEdgarFinancials('DBX', 2019)

import edgar
# edgar.download_index('./TestPythonEdgar/', 2020, skip_all_present_except_last=False)


# print('filing')
# print(filing)
# print('')
# print('income_statements')
# print(income_statements)
# print('')
# print('balance_sheets')
# print(balance_sheets)
# print('')
# print('cash_flows')
# print(cash_flows)
# print('')