

from margay.connectEdgar.stock import Stock
from margay.connectEdgar.financials import FinancialReportEncoder
from margay.connectEdgar.filing import Filing


from margay.index_edgar_filing import download_index

import json
import pathlib
from margay.dataloader_old import DataloaderDB
from margay.dataloader_old import DataloaderEdgar
from margay.dataloader.dataloaderEdgar import DataloaderEdgar


from datetime import timedelta, date, datetime
from pympler.tracker import SummaryTracker


dl_edgar = DataloaderEdgar()

dl_db = DataloaderDB()

# I think stack overflow helped.
def is_subpath(node, path, list_path):
    return any(path in y and path != y for y in list_path)


def automatically_restructure_network(df):
    # -----------------------------------------------
    # -- Logic to insert new node into the network --
    # -----------------------------------------------
    df_new_node = df[df['ruleNodePath'].isnull()] # node not in network
    df_new_node_unique = df_new_node[~df_new_node['ruleNodeId'].duplicated()]
    list_new_network_node = []
    for index, row in df_new_node_unique.iterrows():
        df_best_path = df_new_node[df_new_node['ruleNodeId'] == row['ruleNodeId']]
        
        # filtering out rules whoes path is a subset of another rule's path.
        # df_best_path['isSubpath'] = [is_subpath(x, df_best_path['ruleParentNodePath']) for x in df_best_path['ruleParentNodePath']]
        df_best_path = df_best_path[df_best_path['isSubpath'] == False] 
        
        if df_best_path.shape[0] != 1: # TODO: I think this is completely possible and needs to be dealt with.
            raise Exception(f'Insertion:Network: {network_name}. I am not able to deal with multiple best paths \n{df_best_path}')

        list_new_network_node.append({
            'nodeId': df_best_path.iloc[0]['ruleNodeId'],
            'parentNodeId': df_best_path.iloc[0]['ruleParentNodeId'],
            'networkName': network_name,
            'weight': df_best_path.iloc[0]['ruleWeight'],
            'originalParentNodeId': None
        })
    dl_db.var_insert('test_Network', list_new_network_node, commit = True) #TODO - does this satisfy other rules??



    # ------------------------------------------------
    # -- Logic to update parent of an existing node --
    # ------------------------------------------------
    # Is there a reason why we need to seperate insertions and updates?
    df_update_node = df[df['ruleNodePath'].notnull()] # node in network
    df_update_node_unique = df_update_node[~df_update_node['ruleNodeId'].duplicated()]
    list_update_network_node = []
    for index, row in df_update_node_unique.iterrows():
        df_best_path = df_update_node[df_update_node['ruleNodeId'] == row['ruleNodeId']]
        
        # get the longest path, or the rule where ruleParentNodeId is the closest to the ruleNodeId in terms of hierarchy
        df_best_path = df_best_path[df_best_path['isSubpath'] == False] 

        if df_best_path.shape[0] != 1: # TODO: I think this is completely possible and needs to be dealt with.
            raise Exception(f'Update:Network: {network_name}. I am not able to deal with multiple best paths \n{df_best_path}')

        # update the database with the new parent
        sqlStatement = f'''
                UPDATE test_Network
                SET 
                    parentNodeId = "{df_best_path.iloc[0]['ruleParentNodeId']}",
                    ordinal = "{0}"
                WHERE
                    nodeId = "{df_best_path.iloc[0]['ruleNodeId']}" and
                    networkName = "{network_name}";
            '''
        dl_db.sql_execute_statement(sqlStatement, commit=False)
    dl_db.commit_changes()




"""
    (new) dl_db.var_insert(self, updates, commit)
        updates:[{
            table: String
            values: {field: value} 
            where: {field: value}
        }]

"""















# network_name = 'sfp-cls-cal'
# network_name = 'scf-indir-cal'
network_name = 'test'
sql_statement = f'''
WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, weight, path) as (
    SELECT nodeId, parentNodeId, originalParentNodeId, weight, nodeId FROM test_Network WHERE parentNodeId = '{network_name}' and networkName = '{network_name}'
    
    UNION
    
    SELECT test_Network.nodeId, test_Network.parentNodeId, test_Network.originalParentNodeId, test_Network.weight, paths.path || '/' || test_Network.nodeId
    FROM test_Network 
    JOIN paths
    WHERE test_Network.parentNodeId = paths.nodeId and networkName = '{network_name}'
)
SELECT 
    test_NetworkRollUpRule.nodeId ruleNodeId, 
    test_NetworkRollUpRule.parentNodeId ruleParentNodeId,
    test_NetworkRollUpRule.weight ruleWeight,
    nPath.parentNodeId as networkParentNodeId,
    pnPath.path as networkParentNodePath,
    nPath.path ruleNodePath,
    rpnPath.path ruleParentNodePath
FROM test_NetworkRollUpRule
LEFT JOIN paths nPath
ON nPath.nodeId = test_NetworkRollUpRule.nodeId
LEFT JOIN paths pnPath
ON nPath.parentNodeId = pnPath.nodeId
LEFT JOIN paths rpnPath
ON rpnPath.nodeId = test_NetworkRollUpRule.parentNodeId
WHERE 
    test_NetworkRollUpRule.networkName == '{network_name}'
'''
df = dl_db.sql_select(sql_statement, None)

# determine if parent-path of each rule is a subset parent-path of another rule for that particular ruleNodeId. 
df['isSubpath'] = [
    is_subpath(ruleNodeId, ruleParentNodePath, df['ruleParentNodePath'].loc[df['ruleNodeId'] == ruleNodeId]) 
    for ruleNodeId, ruleParentNodePath in zip(df['ruleNodeId'], df['ruleParentNodePath'])
]

















# --------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------
# ------------------------ This uses Objects with real data ----------------------------
# --------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------


# network_name = 'sfp-cls-cal'
# network_name = 'scf-indir-cal'
network_name = 'soi-cal'
sql_statement = f'''
WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, weight, path) as (
    SELECT nodeId, parentNodeId, originalParentNodeId, weight, nodeId FROM Network WHERE parentNodeId = '{network_name}' and networkName = '{network_name}'
    
    UNION
    
    SELECT Network.nodeId, Network.parentNodeId, Network.originalParentNodeId, Network.weight, paths.path || '/' || Network.nodeId
    FROM Network 
    JOIN paths
    WHERE Network.parentNodeId = paths.nodeId and networkName = '{network_name}'
)
SELECT 
    NetworkRollUpRule.nodeId ruleNodeId, 
    NetworkRollUpRule.parentNodeId ruleParentNodeId,
    NetworkRollUpRule.weight ruleWeight,
    nPath.parentNodeId as networkParentNodeId,
    pnPath.path as networkParentNodePath,
    nPath.path ruleNodePath,
    rpnPath.path ruleParentNodePath
FROM NetworkRollUpRule
LEFT JOIN paths nPath
ON nPath.nodeId = NetworkRollUpRule.nodeId
LEFT JOIN paths pnPath
ON nPath.parentNodeId = pnPath.nodeId
LEFT JOIN paths rpnPath
ON rpnPath.nodeId = NetworkRollUpRule.parentNodeId
WHERE 
    NetworkRollUpRule.networkName == '{network_name}'
'''
df = dl_db.sql_select(sql_statement, None)

# df =  pd.read_sql_query(sql_statement, self.dl_db)

df['isSubpath'] = [
    ruleParentNodePath in networkParentNodePath or
    is_subpath(ruleNodeId, ruleParentNodePath, df['ruleParentNodePath'].loc[df['ruleNodeId'] == ruleNodeId]) 
    for ruleNodeId, ruleParentNodePath, networkParentNodePath in zip(df['ruleNodeId'], df['ruleParentNodePath'], df['networkParentNodePath'])
]

dl_edgar = DataloaderEdgar(None, None)
list_network_insert, list_network_update = dl_edgar.automatically_restructure_network(df, network_name)









# --------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------



























dl_edgar.review_network_structure('soi-cal')


dl_db = DataloaderDB()
df_is = dl_db.df_incomeStatement_element






# insert test data into test_Table
list_element_row = [
    {
        'elementId': 'A',
        'elementName': 'A',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'B',
        'elementName': 'B',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'C',
        'elementName': 'C',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'D',
        'elementName': 'D',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'E',
        'elementName': 'E',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'F',
        'elementName': 'F',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'G',
        'elementName': 'G',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'I',
        'elementName': 'I',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'J',
        'elementName': 'J',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    },{
        'elementId': 'X',
        'elementName': 'X',
        'balance': 'credit',
        'periodType': None,
        'abstract': False
    }
]

list_temp_element_row = [
 
]
dl_db.var_insert('test_Element', list_element_row, False)


list_network_row = [
    {
        'nodeId': 'test',
        'networkName': 'test',
        'parentNodeId': None,
        'weight': 1,
        'originalParentNodeId': None
    },
    {
        'nodeId': 'A',
        'networkName': 'test',
        'parentNodeId': 'test',
        'weight': 1,
        'originalParentNodeId': 'test'
    },{
        'nodeId': 'B',
        'networkName': 'test',
        'parentNodeId': 'A',
        'weight': 1,
        'originalParentNodeId': 'A'
    },{
        'nodeId': 'C',
        'networkName': 'test',
        'parentNodeId': 'A',
        'weight': 1,
        'originalParentNodeId': 'A'

    },{
        'nodeId': 'D',
        'networkName': 'test',
        'parentNodeId': 'B',
        'weight': 1,
        'originalParentNodeId': 'B'

    },{
        'nodeId': 'E',
        'networkName': 'test',
        'parentNodeId': 'B',
        'weight': 1,
        'originalParentNodeId': 'B'

    },{
        'nodeId': 'F',
        'networkName': 'test',
        'parentNodeId': 'C',
        'weight': 1,
        'originalParentNodeId': 'C'

    },{
        'nodeId': 'G',
        'networkName': 'test',
        'parentNodeId': 'C',
        'weight': 1,
        'originalParentNodeId': 'C'

    },
    {
        'nodeId': 'I',
        'networkName': 'test',
        'parentNodeId': 'F',
        'weight': 1,
        'originalParentNodeId': 'F'

    },{
        'nodeId': 'J',
        'networkName': 'test',
        'parentNodeId': 'D',
        'weight': 1,
        'originalParentNodeId': 'D'
    },{
        'nodeId': 'X',
        'networkName': 'test',
        'parentNodeId': 'D',
        'weight': 1,
        'originalParentNodeId': 'D'
    }
]

list_temp_network_row = [
    
]
dl_db.var_insert('test_Network', list_network_row, False)


list_network_rule_row = [
    {
        'nodeId': 'H',
        'networkName': 'test',
        'parentNodeId': 'F',
        'weight': 1
    },
    {
        'nodeId': 'H',
        'networkName': 'test',
        'parentNodeId': 'C',
        'weight': 1
    },
    {
        'nodeId': 'H',
        'networkName': 'test',
        'parentNodeId': 'I',
        'weight': 1
    },{
        'nodeId': 'J',
        'networkName': 'test',
        'parentNodeId': 'E',
        'weight': 1
    }
]

list_temp_network_rule_row = [
    {
        'nodeId': 'X',
        'networkName': 'test',
        'parentNodeId': 'G',
        'weight': 1
    },{
        'nodeId': 'X',
        'networkName': 'test',
        'parentNodeId': 'F',
        'weight': 1
    }
]
dl_db.var_insert('test_NetworkRollUpRule', list_temp_network_rule_row, False)

dl_db.commit_changes()
































dl_edgar.edgar_into_db('MSFT', 'IncomeStatement', form_type = '10-K', year=2020)


df_filing = dl_edgar._get_df_sec_filing(ticker = 'DBX', year=2021)
filing = Filing(company='DBX', url= './scripts/data/sec_edgar_filings/0001467623-21-000012.txt', running_test=True)
# soup_calc_arc = filing.documents['dbx-20201231_cal.xml'].doc_text.xbrl
soup_calc_arc = filing.get_filing_cal_network('IncomeStatement')
dl_edgar._insert_network_rules_to_DB_from_soup(soup_calc_arc, 'soi-cal', df_filing['filingFilePath'].iloc[0])

# soup_element_file = filing.documents['dbx-20201231.xsd'].doc_text.xbrl
# list_soup_element = soup_element_file.find_all('xs:element')
# dl_edgar._insert_elements_to_DB_from_soup('DBX', list_soup_element, df_filing['filingFilePath'].iloc[0])




























def main():
    # print('hello world!!')
    # testConnectEdgar()    
    # dl_Edgar()
    # testLastDay(datetime(2005, 5, 14))
    # db_columns()
    # update_network()
    # index_edgar_filing()

    # list_filing = dl_edgar._get_sec_filing(ticker= 'DBX')

    # print(dl_db.get_table_columns('IncomeStatement'))
    
    # dl_db.var_update('Element', {
    #     'non-gaap_GainLossonInducedConversionandDebtExtinguishment': {
    #         'elementId': 'non-gaap_GainLossOnInducedConversionAndDebtExtinguishment',
    #         'elementName': 'GainLossOnInducedConversionAndDebtExtinguishment'
    #     }
    # }, True)
    # dl_db.var_update('Network', {
    #     'non-gaap_GainLossonInducedConversionandDebtExtinguishment': {
    #         'elementId': 'non-gaap_GainLossOnInducedConversionAndDebtExtinguishment',
    #         'nodeId': 'non-gaap_GainLossOnInducedConversionAndDebtExtinguishment'
    #     }
    # }, True)

    # dl_edgar.update_network('us-gaap_NoninterestIncome', 'soi-cal', 'us-gaap_RevenuesExcludingInterestAndDividends', commit=True)

    pass

def dl_Edgar():
    # tracker = SummaryTracker()

    dl_edgar.edgar_into_db('INTC', 'all') 
    # tracker.print_diff()



def balance_sheet_connectEdgar():
    ticker = 'AAPL'
    year = 2020

    filePath = './scripts/data/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/'
    pathlib.Path(filePath).mkdir(parents=True, exist_ok=True)

    filing = getSecFiling(ticker, year)

    # income_statements = filing.get_income_statements()
    # with open(filePath + 'income_statements.json', 'w') as json_file:
    #     json.dump(json.loads(FinancialReportEncoder().encode(income_statements)), json_file)

    balance_sheets = filing.get_balance_sheets()
    with open(filePath + 'balance_sheets.json', 'w') as json_file:
        json.dump(json.loads(FinancialReportEncoder().encode(balance_sheets)), json_file)


def update_network():
    dl_edgar.update_network(
        'us-gaap_CashAndCashEquivalentsAtCarryingValue', 
        'scf-indir-cal', #soi-cal     sfp-cls-cal        scf-indir-cal
        'us-gaap_CashCashEquivalentsRestrictedCashAndRestrictedCashEquivalents', 
        commit=True)

def lsat_day_of_month(dt):
    if date.month == 12:
        return date.replace(day=31)
    return datetime(dt.year, dt.month + 1, 1) - timedelta(days=1)




def getSecFiling(ticker, year):
    df_filing = dl_edgar._get_df_sec_filing(ticker = ticker, year=year)
    filing = Filing(company=ticker, url= 'https://www.sec.gov/Archives/' + df_filing['filingFilePath'].iloc[0])

    return filing



def testConnectEdgar():
    aaplFiling = getSecFiling('AAPL', 2020)
    aaplIncomeStatement = aaplFiling.get_income_statements()
    print('testConnectEdgar:1')
    print(aaplIncomeStatement)
    print('testConnectEdgar:2')
    print(aaplIncomeStatement.reports[0])
    print('testConnectEdgar:3')
    print(aaplIncomeStatement.reports[0].map)


def index_edgar_filing():
    # download_index('./data/index_edgar_filing/', 2010)
    # dl_edgar.get_tickers_and_cik()
    dl_edgar.insert_filing_index_from_edgar(since_year=2014)
    pass
    
def db_columns():
    dl_db.update_schema('CashFlow')


if __name__ == '__main__':
    main()



# PropertyPlantAndEquipmentAndFinanceLeaseRightOfUseAssetAfterAccumulatedDepreciationAndAmortization
# PropertyPlantandEquipmentandFinanceLeaseRightofUseAssetafterAccumulatedDepreciationandAmortization






import itertools


l_1 = ['A', 'B', 'E']
l_2 = ['A', 'B']


for i_1, i_2, i_3 in itertools.zip_longest(l_1, l_2, l_3):
    print(i_1)
    print(i_2)
    print(i_3)

    # ---- CASE: Node not in Network -----
    # how do we compare the rules.
    # do we always choose the longest path? Yes?



