from margay.dataloader import dataloaderDB
import pandas as pd

def _insertIntoDB(row):
    dl_db.var_insert('Element', [{
        'elementId': 'srt_' + row['name'],
        'elementName': row['name'],
        'prefix': 'srt',
        'label': row['label'],
        'balance': row['balance']
    }], False)

dl_db = dataloaderDB()

df = pd.read_csv('./data/srt-2020-01-31/SRT_Taxonomy_2020_Elements.csv', sep=',')
df = df.convert_dtypes()
df = df[(df['prefix'] == 'srt') & (df['abstract'] is not True) & (df['balance'].notna())]


df.apply(lambda x: _insertIntoDB(x), axis=1)
dl_db.commit_changes()
# print(df)