import sys
sys.path.append('.')

from margay.dataloader.dataloaderDB import DataloaderDB
from margay.visualise import Visualise

import pandas as pd
import igraph
import plotly.graph_objects as go
from datetime import datetime, date

import numpy as np


dl_db = DataloaderDB()
dl_db.create_sqlite3_connection()
dl_db.create_mongo_connection()

# print(dl_db.sql3_select('SELECT * FROM Element;', None))

dict_msft2020 = dl_db.mdb_IncomeStatement.find_one(
    {
        'ticker':'TSLA',
        # 'date': '20201231'
        'date': {
            '$gte': '20200101',
            '$lte': '20201231'
        }
    }, 
    {
        '_id': 0,
        'ticker': 0, 
        'date': 0, 
        'filingType': 0, 
        'dataType': 0, 
        'source': 0
    }
)


df_node = pd.DataFrame.from_dict([dict_msft2020])
df_network = dl_db.sql3_select(
    '''
        SELECT 
            Network.nodeId,
            Network.parentNodeId,
            Element.balance
        FROM Network 
        LEFT JOIN Element ON Network.nodeId = Element.elementId 
        WHERE networkName="soi-cal";
    ''', 
    None
)
df_network['in_network'] = '1'


show_rule = True
if show_rule:
    df_rule = dl_db.sql3_select(
        '''
            SELECT 
                Rule.nodeId,
                Rule.parentNodeId,
                Element.balance
            FROM NetworkRollUpRule as Rule 
            LEFT JOIN Element ON Rule.nodeId = Element.elementId 
            WHERE 
                networkName="soi-cal" AND
                Rule.nodeId NOT IN (SELECT nodeId FROM Network WHERE nodeId = Rule.nodeId)
            ;
        ''', 
        None
    )
    df_rule['in_network'] = '0'
else:
    df_rule = pd.DataFrame()

df_network = pd.concat([df_network, df_rule])
df_network = df_network.applymap(lambda x: x.lower() if pd.notnull(x) else x)


df_edge = df_network[['nodeId', 'parentNodeId', 'in_network']]
df_edge = df_edge.drop_duplicates(subset=['nodeId', 'parentNodeId'])
# df_edge.fillna('strNone')



vis =  Visualise()
g = vis.create_igraph(df_edge)
# g = igraph.Graph.DataFrame(df_edge, directed=True)


# s = set(g.subcomponent(g.vs.find(name='us-gaap_CostOfGoodsAndServicesSold'), mode="out"))
# t = set(g.subcomponent(g.vs.find(name='scf-indir-cal'), mode="in"))
# s.intersection(t)

root_node = g.vs.find(name='soi-cal')
# node = g.vs.find(name='us-gaap_investmentincomeinterest')
t = set(g.subcomponent(root_node, mode="in"))
seen_nodes = set()
for key in dict_msft2020:
    balace = df_network.loc[df_network['nodeId'] == key.lower(), 'balance'].iloc[0]
    
    node = g.vs.find(name=key.lower())
    node['balance'] = balace
    if balace == 'credit':
        node['set_value'] = dict_msft2020[key]
    elif balace == 'debit':
        node['set_value'] = -dict_msft2020[key]

    list_parent_vertex = g.subcomponent(node, mode="out")
    s = set(list_parent_vertex)
    seen_nodes = seen_nodes.union(s)


g.delete_vertices(t.difference(seen_nodes))


for v in g.vs:
    v['label'] = v['name'].replace('us-gaap_', '')
    v['calc_value'] = 0


for key in dict_msft2020:
    node = g.vs.find(name=key.lower())
    if not node['set_value']:
        continue

    list_parent_vertex = g.subcomponent(node, mode="out")
    list_child_vertex = g.subcomponent(node, mode="in")
    if len(list_child_vertex) <= 1:
        for idx_vertex in list_parent_vertex:
            g.vs[idx_vertex]['calc_value'] += node['set_value']
            
            # if node['balance'] == 'credit':
            #     g.vs[idx_vertex]['calc_value'] += node['set_value']
            # elif node['balance'] == 'debit':
            #     g.vs[idx_vertex]['calc_value'] -= node['set_value']



## --------- look at sub graph ---------------
# root_node = g.vs.find(name='soi-cal')
# curr_node = set(g.subcomponent(root_node, mode="in"))

# new_root_node = g.vs.find(name='us-gaap_investmentincomeinterest')
# new_node = set(g.subcomponent(new_root_node, mode="out"))

# g.delete_vertices(curr_node.difference(new_node))



# Visualise the graph
layout = g.layout_reingold_tilford(mode="in", root=[0])
layout.mirror(1)

x_node, y_node = list(zip(*layout.coords))
labels = [v['label'] for v in g.vs]
set_values = [v['set_value'] for v in g.vs]
calc_values = [v['calc_value'] for v in g.vs]

x_network_edge = []
y_network_edge = []

x_rull_edge = []
y_rull_edge = []

for e in g.es:
    edge = e.tuple
    # if e['in_network'] == '1':
    x_network_edge += [layout.coords[edge[0]][0], layout.coords[edge[1]][0], None]
    y_network_edge += [layout.coords[edge[0]][1], layout.coords[edge[1]][1], None]
    # else:
    #     x_rull_edge += [layout.coords[edge[0]][0], layout.coords[edge[1]][0], None]
    #     y_rull_edge += [layout.coords[edge[0]][1], layout.coords[edge[1]][1], None]



def get_annotations(pos, labels, set_values, calc_values, font_size=9, font_color='rgb(0,0,0)'):
    L=len(pos)
    if len(labels)!=L:
        raise ValueError('The lists pos and text must have the same len')
    annotations = []

    for k in range(L):
        calc_val_colour = 'green'

        text = (labels[k] if len(labels[k]) < 40 else labels[k][:40] + '...')

        if set_values[k]:
            text += '<br>' + '<span style="font-weight:800">'+ f'{set_values[k]:,}' + '</span>'
            
            if set_values[k] != calc_values[k]:
                calc_val_colour = 'red'
        if calc_values[k]:
            text += '<br>' + f'<span style="color:{calc_val_colour}; font-weight:800">{calc_values[k]:,}</span>'

        annotations.append(
            dict(
                text= text, # or replace labels with a different list for the text within the circle
                x=pos[k][0], y=pos[k][1]-0.35,
                xref='x1', yref='y1',
                font=dict(color=font_color, size=font_size),
                showarrow=False)
        )
    return annotations



fig = go.Figure()
fig.add_trace(go.Scatter(
    x=x_network_edge,
    y=y_network_edge,
    mode='lines',
    line=dict(color='rgb(210,210,210)', width=3),
    hoverinfo='none'
))

fig.add_trace(go.Scatter(
    x=x_rull_edge,
    y=y_rull_edge,
    mode='lines',
    line=dict(color='rgb(210,210,210)', width=3, dash='dash'),
    hoverinfo='none'
))

fig.add_trace(go.Scatter(
    x=x_node,
    y=y_node,
    mode='markers',
    name='Element',
    marker=dict(
        symbol='circle-dot',
        size=18,
        color='#6175c1',    #'#DB4551',
        line=dict(color='rgb(50,50,50)', width=1)
    ),
    text=labels,
    hoverinfo='text',
    opacity=0.8,
))

fig.update_layout(
    title = 'Tree....',
    annotations = get_annotations(layout.coords, labels, set_values, calc_values)
)

fig.show()