import sys

sys.path.append('.')

from margay.dataloader.dataloaderDB import DataloaderDB
from margay.visualise import Visualise

import pandas as pd



dl_db = DataloaderDB()
dl_db.create_sqlite3_connection()
dl_db.create_mongo_connection()

# Retrieve financial Income Statement data for MSFT
dict_msft2020 = dl_db.mdb_IncomeStatement.find_one(
    {'ticker':'MSFT'}, 
    {
        # 0 = means we do not want to retrieve these columns
        # by default all are 1.
        '_id': 0,
        'ticker': 0, 
        'date': 0, 
        'filingType': 0, 
        'dataType': 0, 
        'source': 0
    }
)
# put the financial data into a Dataframe
df_node = pd.DataFrame.from_dict([dict_msft2020])

# retrieve the network structure from sqlite3
df_network = dl_db.sql3_select('SELECT * FROM Network WHERE networkName="soi-cal";', None)

# select the columns of the network we want to look at
df_edge = df_network[['nodeId', 'parentNodeId']]

v = Visualise()
v.display_network(df_edge, dict_msft2020, root_node='soi-cal')
