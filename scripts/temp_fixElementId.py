from margay.dataloader.dataloaderDB import DataloaderDB
dl_db = DataloaderDB()
dl_db.create_sqlite3_connection()
dl_db.create_mongo_connection()



df_element = dl_db.sql3_select(
    '''
        SELECT
            elementId,
            elementName
        FROM
            Element
        WHERE
            Prefix = 'non-gaap';
    ''', 
    None
)


df_rule = dl_db.sql3_select(
    '''
        SELECT 
            Rule.nodeId
        FROM NetworkRollUpRule as Rule 
        WHERE 
            Rule.nodeId NOT IN (
                SELECT elementId FROM Element WHERE Rule.nodeId = elementId
            )
        ;
    ''', 
    None
)


for index, row in df_rule.iterrows():
    prefix, elementName = row['nodeId'].split('_')
    
    print(prefix)
    print(elementName)
    element = df_element[df_element['elementName'] == elementName]
    print(element)
    if len(element) > 0:
        elementId = element.iloc[0]['elementId']

        str_statement = f'''
            UPDATE Element 
            SET elementId = '{row['nodeId']}' 
            WHERE
                elementId = '{elementId}';
        '''

        print(str_statement)

        dl_db.sql3_statement(str_statement)
    print()
    print()
    print()



dl_db.sqlite3_conn.commit()







# ----------------------------------------------------------------------------------------------
# --------------------- Delete elements and rules for some ticker-------------------------------
# ----------------------------------------------------------------------------------------------
ticker = 'tsla'

str_statement = f'''
    DELETE FROM Element
    WHERE  elementId LIKE '{ticker}_%';
'''

str_statement = f'''
    DELETE FROM NetworkRollUpRule
    WHERE  nodeId LIKE '{ticker}_%';
'''


dl_db.sql3_statement(str_statement)
dl_db.sqlite3_conn.commit()

# ----------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------
