import sys

sys.path.append('.')

import sqlite3
from margay.dataloader import dataloaderEdgar
from margay.dataloader.dataloaderDB import DataloaderDB

import igraph

import pandas as pd



# from igraph import Graph, EdgeSeq
import plotly.graph_objects as go

from margay.visualise import Visualise


dl_db = DataloaderDB()
dl_db.create_sqlite3_connection()
dl_db.create_mongo_connection()

# print(dl_db.sql3_select('SELECT * FROM Element;', None))

dict_msft2020 = dl_db.mdb_IncomeStatement.find_one(
    {'ticker':'MSFT'}, 
    {
        '_id': 0,
        'ticker': 0, 
        'date': 0, 
        'filingType': 0, 
        'dataType': 0, 
        'source': 0
    }
)


df_node = pd.DataFrame.from_dict([dict_msft2020])


# df_edge.apply(lambda x: dict_msft2020[x['nodeId']] if x['nodeId'] in dict_msft2020 else None, axis=1 )


df_network = dl_db.sql3_select('SELECT * FROM Network WHERE networkName="soi-cal";', None)

df_edge = df_network[['nodeId', 'parentNodeId']]
# df_edge.replace(
#     {'nodeId': r'us-gaap_', 'parentNodeId': r'us-gaap_'}, 
#     {'nodeId': '', 'parentNodeId': ''}, 
#     regex=True, inplace=True
# )



g = igraph.Graph.DataFrame(df_edge, directed=True)


# s = set(g.subcomponent(g.vs.find(name='us-gaap_CostOfGoodsAndServicesSold'), mode="out"))
# t = set(g.subcomponent(g.vs.find(name='scf-indir-cal'), mode="in"))
# s.intersection(t)

node = g.vs.find(name='soi-cal')
t = set(g.subcomponent(node, mode="in"))
seen_nodes = set()
for key in dict_msft2020:
    node = g.vs.find(name=key)
    node['value'] = dict_msft2020[key]
    
    s = set(g.subcomponent(node, mode="out"))
    seen_nodes = seen_nodes.union(s)

g.delete_vertices(t.difference(seen_nodes))

for v in g.vs:
    v['label'] = v['name'].replace('us-gaap_', '')

layout = g.layout_reingold_tilford(mode="in", root=[0])
layout.mirror(1)
# igraph.plot(g, layout=layout, bbox=(800,800), vertex_label_size=7, )


x_node, y_node = list(zip(*layout.coords))
labels = [v['label'] for v in g.vs]
values = [v['value'] for v in g.vs]

x_edge = []
y_edge = []

for e in g.es:
    edge = e.tuple
    x_edge += [layout.coords[edge[0]][0], layout.coords[edge[1]][0], None]
    y_edge += [layout.coords[edge[0]][1], layout.coords[edge[1]][1], None]
     

def get_annotations(pos, labels, value, font_size=9, font_color='rgb(0,0,0)'):
    L=len(pos)
    if len(labels)!=L:
        raise ValueError('The lists pos and text must have the same len')
    annotations = []

    for k in range(L):
        text = (labels[k] if len(labels[k]) < 40 else labels[k][:40] + '...')
        if value[k]:
            text += '<br>' + '<span style="color:green; font-weight:800">'+ f'{value[k]:,}' + '</span>'

        annotations.append(
            dict(
                text= text, # or replace labels with a different list for the text within the circle
                x=pos[k][0], y=pos[k][1]-0.35,
                xref='x1', yref='y1',
                font=dict(color=font_color, size=font_size),
                showarrow=False)
        )
    return annotations



fig = go.Figure()
fig.add_trace(go.Scatter(x=x_edge,
                   y=y_edge,
                   mode='lines',
                   line=dict(color='rgb(210,210,210)', width=3),
                   hoverinfo='none'
                   ))
fig.add_trace(go.Scatter(x=x_node,
                  y=y_node,
                  mode='markers',
                  name='Element',
                  marker=dict(symbol='circle-dot',
                                size=18,
                                color='#6175c1',    #'#DB4551',
                                line=dict(color='rgb(50,50,50)', width=1)
                                ),
                  text=labels,
                  hoverinfo='text',
                  opacity=0.8,
                  ))

fig.update_layout(
    title = 'Tree....',
    annotations = get_annotations(layout.coords, labels, values)
)

fig.show()