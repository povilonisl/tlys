from bs4 import BeautifulSoup
import json
import pathlib

dataDirPath = './data/'


def parseNetwork(dict, soup):
   
    dict[soup['id']] = {}

    for child in soup.findChildren('div', {'class': 'e'}, recursive=False):
        parseNetwork(dict[soup['id']], child)





with open(dataDirPath + 'IncomeStatementNetwork.html', 'r') as html_file:

    soup = BeautifulSoup(html_file, 'html.parser')

    #incomeStatementAbstract = soup.find(id='us-gaap:IncomeStatementAbstract')
    divIncomeStatement = soup.find(id='added:IncomeStatementLineItems')

    dictIncomeNetwork = {}



    # print(divIncomeStatement['id'])
    # print(len(divIncomeStatement.findChildren('div', {'class': 'e'}, recursive=False)))



    parseNetwork(dictIncomeNetwork, divIncomeStatement.findChildren('div', {'class': 'e'}, recursive=False)[0])


    pathlib.Path(dataDirPath).mkdir(parents=True, exist_ok=True) 

    with open(dataDirPath + 'incomeStatementNetwork.json', 'w') as json_file:
        json.dump(dictIncomeNetwork, json_file)

    # print(len(divIncomeStatement.findChildren('div', recursive=False)))


    #print(incomeStatementAbstract)
    # print(incomeStatementAbstract.prettify())



