from dataloader import DataloaderDB, DataloaderCore, DataloaderAPI, DataloaderFile
from margay import Margay
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates
from helper_func import HelperFunc
from cluster import Cluster
import pandas as pd

from datetime import timedelta, date, datetime


dl_api = DataloaderAPI()
dl_core = DataloaderCore()
dl_db = DataloaderDB()
dl_file = DataloaderFile()

helper = HelperFunc()
#dl_api.alphavantage('IBM', 'INCOME_STATEMENT')
#dl_api.alphavantage('IBM')

m = Margay()


m.set_tickers(['DBX'], True, False)
dl_file.import_from_csv('dbxBalanceSheetAnn.csv', 'BalanceSheetAnn', save_to_db=True)
# dl_file.export_to_csv(BalanceSheetAnn, 'dbxBalanceSheetAnn')


# m.cluster_create('cloudStorage', ['DBX', 'BOX'])

#m.cluster_create('cloudStorage', ['ACN', 'IBM', 'MSFT'])

# SUMO, 



# m.cluster_get_v1('SaaS')
# m.plot_ratios(['SaaS'], ['DBX'], 'RatiosQtr', ['ttm_p/e', 'p/bv', 'roce'])
# m.clusters['cloudStorage']


# m.cluster_get('cloudStorage')
# m.ticker_update_ratios('cloudStorage')
# m.cluster_get('Technology')
# m.ticker_update_ratios('Technology')

# m.set_tickers(['IBM'], True, False)
# m.set_tickers(['ACN'], True, False)
# m.set_tickers(['MSFT'], True, False)
# m.ticker_update_ratios('IBM')
# m.ticker_update_ratios('ACN')
# m.ticker_update_ratios('MSFT')
# m.group_add_tickers('test1', ['IBM', 'ACN', 'MSFT'])
# m.plot_group('test1', 'ratios_qtr', ['ttm_p/e', 'p/bv', 'roce'])

#print(m.tickers['ACN']['TimeSeriesDaily'])
# print('============================================================')
# m.set_tickers(['DBX'], True, False)
# m.set_tickers(['BOX'], True, False)
# m.ticker_update_ratios('DBX')
# m.ticker_update_ratios('BOX')
# m.group_add_tickers('tempCS', ['DBX', 'BOX']) #koofr
# m.plot_group('cloudStorage', 'ratios_qtr', ['ttm_p/e', 'p/bv', 'roce'])




# plot tickers against bunch of clusters
# m.plot(list_clusters, list_tickers, list_ratios)

# # future:
# m.illustrator.add_index(index)
# m.illustrator.set_options()



# 
# m.group_add_tickers('g2', ['Upwork'])
# m.group_add_tickers('g2', ['Alteryx'])


























# --------------- YAML ---------------------------

"""
import yaml

def yaml_loader(filepath):
    #loads a yaml file
    with open(filepath, "r") as file_descriptor:
        data = yaml.load(file_descriptor)
    return data

def yaml_dump(filepath, data):
    #dumps data to ayaml file
    with open(filepath, "w") as file_descriptor:
        yaml.dump(data, file_descriptor)

if __name__ == "__main__":
    filepath = "test.yaml"
    data = yaml_loader(filepath)
    print data
    items = data.get("items")
"""






# ----------------- Fundamentals -----------------------------------
# EBIT = Revenue - COGS (Cost of goods sold) - Operating Expenses
# 21,777,000 - 10,677,000 - 6,834,000 = 4266000 = 4.266e+9

# cannot use this with alphavantage since it doesn't always have interest income
# EBIT = Net Sales - COGS (Cost of goods sold) - SG&A (Selling, General and Admin relative expenses) + II (Interest Income)
# 21,777,000  - 10,677,000 - 5,103,000 - 48000 = 5949000 = 5.949e+9








# ------------------- sqlite --------------------------------
# CREATE TABLE TimeSeriesDaily (ticker TEXT NOT NULL, date TIMESTAMP NOT NULL, open REAL, high REAL, low REAL, close REAL, adjusted_close REAL, volume REAL, dividend_amount REAL, split_coefficient REAL, PRIMARY KEY (ticker, date), FOREIGN KEY (ticker) REFERENCES StockTicker (ticker) ON DELETE CASCADE ON UPDATE NO ACTION)

# CREATE TABLE IncomeStatementAnn (
#     ticker TEXT NOT NULL,
#     endDate TIMESTAMP NOT NULL,
#     reportType TEXT NOT NULL,
#     costOfRevenue REAL,
#     discontinuedOperations REAL,
#     ebit REAL,
#     effectOfAccountingCharges REAL,
#     extraordinaryItems REAL, 
#     grossProfit REAL,
#     incomeBeforeTax REAL, 
#     incomeTaxExpense REAL,
#     interestExpense REAL,
#     interestIncome REAL,
#     minorityInterest REAL,
#     netIncome REAL,
#     netIncomeApplicableToCommonShares REAL,
#     netIncomeFromContinuingOperations REAL,
#     netInterestIncome REAL,
#     nonRecurring REAL, 
#     operatingIncome REAL, 
#     otherItems REAL,
#     otherNonOperatingIncome REAL, 
#     otherOperatingExpense REAL,
#     preferredStockAndOtherAdjustments REAL, 
#     reportedCurrency TEXT,
#     researchAndDevelopment REAL, 
#     sellingGeneralAdministrative REAL,
#     taxProvision REAL, 
#     totalOperatingExpense REAL, 
#     totalOtherIncomeExpense REAL,
#     totalRevenue REAL,
#     PRIMARY KEY (ticker, endDate, reportType),
#     FOREIGN KEY (ticker) REFERENCES StockTicker (ticker) ON DELETE CASCADE ON UPDATE NO ACTION  
# )



# CREATE TABLE BalanceSheet (
#     ticker TEXT NOT NULL,
#     date TIMESTAMP NOT NULL,
#     reportType TEXT NOT NULL,
#     reportedCurrency TEXT,
#     totalAssets REAL,
#     intangibleAssets REAL,
#     earningAssets REAL,
#     otherCurrentAssets REAL,
#     totalLiabilities REAL, 
#     totalShareholderEquity REAL,
#     deferredLongTermLiabilities REAL, 
#     otherCurrentLiabilities REAL,
#     commonStock REAL,
#     retainedEarnings REAL,
#     otherLiabilities REAL,
#     goodwill REAL,
#     otherAssets REAL,
#     cash REAL,
#     totalCurrentLiabilities REAL,
#     shortTermDebt REAL, 
#     currentLongTermDebt REAL, 
#     otherShareholderEquity REAL,
#     propertyPlantEquipment REAL, 
#     totalCurrentAssets REAL,
#     longTermInvestments REAL, 
#     netTangibleAssets TEXT,
#     shortTermInvestments REAL, 
#     netReceivables REAL,
#     longTermDebt REAL, 
#     inventory REAL, 
#     accountsPayable REAL,
#     totalPermanentEquity REAL,
#     additionalPaidInCapital REAL,
#     commonStockTotalEquity REAL,
#     preferredStockTotalEquity REAL,
#     retainedEarningsTotalEquity REAL,
#     treasuryStock REAL,
#     accumulatedAmortization REAL,
#     otherNonCurrrentAssets REAL,
#     deferredLongTermAssetCharges REAL,
#     totalNonCurrentAssets REAL,
#     capitalLeaseObligations REAL,
#     totalLongTermDebt REAL,
#     otherNonCurrentLiabilities REAL,
#     totalNonCurrentLiabilities REAL,
#     negativeGoodwill REAL,
#     warrants REAL,
#     preferredStockRedeemable REAL,
#     capitalSurplus REAL,
#     liabilitiesAndShareholderEquity REAL,
#     cashAndShortTermInvestments REAL,
#     accumulatedDepreciation REAL,
#     commonStockSharesOutstanding REAL,
#     PRIMARY KEY (ticker, date, reportType),
#     FOREIGN KEY (ticker) REFERENCES StockTicker (ticker) ON DELETE CASCADE ON UPDATE NO ACTION  
# )


# CREATE TABLE CashFlow (
#     ticker TEXT NOT NULL,
#     date TIMESTAMP NOT NULL,
#     reportType TEXT NOT NULL,
#     reportedCurrency TEXT,
#     investments REAL,
#     changeInLiabilities REAL,
#     cashflowFromInvestment REAL,
#     otherCashflowFromInvestment REAL,
#     netBorrowings REAL, 
#     cashflowFromFinancing REAL,
#     otherCashflowFromFinancing REAL, 
#     changeInOperatingActivities REAL,
#     netIncome REAL,
#     changeInCash REAL,
#     operatingCashflow REAL,
#     otherOperatingCashflow REAL,
#     depreciation REAL,
#     dividendPayout REAL,
#     stockSaleAndPurchase REAL,
#     changeInInventory REAL, 
#     changeInAccountReceivables REAL, 
#     changeInNetIncome REAL,
#     capitalExpenditures REAL, 
#     changeInReceivables REAL,
#     changeInExchangeRate REAL, 
#     changeInCashAndCashEquivalents TEXT,
#     PRIMARY KEY (ticker, date, reportType),
#     FOREIGN KEY (ticker) REFERENCES StockTicker (ticker) ON DELETE CASCADE ON UPDATE NO ACTION  
# )









# CREATE TABLE Cluster (
#     clusterName TEXT NOT NULL,
#     dateUpdate TIMESTAMP,
#     PRIMARY KEY (clusterName)
# );

# CREATE TABLE ClusterMember (
#     clusterName TEXT NOT NULL,
#     tickerName TEXT NOT NULL,
#     PRIMARY KEY (clusterName, tickerName),
#     FOREIGN KEY (clusterName) REFERENCES Cluster (clusterName) ON DELETE CASCADE ON UPDATE NO ACTION,
#     FOREIGN KEY (tickerName) REFERENCES StockTicker (ticker) ON DELETE NO ACTION ON UPDATE NO ACTION
# );





# CREATE TABLE ClusterRatiosQtr (
#     clusterName TEXT NOT NULL,
#     tickerName TEXT NOT NULL,
#     PRIMARY KEY (clusterName, tickerName),
#     FOREIGN KEY (clusterName) REFERENCES Cluster (clusterName) ON DELETE CASCADE ON UPDATE NO ACTION,
#     FOREIGN KEY (tickerName) REFERENCES StockTicker (ticker) ON DELETE NO ACTION ON UPDATE NO ACTION
# )


# CREATE TABLE ClusterRatiosAnn (
#     clusterName TEXT NOT NULL,
#     tickerName TEXT NOT NULL,
#     PRIMARY KEY (clusterName, tickerName),
#     FOREIGN KEY (clusterName) REFERENCES Cluster (clusterName) ON DELETE CASCADE ON UPDATE NO ACTION,
#     FOREIGN KEY (tickerName) REFERENCES StockTicker (ticker) ON DELETE NO ACTION ON UPDATE NO ACTION
# )





# UPDATE BalanceSheet
# SET shortTermDebt = 166400000,
#     capitalLeaseObligations = 923400000,
#     longTermDebt = 0
# WHERE
#     ticker = 'DBX' AND
#     reportFrequency = 'quarterly' AND
#     date = '2020-06-30';

# UPDATE BalanceSheet
# SET shortTermDebt = 164300000,
#     capitalLeaseObligations = 919500000,
#     longTermDebt = 0
# WHERE
#     ticker = 'DBX' AND
#     reportFrequency = 'quarterly' AND
#     date = '2020-03-31';


# UPDATE BalanceSheet
# SET shortTermDebt = 156600000,
#     capitalLeaseObligations = 850100000,
#     longTermDebt = 0
# WHERE
#     ticker = 'DBX' AND
#     reportFrequency = 'quarterly' AND
#     date = '2019-12-31';



# UPDATE BalanceSheet
# SET shortTermDebt = 146900000,
#     capitalLeaseObligations = 754200000,
#     longTermDebt = 0
# WHERE
#     ticker = 'DBX' AND
#     reportFrequency = 'quarterly' AND
#     date = '2019-09-30';


# UPDATE BalanceSheet
# SET shortTermDebt = 147200000,
#     capitalLeaseObligations = 721000000,
#     longTermDebt = 0
# WHERE
#     ticker = 'DBX' AND
#     reportFrequency = 'quarterly' AND
#     date = '2019-06-30';


# UPDATE BalanceSheet
# SET shortTermDebt = 144800000,
#     capitalLeaseObligations = 548200000,
#     longTermDebt = 0
# WHERE
#     ticker = 'DBX' AND
#     reportFrequency = 'quarterly' AND
#     date = '2019-03-31';


