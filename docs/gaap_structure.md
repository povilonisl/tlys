Retrieving financial Information from Financial Statements
- They are flat.
    - They give the concept, but they don't give us its position in the Network.
- There are MANY components
    - Components: 5000~ 
    - Abstracts/Rollups: 800~
    - Overall, 700~ differnt data points per statement 
    
    
Storing financial Information
- A lot of columns to store in SQlite
    - 2000 is the limit
    - Change data structure to No SQL?
    - Most liekly we will not see most of the columns in our lives, but there is no way to tell which ones.
- We could store the data in a flat manner
    - Worry about the Network only when we are trying to display the data.
    - Though, I prefer to Roll-up the abstract columns, so we cannot directly store it.
        - 


Storing the network
- Store each component and its parent?
- Store the Network as a tree.
    - Where its stored in just one table - ElementTable
    - Each Record:
        - Id: The gaap id of component/abstract
        - Label: Component/abstract's label
        - ... - Other info stored about the component/abstract
        - Parent: Reference to parent component (self referenching table).
        - Level: How deep it is
        - Line Number: From the source Network Table
        - Rollup ? - If true, then rollup, else don't
            - E.g. Balance sheet is parent of Total Assets?


Print structured statement
- Access each field in turn in selected Statement Table
- Find the field in the Element Table
- Check if this field is already in print dictionary
    - If not, 
        - then get get the Parent from Element Table
        - Check if in Print Dictionary
        - Repeat
    - If yes,
        - ...
- Sort the component table?


Displaying the financial Informtation:
    Income Statement
    BalanceSheet
        Assets: {2019: 34, 2020: 50}
            Short Term Assets: {2019: 23, 2020: 24}
                Cash, Cash Equivalents, Short Term Investments
                    Cash
                    Cash Equivalents
                    Short Term Investments
            Long Term Assets
        Liabilities
            Short Term
            Long Term
    Cash Flow



Balance Sheet Table
Record: DBX - 2020 - Ann
Fields:
    Assets, Cash, Other Receivables, Receivables.

How to get all of the historic data for this statement, not just current?
- Just store in a dict/list once the corresponding element found



Need to scrape the networks to get those relations....