We would have df_data (series??), we would have df_network

Iterate over each column/field in df_data.


# Requirements
- Determine if all of the data points align with other data we have, e.g. the example.
- Roll up our data. E.g. the example
    - To Make it easier when reporting.
- Create either export or visual functionality to see data in the network.
    - Potentially it should be possible to provide which data points we want to see.

# Questions
- Where does the rolled up data get stored?
    - I think I want to keep OG data and processed data in seperate tables. 
        - If we ever want to we can recalculate
        - Can perform some sanity checks
        - Also we don't have to retrieve the data again if we do want the OG data, if we had to it would feel like a bottleneck. Also, what if we cannot retrieve it for some reason?
    - e.g. IncomeStatment -> IncomeStatementProcessed
- How does it interact with auto_network_restructure
    - If validaton fails, then it fails, roll-up data does not get stored.
    - Can auto_network_restructure make use of it to determine how node(s) should be restructured?

# Example of the problem
if we know thata general and admn has value of -26,385M
We also know that selling and marketing expenses is at -20,865M
Finally if we know that Research & Development is -22,248M
Then we can these numbers up to get Operating Expenses of -48,663M
If we already had Operating expenses, then we would be validating our depper levels of network against higher lvels.




# Solution 1
Paramaters:
- df or series of data
- df of network
    - There should a column for node's path


So, what we want to do is get the values that we have and place it into the network.

Start at the lowest level that has data and roll up the data in breadth first approrach.