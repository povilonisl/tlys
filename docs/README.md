stock = Stock(ticker)
- __init__ -> _find_cik()

filing = stock.get_filing(period, year)
- get_financial_filing_info(period=period, cik=self.cik, year=year, quarter=quarter)
    - Returns records from master index that match the form-type, company, year, quarter (or latest if not provided)
        - each records contains the type of file filed and where to find it.
    - will ONLY retrieve that match the paramaters or latest year/quarter/
- If nothing has been retrieved for what has been asked.
    - goes through other quarters
- once found the filing for the given paramaters create a Filing Object.
    - Create DTD
    - Parse the sgml file and put its components into a dictionary.
    - Create a Document objects for each document in sgml txt file

filing.get_income_statements() 
- get FilingSummary.xml
- for each shortName (should only match one)
    - find the shortname in FilingSummary.xml
    - Get the filename where the data is stored for the shortname
    - Get document that has the file name
    - get html of the document
    - call get_financial_report(self.company, self.date_filed, financial_html_text) - lives in financials.py
    - return

get_financial_report(self.company, self.date_filed, financial_html_text)
- _process_financial_info(financial_html_text)
    - Put the html into beautiful soop
    - get table with class: 'report'
    - Get all rows
    - Get the metadata of Financial Statement from the first 2 rows
    - 






By downloading pages to local machine and parsing with BeautifulSoup, I retrieved US GAAP Elements, Abstract and Network. Links to original content:
http://www.xbrlsite.com/
http://www.xbrlsite.com/US-GAAP-2011/Exemplars/Viewer.html


https://xbrl.fasb.org/us-gaap/



TODO:
- Naming of the columns
    - Currently, they all have a prefix of us-gaap_, but should that be removed?
    - What about once I integrate other Standards/StockMarkets.