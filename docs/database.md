

Database technology: SQLite
Using Sqlite due to no cost of use, and potentially easy to transfer to remote SQL Database..

Tables:

# sql vs nosql 
nosql
- we don't have to define/create the columns beforehand
- we don't have to have loads of empty columns
- easy to retrieve only columns that have data

sql
- there are no risk of alteration in column names referencing the same data.
- easy to integrate with Pandas
    - Regular SQL -> Pandas
    - nosql can have complex data structures that might be hard for Pandas
    - nosql has inconsistent columns which might troubles Pandas.



# MongoDB
Follow the following tutorail to install MongoDB: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

## Start MongoDB from comand line - mongod
Start MongoDB: 
    sudo systemctl start mongod
Check the status:
    sudo systemctl status mongod
(Other) to ensure that MongoDB will start on reboot:
    sudo systemctl enable mongod
Stop MongoDB:
    sudo systemctl stop mongod
Restart MongoDB:
    sudo systemctl restart mongod

Follow the state of the process for errors by checking the following file:
    /var/log/mongodb/mongod.log

## Using MongoDB - mongosh
To use MonogDB:
    mongosh



# EdgarFilingIndex
CREATE TABLE EdgarFilingIndex (
    cik TEXT,
    companyName TEXT,
    filingType TEXT,
    date TIMESTAMP,
    filingFilePath TEXT,
    filingIndexPath TEXT,
    PRIMARY KEY(cik, filingType, date, filingFilePath)
);




# Element
- Element
    - deprecated: 
        - To store the date of when it was depricated, otherwise Null
    - source: 
        - To store 'us-gaap-{{yyyy}}-{{mm}}-{{dd}}' or all the filingPath seperated by | 

CREATE TABLE Element (
    elementId TEXT PRIMARY KEY,
    elementName TEXT NOT NULL,
    label TEXT,
    prefix TEXT,
    source TEXT, 
    balance TEXT CHECK (balance IN ("debit", "credit", NULL)), 
    deprecated TEXT, 
    periodType TEXT CHECK (periodType IN ("instant", "duration", NULL)), 
    type TEXT, 
    abstract BOOLEAN CHECK (abstract IN (0, 1))
);


CREATE TABLE test_Element (
    elementId TEXT PRIMARY KEY,
    elementName TEXT NOT NULL,
    label TEXT,
    prefix TEXT,
    source TEXT, 
    balance TEXT CHECK (balance IN ("debit", "credit", NULL)), 
    deprecated TEXT, 
    periodType TEXT CHECK (periodType IN ("instant", "duration", NULL)), 
    type TEXT, 
    abstract BOOLEAN CHECK (abstract IN (0, 1))
);


# Network
Network
- originDesccription
    - where did we find this element, was it from a statement or fasb network?
    - ticker, year, statement, reportingFrequency
- elementId
    - it will never differ from nodeId
    - it had to be it's own field so that we can link elementId as foreign key. (I think - NetworkRollUpRule works...)
- nodeId
    - we should only have one parent for each node, so there should never be a nodeId that has is duplicated outside of specified networkName.

CREATE TABLE Network (
    nodeId TEXT,
    networkName TEXT,
    parentNodeId TEXT DEFAULT NULL,
    ordinal INT DEFAULT 0, 
    originalParentNodeId TEXT DEFAULT NULL, 
    originDescription TEXT,  
    weight INTEGER CHECK (weight IN (-1, 1, NULL)),
    PRIMARY KEY (nodeId, networkName)
    FOREIGN KEY(parentNodeId, networkName) REFERENCES Network(nodeId, networkName)
    FOREIGN KEY(originalParentNodeId, networkName) REFERENCES Network(nodeId, networkName)
    FOREIGN KEY(nodeId) REFERENCES Element(elementId)
);


CREATE TABLE test_Network (
    nodeId TEXT,
    networkName TEXT,
    parentNodeId TEXT DEFAULT NULL,
    ordinal INT DEFAULT 0, 
    originalParentNodeId TEXT DEFAULT NULL, 
    originDescription TEXT,  
    weight INTEGER CHECK (weight IN (-1, 1, NULL)),
    PRIMARY KEY (nodeId, networkName)
    FOREIGN KEY(parentNodeId, networkName) REFERENCES test_Network(nodeId, networkName)
    FOREIGN KEY(originalParentNodeId, networkName) REFERENCES test_Network(nodeId, networkName)
    FOREIGN KEY(nodeId) REFERENCES test_Element(elementId)
);


# NetworkRollUpRule
Stores the rules that network(s) cannot break. Theses are usually automatically populated using the calc.xml file in filings of companies
NetworkRollUpRule
- PRIMARY KEY
    - composite primary key of nodeId, parentNodeId, networkName - there might be multiple structural rules for each node. e.g. 'cash, cash equivalents' is child of 'short term assets', it's also a child of Assets.

CREATE TABLE NetworkRollUpRule (
    nodeId TEXT,
    parentNodeId TEXT,
    networkName TEXT,
    weight INTEGER CHECK (weight IN (-1, 1, NULL)),
    source TEXT,
    PRIMARY KEY (nodeId, parentNodeId, networkName),
    FOREIGN KEY (nodeId) REFERENCES Element(elementId),
    FOREIGN KEY (parentNodeId) REFERENCES Element(elementId)
);

CREATE TABLE test_NetworkRollUpRule (
    nodeId TEXT,
    parentNodeId TEXT,
    networkName TEXT,
    weight INTEGER CHECK (weight IN (-1, 1, NULL)),
    source TEXT,
    PRIMARY KEY (nodeId, parentNodeId, networkName),
    FOREIGN KEY (nodeId) REFERENCES test_Element(elementId),
    FOREIGN KEY (parentNodeId) REFERENCES test_Element(elementId)
);




## IncomeStatement:
- By Default it only has 3 columns: ticker, date, filingType, dataType. The 3 of them form a composite Primary Key.
- Extra coluimns are added based on the Network of IncomeStatement, where the network is stored in StatementElement under the root called 'soi-cal'.
- Extra columns might be added as Financial Statements are parsed from Edgar.
    - There should be no element in a Financial Statement that doesn't exist in the Network, hence there shouldn't be an elelemnt in Financial Statement that doesn't have a corresponding column in IncomeStatement
    - Though, if one is found 
        -  **TODO**
        - It will be reported to the user, and outputed to a JSON file???
- **TODO**: change column name 'reportFrequency' to 'filingType'?
- **TODO**: Update 'dataType' column 
    - To not allow null values
    - To only allow certain values, e.g. 'original', 'calc'
- 
IncomeStatement
- ticker - the 4 characters to represent company's stock
- date - acceptence date of the filing (I think)
- filingType - e.g. 10-K, 10-Q
- dataType - e.g original, rollup
- source - currently this is the location filing in edgar 
- mongodbId - the id of corresponding record in mongoDB cluster.

CREATE TABLE IncomeStatement (
    ticker TEXT NOT NULL,
    date TIMESTAMP NOT NULL,
    filingType TEXT NOT NULL,
    dataType TEXT NOT NULL,
    source TEXT NOT NULL,
    mongodbId TEXT NOT NULL,
    PRIMARY KEY (ticker, date, filingType, dataType),
    FOREIGN KEY (ticker) REFERENCES StockTicker (ticker) ON DELETE CASCADE ON UPDATE NO ACTION  
);


## BalanceSheet
- Same IncomeStatement, please refer to it.

CREATE TABLE BalanceSheet (
    ticker TEXT NOT NULL,
    date TIMESTAMP NOT NULL,
    filingType TEXT NOT NULL,
    dataType TEXT NOT NULL,
    source TEXT NOT NULL,
    mongodbId TEXT NOT NULL,
    PRIMARY KEY (ticker, date, filingType, dataType),
    FOREIGN KEY (ticker) REFERENCES StockTicker (ticker) ON DELETE CASCADE ON UPDATE NO ACTION  
);


## CashFlow
- Same IncomeStatement, please refer to it.

CREATE TABLE CashFlow (
    ticker TEXT NOT NULL,
    date TIMESTAMP NOT NULL,
    filingType TEXT NOT NULL,
    dataType TEXT NOT NULL,
    source TEXT NOT NULL,
    mongodbId TEXT NOT NULL,
    PRIMARY KEY (ticker, date, filingType, dataType),
    FOREIGN KEY (ticker) REFERENCES StockTicker (ticker) ON DELETE CASCADE ON UPDATE NO ACTION  
);














SELECT
    stm_ele.element,
    chl_ele.element
FROM
    StatementElement AS stm_ele
JOIN
    StatementElement AS chl_ele
ON 
    stm_ele.element = chl_ele.parentElement
WHERE
    stm_ele.element = "us-gaap_Revenues";  



WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, path) as (
    SELECT nodeId, parentNodeId, originalParentNodeId, nodeId from Network WHERE parentNodeId = 'soi-cal' and networkName = 'soi-cal'
    
    UNION
    
    SELECT Network.nodeId, Network.parentNodeId, Network.originalParentNodeId, paths.path || '/' || Network.nodeId
    FROM Network 
    JOIN paths
    WHERE Network.parentNodeId = paths.nodeId and networkName = 'soi-cal'
)
SELECT nodeId, parentNodeId, originalParentNodeId, path, Element.balance, Element.elementName, Element.label FROM paths
LEFT JOIN Element
ON paths.nodeId = Element.elementId;







WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, path) as (
    SELECT nodeId, parentNodeId, originalParentNodeId, nodeId from Network WHERE parentNodeId = 'soi-cal' and networkName = 'soi-cal'
    
    UNION
    
    SELECT Network.nodeId, Network.parentNodeId, Network.originalParentNodeId, paths.path || '/' || Network.nodeId
    FROM Network 
    JOIN paths
    WHERE Network.parentNodeId = paths.nodeId and networkName = 'soi-cal'
)
SELECT paths.path, NetworkRollUpRule.nodeId, NetworkRollUpRule.parentNodeId FROM paths
LEFT JOIN NetworkRollUpRule
ON paths.nodeId = NetworkRollUpRule.nodeId;



WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, weight, path) as (
    SELECT nodeId, parentNodeId, originalParentNodeId, weight, nodeId from Network WHERE parentNodeId = 'soi-cal' and networkName = 'soi-cal'
    
    UNION
    
    SELECT Network.nodeId, Network.parentNodeId, Network.originalParentNodeId, Network.weight, paths.path || '/' || Network.nodeId
    FROM Network 
    JOIN paths
    WHERE Network.parentNodeId = paths.nodeId and networkName = 'soi-cal'
)
SELECT 
    NetworkRollUpRule.nodeId ruleNodeId, 
    NetworkRollUpRule.parentNodeId ruleParentNodeId, 
    nPath.parentNodeId as networkParentNodeId,
    pnPath.path as networkParentNodePath,
    nPath.path ruleNodePath,
    rpnPath.path ruleParentNodePath
FROM NetworkRollUpRule
LEFT JOIN paths nPath
ON nPath.nodeId = NetworkRollUpRule.nodeId
LEFT JOIN paths pnPath
ON nPath.parentNodeId = pnPath.nodeId
LEFT JOIN paths rpnPath
ON rpnPath.nodeId = NetworkRollUpRule.parentNodeId
WHERE 
    NetworkRollUpRule.NetworkName == 'soi-cal'


df_network_rule = dl_db.sql_select(sql, index_column=None)