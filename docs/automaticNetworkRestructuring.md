


# Requirement 

1. There should be no change unless there is a conflict or Automatic Network Validation fails. 
2. If all rules will produce a parallel result then take the first rule.
    - [Possible Scenario 1]
3. Node should choose to be as close to the parent as possible.
    - The distance is (No. of edges between node and parent node)^2
    - [Possible Scenario 2]
        - A -> C -> B -> D
    - [Possible Scenario 3]
        - A -> G -> C -> F -> X
    




# Network layout and Network Rules
To be able to visualise this please look at the source md file.

Network Original:
        G
    C
        F
            I
A
        E
    B
            X
        D
            J


Network Current:
        G
    C
        F
            I
                H
A           
            J
        E
    B
        D
            


Network Rule:
No.     Node    parentNode
1       H       F
2       H       C
3       H       I
4       J       E

# Scenarios
What is the general rule? Node should attempt to be as close to the parent as possible? Or, the other way round?


Possible Scenario 1:
Who should be a child of who? A -> B -> C -> D or A -> C -> B -> D?
Network
parentNode      Node
                A
A               B
A               C

Network Rule:
parentNode      Node
B               D
C               D



Possible Scenario 2:
There is a rule for C and it is satisfied, so we don't want to restructure it. Hence, C would become parent of B.
Network
parentNode      Node
                A
A               B
A               C

Network Rule:
parentNode      Node
B               D
C               D
A               C


Possible Scenario 3:
Developement of Scenario 2
Network
parentNode      Node
                A
A               B
A               C
C               G
C               F
B               X

Network Rule:
parentNode      Node
C               F
A               G
F               X
G               X



Possible Scenario 4:
C -> D does not have A is a root (parent). Though, D is parent of X, so should C also be parent of X? And, where should they be moved to in the network?
Network
parentNode      Node
                A
A               B
C               D
B               X

Network Rule:
parentNode      Node
B               X
D               X


Possible Scenario 5:
What happens when there are multiple rules which individually are satisfied, but all of them have been applied the network has incorrect structure. Outcome should be error.
Network
parentNode      Node
                A
A               B
A               C
C               F
B               X

Network Rule:
parentNode      Node
F               B
C               X
F               X






# Questions
- How do we ensure that we are not breaking rules of children nodes of the node we will restructure?
- What happens if the resulting restructuring fails validation?
    - How would it correct itself?
- Does distance need to be squared?
- Do we consider initial network edges as network rules? 
    - Consider [Possible Scenario 3]
        - Should the edge from A go to C or from A to G, also
        - Should there be an edge from A to B, or does B move with node X?
    - Consider [Possible Scenario 4]
        - For similar reasons as [Possible Scenario 3].

# Solution 1

Principles
- The node closest to the rule parent
- Sort the rules by rule parent node ASC, for each rule we get all rules for that node.



Conflicting rules
- The rules where parent of rule is involved as parent of another rule or as a child.