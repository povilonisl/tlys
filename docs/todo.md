14/02/2022
- [] How and should we ensure that rules are unique in NetwowrkRollUpRules??

19/04/2021
- [ ] Implement roll-up and roll-up validation.
- [ ] Implement automatic restructuring of Network based on NetworkRules table. 
- [x] Need to retrieve '{{stockTicker}}-{{filingData}}.xsd', '{{stockTicker}}-{{filingData}}_cal.xsd', '{{stockTicker}}-{{filingData}}_def.xsd' files
    - There might be no need to retrieve _def.xsd file
- [x] Need to parse '{{stockTicker}}-{{filingData}.xsd', '{{stockTicker}}-{filingData}_cal.xsd', '{{stockTicker}}-{filingData}_def.xsd' files
    - And make use of it as appropriate
- [x] Need to find a way to deal with us-gaap elements being used in a filing, but not having them in my database.
- [ ] Need to create new field in IncomeStatement, BalanceSheet and CashFlow table
    - Source: link to edgar archives.
- [x] Need to create new field on Elements table
    - Source: link to edgar archives from where this variable first came from. And, subsequent ones seperated by '|'
- [x] Need to create new Table
    - NetworkRules: Stores rules based on calc files from SEC filings found in _cal.xsd file.
        - NetworkName
        - element 
        - Parent Element
- [ ] Refresh the data for the following tables: Network, Element. Resent and refresh for IncomeStatement, BalanceSheet, and CashFlow.
- [ ] Can we store non structural roles somehow?
- [ ] Consider storing the actual SEC filings locally.

[x]


Project: Opening up access
- [ ] Make the code SQL Injection safe.


Test
- [ ] revisit no-sql