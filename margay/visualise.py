
import plotly.graph_objects as g_obj
import igraph


class Visualise:
    def __init__(self):
        pass

    def temp_one(self):
        pass

    def display_simple_data(self):
        pass


    def display_network(self, df_edge, df_node, root_node = None):
        """Given a Dataframe of nodes and parent nodes display it in a tree structure
        using plotly and igraph 

        Args:
            df_network (DataFrame): 
                First Column is the source node, Second Column is the target node,
                following columns are attributes of the edge.
            df_node (DataFrame):
                Fist Column is the node, following columns are attributes of the node.
                A LOT TODO: fist column should be name, and following columns should various attributes
            year (int): For which year should the filing be retrieved 
            period (str, optional): Quarterly or annual Filing. Defaults to 'annual'.

        Returns:
            [type]: [description]
        """
        
        graph = igraph.Graph.DataFrame(df_edge, directed=True)
        
        # find all the nodes that can visit/incoming the target node 
        node = graph.vs.find(root_node)
        target_node = set(graph.subcomponent(node, mode="in"))
        seen_nodes = set()
        for key in df_node:
            # find all the nodes that depart from source node
            node = graph.vs.find(name=key)

            # update the 'value' attribute of the node 
            node['value'] = df_node[key]

            source_node = set(graph.subcomponent(node, mode="out"))
            seen_nodes = seen_nodes.union(source_node)

        graph.delete_vertices(target_node.difference(seen_nodes))

        # update the 'label' attribute of the node
        for v in graph.vs:
            v['label'] = v['name'].replace('us-gaap_', '')

        # create the layout we want to use for the graph
        layout = graph.layout_reingold_tilford(mode="in", root=[0])
        layout.mirror(1) # mirror in the x axis

    
        # ----------
        # using Plotly create visualisation of the graph
        # ---------------

        # gather the coordinates for each node
        x_node, y_node = list(zip(*layout.coords))
        # gathe the attributes of each node
        labels = [vertex['label'] for vertex in graph.vs]
        values = [vertex['value'] for vertex in graph.vs]

        x_edge = []
        y_edge = []

        for e in graph.es:
            edge = e.tuple
            x_edge += [layout.coords[edge[0]][0], layout.coords[edge[1]][0], None]
            y_edge += [layout.coords[edge[0]][1], layout.coords[edge[1]][1], None]
            
        fig = g_obj.Figure()
        fig.add_trace(g_obj.Scatter(
            x=x_edge,
            y=y_edge,
            mode='lines',
            line=dict(color='rgb(210,210,210)', width=3),
            hoverinfo='none'
        ))
        fig.add_trace(g_obj.Scatter(
            x=x_node,
            y=y_node,
            mode='markers',
            name='Element',
            marker=dict(
                symbol='circle-dot',
                size=18,
                color='#6175c1',    #'#DB4551',
                line=dict(color='rgb(50,50,50)', width=1)
            ),
            text=labels,
            hoverinfo='text',
            opacity=0.8,
        ))

        fig.update_layout(
            title = 'Tree....',
            annotations = self._get_annotations(layout.coords, labels, values)
        )

        fig.show()
    

    def _get_annotations(self, pos, labels, value, font_size=9, font_color='rgb(0,0,0)'):
        L=len(pos)
        if len(labels)!=L:
            raise ValueError('The lists pos and text must have the same len')
        annotations = []

        for k in range(L):
            text = (labels[k] if len(labels[k]) < 40 else labels[k][:40] + '...')
            if value[k]:
                text += '<br>' + '<span style="color:green; font-weight:800">'+ f'{value[k]:,}' + '</span>'

            annotations.append(
                dict(
                    text= text, # or replace labels with a different list for the text within the circle
                    x=pos[k][0], y=pos[k][1]-0.35,
                    xref='x1', yref='y1',
                    font=dict(color=font_color, size=font_size),
                    showarrow=False)
            )
        return annotations
        
    
    def create_igraph(self, df_rule):
        """This creates a graph from root to leaves, given a dataframe of rules
        that indicates parent nodes of nodes where the relationship does not have to be direct 

        Args:
            df_rule (_type_): _description_

        Returns:
            _type_: _description_
        """        
        dict_node = {}
        # for rule in df_rule.itertuples():
        

        for index, row in df_rule.iterrows():
            parent_node_id = row['parentNodeId'] if row['parentNodeId'] != None else 'NA'

            if row['nodeId'] in dict_node:
                dict_node[row['nodeId']].append(parent_node_id)
            else:
                dict_node[row['nodeId']] = [parent_node_id]

            if parent_node_id not in dict_node:
                
                dict_node[parent_node_id] = []
                

        g = igraph.Graph(directed=True)
        # g.add_vertices(list(dict_node.keys()))
           
        dict_path = {}
        for v_name in dict_node.keys():
            self._add_to_graph(g, v_name, dict_path, dict_node)

        return g




    def _is_sub_path(self, parent_info, path):
        for other_parent_v_name in list(parent_info):
            other_parent_path = parent_info[other_parent_v_name]

            set_other_path = set(other_parent_path)
            set_path = set(path)

            if set_path.issubset(set_other_path):
                return True
            elif set_other_path.issubset(path):
                del parent_info[other_parent_v_name]
            
        return False

    def _add_to_graph(self, g, v_name, dict_path, dict_node):
        list_v_name = []
        set_v_name = set()
        if len(g.vs) > 0:
            list_v_name = g.vs['name']
            set_v_name = set(list_v_name)

        # if the vertex alrady exists then it has been created and connected
        if v_name in set_v_name:
            return

        # get parent vertex ids
        list_parent_v_name = dict_node[v_name]

        # if we have not created parent vertecies yet then create them first
        set_new_v_name = set(list_parent_v_name).difference(set_v_name)
        for new_v_name in set_new_v_name:
            self._add_to_graph(g, new_v_name, dict_path, dict_node)


        g.add_vertices(v_name)
        if len(list_parent_v_name) == 0:
            dict_path[v_name] = []
            return

        # iterate over parent vertecies and remove them if they are a sub path of another vertex
        # as we do not have to worry about them anymore
        # Also, identify which vertex has the longest path
        parent_info = {}
        l_p_v_name = None
        for parent_v_name in list_parent_v_name:
            parent_path = dict_path[parent_v_name]

            # check if vertex is a sub path
            # the function will also check if another path is a sub path of 'parent_path' and remove it from dictionary
            if self._is_sub_path(parent_info, parent_path):
                continue

            if (
                (l_p_v_name == None) or 
                (not l_p_v_name in parent_info) or 
                (len(parent_path) > len(parent_info[l_p_v_name]))
            ):
                # vertex qualifies to be counted as the vertex with longest path
                l_p_v_name = parent_v_name

            # vertex is not a sub path so add it to dictionary
            parent_info[parent_v_name] = parent_path
        
        # list parent names that are not sub-vertecies of other parent verticies 
        for parent_v_name in list(parent_info):
            if l_p_v_name == parent_v_name or not parent_v_name in parent_info:
                continue

            # identify where the longest path and iterated parent path intersect
            joining_v_id = None
            joining_path_idx = -1
            for temp_v_id in parent_info[parent_v_name]:
                joining_v_id = temp_v_id
                joining_path_idx += 1

                if temp_v_id in parent_info[l_p_v_name]:
                    break

            # get the first vertex that diverges from the longest path
            first_v_id = parent_info[parent_v_name][joining_path_idx - 1]

            # delete the first edge that diverges from longest path
            g.delete_edges([(first_v_id, joining_v_id)])
            
            # create a new edge between first diverging vertex and last vertex of longest path
            g.add_edges([(first_v_id, l_p_v_name)])
            
            # update our dictionary that tracks the paths
            parent_info[parent_v_name] = parent_info[l_p_v_name] + parent_info[parent_v_name][joining_path_idx + 1: ]
            del parent_info[l_p_v_name]  
            l_p_v_name = parent_v_name
            
        
        g.add_edges([(v_name, l_p_v_name)])
        # keep track of paths to the root from each node
        dict_path[v_name] = g.subcomponent(g.vs.find(name=v_name), mode="out")
        

