from margay.connectEdgar.stock import Stock
from margay.connectEdgar.financials import FinancialReportEncoder
import json
import pathlib

def getSecFiling(ticker, year):

    stock = Stock(ticker)
    period = 'annual'
    #year = year
    filing = stock.get_filing(period, year)

    return filing


aaplFiling = getSecFiling('AAPL', 2020)
aaplIncomeStatement = aaplFiling.get_income_statements()

msftFiling = getSecFiling('MSFT', 2020)
msftIncomeStatement = msftFiling.get_income_statements()

dbxFiling = getSecFiling('DBX', 2020)
dbxIncomeStatement = dbxFiling.get_income_statements()

nflxFiling = getSecFiling('NFLX', 2020)
nflxIncomeStatement = nflxFiling.get_income_statements()

acnFiling = getSecFiling('ACN', 2020)
acnIncomeStatement = acnFiling.get_income_statements()

koFiling = getSecFiling('KO', 2020)
koIncomeStatement = koFiling.get_income_statements()

amznFiling = getSecFiling('AMZN', 2020)
amznIncomeStatement = amznFiling.get_income_statements()

tslaFiling = getSecFiling('TSLA', 2020)
tslaIncomeStatement = tslaFiling.get_income_statements()

dictIncome = {
    'aapl': aaplIncomeStatement.reports[0].map,
    'msft': msftIncomeStatement.reports[0].map,
    'dbx': dbxIncomeStatement.reports[0].map,
    'nflx': nflxIncomeStatement.reports[0].map,
    'acn': acnIncomeStatement.reports[0].map,
    'ko': koIncomeStatement.reports[0].map,
    'amzn': amznIncomeStatement.reports[0].map,
    'tsla': tslaIncomeStatement.reports[0].map
}

dictCompare = {}

for ticker in dictIncome:
    for itemStatement in dictIncome[ticker]:
        if itemStatement in dictCompare:
            dictCompare[itemStatement]['tickers'].append(ticker)

            if dictIncome[ticker][itemStatement].label in dictCompare[itemStatement]:
                dictCompare[itemStatement][dictIncome[ticker][itemStatement].label].append(ticker)
            else:
                dictCompare[itemStatement][dictIncome[ticker][itemStatement].label] = [ticker]
        else:
            dictCompare[itemStatement] = {
                'tickers': [ticker],
                dictIncome[ticker][itemStatement].label: [ticker]}

print(dictCompare)


filePath = './scripts/TestSecEdgarFinancials/KeyComparison'
pathlib.Path(filePath).mkdir(parents=True, exist_ok=True)

with open(filePath + 'test.json', 'w') as json_file:
    json.dump(dictCompare, json_file)


# dbxFiling = getSecFiling('DBX', 2020)



#     filePath = './scripts/TestSecEdgarFinancials/' + ticker + '/' + str(year) + '/'
#     pathlib.Path(filePath).mkdir(parents=True, exist_ok=True)

#     income_statements = filing.get_income_statements()
#     with open(filePath + 'income_statements.json', 'w') as json_file:
#         json.dump(json.loads(FinancialReportEncoder().encode(income_statements)), json_file)

#     balance_sheets = filing.get_balance_sheets()
#     with open(filePath + 'balance_sheets.json', 'w') as json_file:
#         json.dump(json.loads(FinancialReportEncoder().encode(balance_sheets)), json_file)

#     cash_flows = filing.get_cash_flows()
#     with open(filePath + 'cash_flows.json', 'w') as json_file:
#         json.dump(json.loads(FinancialReportEncoder().encode(cash_flows)), json_file)

# testSecEdgarFinancials('NFLX', 2012)


# from connectEdgar.dtd import DTD
# from connectEdgar.sgml import Sgml


# dtd = DTD()

# with open(r'src/testText.txt', 'r') as textFile:
#     text = textFile.read()
#     try:
#         sgml = Sgml(text, dtd)
        
#     except:
#         pass
    
#     print(sgml.map)
#     print()
#     print(sgml.map['<SEC-DOCUMENT>']['<DOCUMENT>'])
#     print(sgml.map['<SEC-DOCUMENT>']['<DOCUMENT>'][0])
#     print(type(sgml.map['<SEC-DOCUMENT>']['<DOCUMENT>'][0]['<TEXT>']))
    # print(type(sgml.map['<SEC-DOCUMENT>']['<DOCUMENT>']['<TEXT>']))