from datetime import timedelta, date, datetime


class Util:
    """ Contains Helepr Funcs
    """

    def __init__(self):
        pass

    def last_day_of_month(self, dt):
        """ Change the day of datetime to the last of  month.

        Args:
            dt (datetime.datetime): A date 

        Returns:
            datetime.datetime: New datetime object where the day is the last of month.
        """
        if dt.month == 12:
            return dt.replace(day=31)
        return datetime(dt.year, dt.month + 1, 1) - timedelta(days=1)