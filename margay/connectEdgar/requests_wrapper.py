import requests

class GetRequest:
    def __init__(self, url):
        # creating header should help avoid being detected by a bot detected and kick out of the site.
        # we're not trying to overload the site, we need this because we were getting kicked out for no reason
        headers = {
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36'
        }

        response = requests.get(url, headers = headers)
        response.encoding = 'utf-8'
        if response.status_code != requests.codes.ok:
            raise RequestException('{}: {}'.format(response.status_code, response.text))
        
        self.response = response

class RequestException(Exception):
    pass