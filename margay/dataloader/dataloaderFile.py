class DataloaderFile:
    """[summary]

    TODO:
        - Export StatementElement to JSON
        - Import JSON into StatementElement

        - Output Fundamental data into presentable format in JSON
        - Import data from JSON.
    """


    def __init__(self, dl_db):
        self.dl_db = dl_db

    def export_to_csv(self, df, file_name, exe_path="/usr/bin/libreoffice"):
        """Open dataframe df in excel.

        exe_path - path to the program that will open the csv after it has been saved
        index=True - export the index of the dataframe as the first columns
        tmp_path    - directory to save the file in


        This creates a temporary file name, exports the dataframe to a csv of that file name,
        and then tells excel to open the file (in read only mode). (It uses df.to_csv instead
        of to_excel because if you don't have excel, you still get the csv.)

        Note - this does NOT delete the file when you exit. 
        """

        # df.to_csv(tmp_name, index=index)
        # cmd=[excel_path, '/r', '/e', tmp_name]
        df.to_csv(file_name + '.csv', index=True)
        # cmd=[excel_path, tmp_name]
        cmd=[exe_path, file_name + '.csv']
        
        try:
            ret_val=subprocess.Popen(cmd).pid
        except:
            print("open_in_excel(): failed to open excel")
            print("filename = ", file_name + '.csv')
            print("command line = ", cmd)
            print("Unexpected error:", sys.exc_info()[0])

        return