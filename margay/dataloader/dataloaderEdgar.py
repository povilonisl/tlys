
import pandas as pd


from margay.connectEdgar.stock import Stock
from margay.connectEdgar.financials import FinancialReportEncoder
from margay.index_edgar_filing import download_index



from margay.dataloader_old import DataloaderDB
from margay.dataloader_old import DataloaderMongoDB
"""
    (new) dl_db.var_insert(self, updates, commit)
        updates:[{
            table: String
            values: {field: value} 
            where: {field: value}
        }]

"""


class DataloaderEdgar:
    def __init__(self, dl_db, dl_mongo):
        # self.dl_db = DataloaderDB()
        # self.dl_mongo = DataloaderMongoDB()
        self.dl_db = dl_db
        self.dl_mongo = dl_mongo


        # I think stack overflow helped.
    def is_subpath(self, node, path, list_path):
        return any(path in y and path != y for y in list_path)

    def get_rule_node_path(self, network_name):
        if self.dl_db == None:
            return

        sql_statement = f'''
            WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, weight, path) as (
                SELECT nodeId, parentNodeId, originalParentNodeId, weight, nodeId FROM test_Network WHERE parentNodeId = '{network_name}' and networkName = '{network_name}'
                
                UNION
                
                SELECT test_Network.nodeId, test_Network.parentNodeId, test_Network.originalParentNodeId, test_Network.weight, paths.path || '/' || test_Network.nodeId
                FROM test_Network 
                JOIN paths
                WHERE test_Network.parentNodeId = paths.nodeId and networkName = '{network_name}'
            )
            SELECT 
                test_NetworkRollUpRule.nodeId ruleNodeId, 
                test_NetworkRollUpRule.parentNodeId ruleParentNodeId, 
                nPath.parentNodeId as networkParentNodeId,
                pnPath.path as networkParentNodePath,
                nPath.path ruleNodePath,
                rpnPath.path ruleParentNodePath
            FROM test_NetworkRollUpRule
            LEFT JOIN paths nPath
            ON nPath.nodeId = test_NetworkRollUpRule.nodeId
            LEFT JOIN paths pnPath
            ON nPath.parentNodeId = pnPath.nodeId
            LEFT JOIN paths rpnPath
            ON rpnPath.nodeId = test_NetworkRollUpRule.parentNodeId
            WHERE 
                test_NetworkRollUpRule.networkName == '{network_name}'
        '''
        df =  pd.read_sql_query(sql_statement, self.dl_db)

        df['isSubpath'] = [
            ruleParentNodePath in networkParentNodePath or
            self.is_subpath(ruleNodeId, ruleParentNodePath, df['ruleParentNodePath'].loc[df['ruleNodeId'] == ruleNodeId]) 
            for ruleNodeId, ruleParentNodePath, networkParentNodePath in zip(df['ruleNodeId'], df['ruleParentNodePath'], df['networkParentNodePath'])
        ]

        return df
            
    
    def automatically_restructure_network(self, df, network_name):
        """Given the DF describing current rules, return the changes needed so the respective 
        Network abides by those rules.

        Args:
            df (pandas.DataFrame):  A DF where each row corresponds to a row in NetworkRollUpRule with 
                        additional information about the node and parent node.
            network_name (Str): Name of the Network that we are checking if the rules are followed.

        Raises:
            Exception: [description]
            Exception: [description]
        """

        # -----------------------------------------------
        # -- Logic to insert new node into the network --
        # -----------------------------------------------
        df_new_node = df[df['ruleNodePath'].isnull()] # node not in network
        df_new_node_unique = df_new_node[~df_new_node['ruleNodeId'].duplicated()]
        list_network_insert = []
        for index, row in df_new_node_unique.iterrows():
            df_best_path = df_new_node[df_new_node['ruleNodeId'] == row['ruleNodeId']]
            
            # filtering out rules whoes path is a subset of another rule's path.
            # df_best_path['isSubpath'] = [self.is_subpath(x, df_best_path['ruleParentNodePath']) for x in df_best_path['ruleParentNodePath']]
            df_best_path = df_best_path[df_best_path['isSubpath'] == False] 
            
            if df_best_path.shape[0] != 1: # TODO: I think this is completely possible and needs to be dealt with.
                #raise Exception(f'Insertion:Network: {network_name}. I am not able to deal with multiple best paths \n{df_best_path}')
                print(f'ERROR: Insertion:Network: {network_name}. I am not able to deal with multiple best paths \n{df_best_path}')
            list_network_insert.append({
                'nodeId': df_best_path.iloc[0]['ruleNodeId'],
                'parentNodeId': df_best_path.iloc[0]['ruleParentNodeId'],
                'networkName': network_name,
                'weight': df_best_path.iloc[0]['ruleWeight'],
                'originalParentNodeId': None
            })
        # self.dl_db.var_insert('test_Network', list_new_network_node, commit = True) #TODO - does this satisfy other rules??



        # ------------------------------------------------
        # -- Logic to update parent of an existing node --
        # ------------------------------------------------
        # Is there a reason why we need to seperate insertions and updates?
        df_update_node = df[df['ruleNodePath'].notnull()] # node in network
        df_update_node_unique = df_update_node[~df_update_node['ruleNodeId'].duplicated()]
        list_network_update = []
        for index, row in df_update_node_unique.iterrows():
            df_best_path = df_update_node[df_update_node['ruleNodeId'] == row['ruleNodeId']]
            
            # get the longest path, or the rule where ruleParentNodeId is the closest to the ruleNodeId in terms of hierarchy
            # df_best_path['isSubpath'] = [self.is_subpath(x, df_best_path['ruleParentNodePath']) for x in df_best_path['ruleParentNodePath']]
            df_best_path = df_best_path[df_best_path['isSubpath'] == False] 

            if df_best_path.shape[0] == 0:
                continue
            elif df_best_path.shape[0] != 1: # TODO: I think this is completely possible and needs to be dealt with.
                # raise Exception(f'Update:Network: {network_name}. I am not able to deal with multiple best paths \n{df_best_path}')
                print(f'ERROR: Insertion:Network: {network_name}. I am not able to deal with multiple best paths \n{df_best_path}')

            # update the database with the new parent
            # sqlStatement = f'''
            #         UPDATE test_Network
            #         SET 
            #             parentNodeId = "{df_best_path.iloc[0]['ruleParentNodeId']}",
            #             ordinal = "{0}"
            #         WHERE
            #             nodeId = "{df_best_path.iloc[0]['ruleNodeId']}" and
            #             networkName = "{network_name}";
            #     '''
            # self.dl_db.sql_execute_statement(sqlStatement, commit=False)
            list_network_update.append({
                'table': 'Network',
                'update': {'parentNodeId': df_best_path.iloc[0]['ruleParentNodeId'], 'ordinal': 0},
                'where': {'nodeId': df_best_path.iloc[0]['ruleNodeId'], 'networkName': network_name}
            })
        # self.dl_db.commit_changes()
        return list_network_insert, list_network_update


    def validate_statement_data(self, df_data, df_network):

        


        pass


    def validate_statement_data_old(self, statement_type, data):
        """
        how to get back original data
        - have two copies for every entry?
        - and bool field - original?
        - will have to add it as a composite key.
            - ticker, date, form_type, original 
        """
        if isinstance(data, dict):
            df_data = pd.DataFrame([data])
        else:
            df_data = data

        df_data = df_data.dropna(how="all", axis=1)
        # print(df_data)

        if statement_type == 'IncomeStatement':
            self.validate_IncomeStatement_data(df_data)


    def validate_IncomeStatement_data(self, df_data):
        """IncomeStatement:
        NetIncomeLoss +
            IncomeLossFromContinuingOperationsBeforeIncomeTaxesExtraordinaryItemsNoncontrollingInterest +
                OperatingIncomeLoss +
                    GrossProfit + 
                        RevenueFromContractWithCustomerExcludingAssessedTax + 
                        CostOfGoodsAndServicesSold +      
                    OperatingExpenses + 
                        ResearchAndDevelopmentExpense +
                        Selling General and Administrative
                            SellingAndMarketingExpense + 
                            GeneralAndAdministrativeExpense +
                OtherNonoperatingIncomeExpense +
                InterestIncomeExpenseNonoperatingNet + 
            IncomeTaxExpenseBenefit +
        """
        set_expected_columns = {'NetIncomeLoss', 'OperatingExpenses'}
        if set_expected_columns <= set(df_data.columns):
            print('not all expected columns exist')

        return df_data