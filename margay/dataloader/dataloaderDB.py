import sqlite3
import sys
import glob
import os
from pymongo import MongoClient

import pandas as pd

class DataloaderDB:
    def __init__(self):
        pass


    def create_sqlite3_connection(self, data_base_name = 'local_market.db', store = True):
        sqlite3_conn = sqlite3.connect(os.path.abspath(data_base_name))

        if(store):
            self.sqlite3_conn = sqlite3_conn
        
        return sqlite3_conn

    def create_mongo_connection(self, store = True):
        client = MongoClient('localhost', 27017)
        mongodb = client['TLYS_db']
        mdb_IncomeStatement = mongodb.IncomeStatement
        mdb_BalanceSheet = mongodb.BalanceSheet
        mdb_CashFlow = mongodb.CashFlow

        if (store):
            self.mongodb = mongodb
            self.mdb_IncomeStatement = mdb_IncomeStatement
            self.mdb_BalanceSheet = mdb_BalanceSheet
            self.mdb_CashFlow = mdb_CashFlow

        return mongodb

    def create_all_connection(self):
        return (self.create_sqlite3_connection(), self.create_mongo_connection())


    def smth(self):
        pass


    def sql3_select(self, sql_statement, index_column, sort = False):
        """ Retrieves data from DB for the SQL statement.
        Args:
            sql_statement (String): SQL SELECT statement
            index_column (String): Column to be used as index in Pandas
            sort (Boolean): whether the table should be sorted.
        Returns:
            [pandas.DataFrame]: returns Pandas Dataframe object where the rows are the result of SQL SELECT statement.
        """
        df =  pd.read_sql_query(sql_statement, self.sqlite3_conn)
        if index_column == 'date' and 'date' in df.columns:
            df.date = pd.to_datetime(df.date, format='%Y-%m-%d')
        
        if index_column in df.columns:
            df.set_index(index_column, inplace=True)
            if sort:
                df = df.sort_index()

        return df
    
    def sql3_statement(self, sql_statement, commit = False):
        cur = self.sqlite3_conn.cursor()
        cur.execute(sql_statement)
        cur.close()

        if commit:    
            self.sqlite3_conn.commit()

    def get_rule_node_path(self):
        pass  
