import requests
import pandas as pd
import numpy as np
import sqlite3
import tempfile
import subprocess 
import sys
import glob
import os
from datetime import timedelta, date, datetime
import re
from pandas.tseries.offsets import BDay

# Colourful text
# e.g. print(f'this {Fore.GREEN}color{Style.RESET_ALL}!')
from colorama import Fore
from colorama import Style

#margay
from helper_func import HelperFunc
from margay.util import Util

#connectEdgar
from margay.connectEdgar.stock import Stock
from margay.connectEdgar.financials import FinancialReportEncoder
from margay.connectEdgar.filing import Filing



class DataloaderCore:
    # DataloaderCore
    def __init__(self):
        self.dl_api = DataloaderAPI()
        self.dl_db = DataloaderDB()


    #[{val: 'IBM', opp: '=', name:'ticker'}]
    def db_get_data(self, list_identifier, db_table = 'TimeSeriesDaily', split = True):
        """
            IncomeStatement:
                SELECT * FROM IncomeStatement WHERE ticker = '{identifier}' ORDER BY datetime(date)
            CashFlow
                SELECT * FROM CashFlow WHERE ticker = '{identifier}' ORDER BY datetime(date)
            BalanceSheet
                SELECT * FROM BalanceSheet WHERE ticker = '{identifier}' ORDER BY datetime(date)
            TimeSeriesDaily
                SELECT * FROM TimeSeriesDaily WHERE ticker = '{identifier}' ORDER BY datetime(date)
            Ratios    
                SELECT * FROM RatiosQtr WHERE clusterName = '{identifier}' ORDER BY datetime(date)
                SELECT * FROM RatiosAnn WHERE clusterName = '{identifier}' ORDER BY datetime(date)
            Tickers
                SELECT * FROM Tickers
            Cluster
                SELECT * FROM Cluster WHERE clusterName = '{identifier}'
                SELECT * FROM Cluster
            ClusterMember
                SELECT * FROM ClusterMember WHERE clusterName = '{identifier}'
        """

        if split:
            pass
        else:
            str_list_identifier = ', '.join(f'"{indentifier}"' for indentifier in list_identifier)
            df_db = self.dl_db.sql_select(f"SELECT * FROM {db_table} WHERE ticker IN ({str_list_identifier}) ORDER BY datetime(date)", 'date', True)

        if db_table in ['IncomeStatement', 'BalanceSheet', 'CashFlow']:
            #set data types
            df_db = df_db.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
            df_temp = df_db[df_db.columns.difference(['reportedCurrency', 'reportFrequency', 'ticker'])]
            df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
            df_temp = df_temp.astype(np.float64)
            df_db[df_temp.columns] = df_temp

        # list_df = []
        # for identifier in list_identifier:
        #     if db_table in ['IncomeStatement', 'BalanceSheet', 'CashFlow']:
        #         df_db = self.dl_db.sql_select(f"SELECT * FROM {db_table} WHERE ticker = '{identifier}' ORDER BY datetime(date)")
        #         if df_db.size == 0:
        #             print(f"WARNING; db_get_data(tickers, data_type = 'TimeSeriesDaily'); DB has no data for ticker {ticker}")
        #             continue
                
        #         df_db = df_db.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
        #         df_temp = df_db[df_db.columns.difference(['reportedCurrency', 'reportFrequency', 'ticker'])]
        #         df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
        #         df_temp = df_temp.astype(np.float64)
        #         df_db[df_temp.columns] = df_temp
        #     # elif db_table in ['RatioQtr']:
        #     #     df_db = self.dl_db.sql_select(f"SELECT * FROM {db_table} WHERE clusterName = '{identifier}' ORDER BY datetime(date)")
        #     #     if df_db.size == 0:
        #     #         print(f"WARNING; db_get_data(tickers, data_type = 'TimeSeriesDaily'); DB has no data for ticker {ticker}")
        #     #         continue 

        return df_db


    def db_update_data(self):
        pass


    def get_data(self, tickers, data_type = 'TimeSeriesDaily', update_data = False):
        #not safe against SQL injection
        list_df = []

        for ticker in tickers:
            df_db = self.dl_db.sql_select(f"SELECT * FROM {data_type} WHERE ticker = '{ticker}' ORDER BY datetime(date)", 'date', True)
            
            if df_db.size > 0 and update_data:
                print(df_db.iloc[-1].name.date())
                df_api = self.dl_api.alphavantage(ticker, data_type)
                
                df_new = df_api.loc[df_db.index[-1].date() + timedelta(days=1):]

                df_new.apply(lambda x: self.dl_db.pandas_insert(ticker, x, data_type, False), axis=1)
                self.dl_db.commit_changes()
                df_db = pd.concat([df_db, df_new]) 
            elif df_db.size == 0 :
                df_api = self.dl_api.alphavantage(ticker, data_type)
                df_api.apply(lambda x: self.dl_db.pandas_insert(ticker, x, data_type, False), axis=1)
                self.dl_db.commit_changes()
                df_db = df_api
 
            if data_type in ['IncomeStatement', 'BalanceSheet', 'CashFlow']:
                #set data types
                df_db = df_db.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
                df_temp = df_db[df_db.columns.difference(['reportedCurrency', 'reportFrequency', 'ticker'])]
                df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
                df_temp = df_temp.astype(np.float64)
                df_db[df_temp.columns] = df_temp

            list_df.append(df_db)
        return list_df
        
    def get_income_statements(self, ticker):
        pass

    def get_cash_flow(self, ticker):
        pass

    def get_balance_sheet(self, ticker):
        pass

    def pd_sql_select(self, statement, index_column, sort):
        return self.dl_db.sql_select(statement, index_column, sort)

    def pd_df_insert(self, df):
        pass

    def pd_series_insert(self, df):
        pass

    def var_insert(self, table_name, fields, commit):
        self.dl_db.var_insert(table_name, fields, commit)

    def commit_changes(self):
        self.dl_db.commit_changes()

class DataloaderAPI:
    def __init__(self):
        self.api_data = {}
        pass

    def set_api_key(self, name, key):
        if(not key in self.api_data):
            self.api_data[key] = {}

        self.api_data[name]['key'] = key


    def alphavantage(self, ticker, data_type = 'TimeSeriesDaily'):
        print(f'dl_api.alphavantage - ticker:{ticker}, data_type:{data_type}')

        func = ''
        if data_type == 'TimeSeriesDaily':
            func = 'TIME_SERIES_DAILY_ADJUSTED'
        elif data_type == 'IncomeStatement':
            func = 'INCOME_STATEMENT'
        elif data_type == 'BalanceSheet':
            func = 'BALANCE_SHEET'
        elif data_type == 'CashFlow':
            func = 'CASH_FLOW'

        my_api_data = {}
        if 'alphavantage' in self.api_data:
            my_api_data = self.api_data['alphavantage']
        
        API_URL = "https://www.alphavantage.co/query" 
        api_key = 'VF8PBNPTSXB8RJLH' #VF8PBNPTSXB8RJLH
        if 'key' in my_api_data:
            api_key = my_api_data['api']
        
        # listDF = []
 
        data = { 
            "function": func, 
            "symbol": ticker,
            "outputsize" : "full",
            "datatype": "json", 
            "apikey": api_key} 

        response = requests.get(API_URL, data) 
        response_json = response.json() # maybe redundant
        
        # df.sort_index()

        if func == 'TIME_SERIES_DAILY' :
            df = pd.DataFrame.from_dict(response_json['Time Series (Daily)'], orient= 'index')
            df.index =  pd.to_datetime(df.index, format='%Y-%m-%d')
            df = df.sort_index()

            df = df.rename(columns={ '1. open': 'Open', '2. high': 'High', '3. low': 'Low', '4. close': 'Close', '5. volume': 'Volume'})
            df = df.astype({'Open': 'float64', 'High': 'float64', 'Low': 'float64','Close': 'float64','Volume': 'float64',})
            df = df[[ 'Open', 'High', 'Low', 'Close', 'Volume']]
        elif func == 'TIME_SERIES_DAILY_ADJUSTED':
            df = pd.DataFrame.from_dict(response_json['Time Series (Daily)'], orient= 'index')
            df.index =  pd.to_datetime(df.index, format='%Y-%m-%d')
            df = df.sort_index()

            df = df.rename(columns={ 
                '1. open': 'open', 
                '2. high': 'high', 
                '3. low': 'low', 
                '4. close': 'close', 
                '5. adjusted close': 'adjusted_close', 
                '6. volume': 'volume',
                '7. dividend amount': 'dividend_amount',
                '8. split coefficient': 'split_coefficient'})
            df = df.astype({
                'open': 'float64', 
                'high': 'float64', 
                'low': 'float64',
                'close': 'float64',
                'adjusted_close': 'float64',
                'volume': 'float64',
                'dividend_amount': 'float64',
                'split_coefficient': 'float64'})
            df = df[[ 'open', 'high', 'low', 'close', 'adjusted_close', 'volume', 'dividend_amount', 'split_coefficient']]
        elif func in ['INCOME_STATEMENT', 'BALANCE_SHEET', 'CASH_FLOW']:
            #get the data and put it into a df
            df_annual = pd.DataFrame(response_json['annualReports'])
            df_annual['reportFrequency'] = 'annual'
            df_quarterly = pd.DataFrame(response_json['quarterlyReports'])
            df_quarterly['reportFrequency'] = 'quarterly'
            df = pd.concat([df_annual, df_quarterly])

            #set index
            df.set_index('fiscalDateEnding', inplace=True)
            df.index =  pd.to_datetime(df.index, format='%Y-%m-%d')
            df = df.sort_index()

            #set data types
            df = df.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
            df_temp = df[df.columns.difference(['reportedCurrency', 'reportFrequency'])]
            df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
            df_temp = df_temp.astype(np.float64)
            df[df_temp.columns] = df_temp
        else:
            print('else statement')
            
            # df = df.rename(columns={ 
            #     '5. adjusted close': symbol, })
            # df = df.astype({
            #     symbol: 'float64',})
            # listDF.append(df[[symbol]])

        #result_df = pd.concat(listDF, axis=1, sort=True)
        #return result_df
        return df

    def edgar(self):
        pass


class DataloaderDB:
    def __init__(self, data_base_name = 'local_market.db'):
        print('DataloaderDB:__init__:startedLoading')

        self.helpers = HelperFunc()

        self.conn = sqlite3.connect(os.path.abspath(data_base_name))
        #self.cur = self.conn.cursor()

        self.renew_local_network_df('all')
        
        df = self.df_incomeStatement_element
        print(f'{Fore.YELLOW}soi-cal: ')
        print(df.loc[(df['originalParentNodeId'].isnull()) & (df['parentNodeId'] == 'soi-cal')].index.tolist())

        df = self.df_balanceSheet_element
        print(df.loc[(df['originalParentNodeId'].isnull()) & (df['parentNodeId'] == 'sfp-cls-cal')].index.tolist())

        df = self.df_cashFlow_element
        print(df.loc[(df['originalParentNodeId'].isnull()) & (df['parentNodeId'] == 'scf-indir-cal')].index.tolist())
        print(f'{Style.RESET_ALL}')

        print('DataloaderDB:__init__:finishedLoading')

        client = MongoClient('localhost', 27017)
        self.mongodb = client['TLYS_db']
        self.mdb_IncomeStatement = self.mongodb.IncomeStatement
        self.mdb_BalanceSheet = self.mongodb.BalanceSheet
        self.mdb_CashFlow = self.mongodb.CashFlow
        

    def renew_local_network_df(self, network='all'):
        
        if network == 'IncomeStatement' or network == 'all':
            sql_statement = f'''
                WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, path) as (
                    SELECT nodeId, parentNodeId, originalParentNodeId, nodeId from Network WHERE parentNodeId = 'soi-cal' and networkName = 'soi-cal'
                    
                    UNION
                    
                    SELECT Network.nodeId, Network.parentNodeId, Network.originalParentNodeId, paths.path || '/' || Network.nodeId
                    FROM Network 
                    JOIN paths
                    WHERE Network.parentNodeId = paths.nodeId and networkName = 'soi-cal'
                )
                SELECT nodeId, parentNodeId, originalParentNodeId, path, Element.balance, Element.elementName, Element.label FROM paths
                LEFT JOIN Element
                ON paths.nodeId = Element.elementId;
            '''
            self.df_incomeStatement_element = self.sql_select(sql_statement, index_column = 'nodeId')


        if network == 'BalanceSheet' or network == 'all':
            sql_statement = f'''
                WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, path) as (
                    SELECT nodeId, parentNodeId, originalParentNodeId, nodeId from Network WHERE parentNodeId = 'sfp-cls-cal' and networkName = 'sfp-cls-cal'
                    
                    UNION
                    
                    SELECT Network.nodeId, Network.parentNodeId, Network.originalParentNodeId, paths.path || '/' || Network.nodeId
                    FROM Network 
                    JOIN paths
                    WHERE Network.parentNodeId = paths.nodeId and networkName = 'sfp-cls-cal'
                )
                SELECT nodeId, parentNodeId, originalParentNodeId, path, Element.balance, Element.elementName, Element.label FROM paths
                LEFT JOIN Element
                ON paths.nodeId = Element.elementId;
            '''
            self.df_balanceSheet_element = self.sql_select(sql_statement, index_column = 'nodeId')


        if network == 'CashFlow' or network == 'all':        
            sql_statement = f'''
                WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, path) as (
                    SELECT nodeId, parentNodeId, originalParentNodeId, nodeId from Network WHERE parentNodeId = 'scf-indir-cal' and networkName = 'scf-indir-cal'
                    
                    UNION
                    
                    SELECT Network.nodeId, Network.parentNodeId, Network.originalParentNodeId, paths.path || '/' || Network.nodeId
                    FROM Network 
                    JOIN paths
                    WHERE Network.parentNodeId = paths.nodeId and networkName = 'scf-indir-cal'
                )
                SELECT nodeId, parentNodeId, originalParentNodeId, path, Element.balance, Element.elementName, Element.label FROM paths
                LEFT JOIN Element
                ON paths.nodeId = Element.elementId;
            '''
            self.df_cashFlow_element = self.sql_select(sql_statement, index_column = 'nodeId')

        sql_statement = f'''SELECT * FROM Element;'''
        self.df_element = self.sql_select(sql_statement, index_column = 'elementId')


    def close_db(self):
        #self.cur.close()
        self.conn.close()

    def sql_select(self, sql_statement, index_column, sort = False):
        """ Retrieves data from DB for the SQL statement.
        Args:
            sql_statement (String): SQL SELECT statement
            index_column (String): Column to be used as index in Pandas
            sort (Boolean): whether the table should be sorted.
        Returns:
            [pandas.DataFrame]: returns Pandas Dataframe object where the rows are the result of SQL SELECT statement.
        """
        df =  pd.read_sql_query(sql_statement, self.conn)
        if index_column == 'date' and 'date' in df.columns:
            df.date = pd.to_datetime(df.date, format='%Y-%m-%d')
        
        if index_column in df.columns:
            df.set_index(index_column, inplace=True)
            if sort:
                df = df.sort_index()

        
        #df = df[[ 'open', 'high', 'low', 'close', 'adjusted_close', 'volume', 'dividend_amount', 'split_coefficient']]
        return df


    def var_select(self, table_name, conditions):
        # self.cur.execute("select * from airlines limit 5;")
        # results = self.cur.fetchall()

        #df = pd.read_sql_query(statement, self.conn)
        #return df
        pass

    def sql_insert(self, statement, commit):
        #cur.execute("insert into airlines values (6048, 19846, 'Test flight', '', '', null, null, null, 'Y')")
        # values = ('Test Flight', 'Y')
        # cur.execute("insert into airlines values (6049, 19847, ?, '', '', null, null, null, ?)", values)
        #self.cur.execute(statement)

        cur = self.conn.cursor()
        cur.execute(statement)
        cur.close()

        if commit:    
            self.conn.commit()

    def sql_execute_statement(self, statement, commit):
        #cur.execute("insert into airlines values (6048, 19846, 'Test flight', '', '', null, null, null, 'Y')")
        # values = ('Test Flight', 'Y')
        # cur.execute("insert into airlines values (6049, 19847, ?, '', '', null, null, null, ?)", values)
        #self.cur.execute(statement)

        cur = self.conn.cursor()
        cur.execute(statement)
        cur.close()

        if commit:    
            self.conn.commit()
        
    def var_update(self, table_name, dict_update, commit = False):
        """Updates multiple rows for a specified table

        Args:
            table_name (str): Name of table where the rows exist 
            dict_update (dict of dict): dict where keys are row Id, and value is a dict whose 
                                        keys are fields and values is the new data 
            commit (bool, optional): Whether to commit the changes or not, default False.
        """

        cur = self.conn.cursor()
        
        for row_id in dict_update:
            sqlStatement = f'UPDATE {table_name} SET'
            sql_update = ''
            whereClause = f'WHERE elementId = "{row_id}"'

            for field in dict_update[row_id]:
                sql_update += f'{field} = '
                if type(dict_update[row_id][field]) is str:
                    sql_update += f'"{dict_update[row_id][field]}", '
                elif dict_update[row_id][field] is None:
                    sql_update += 'null, '
                else:
                    sql_update += f'{dict_update[row_id][field]}, '

            sql_update = sql_update[:-2] 
                
            sqlStatement += ' ' + sql_update + ' ' + whereClause + ';'
            cur.execute(sqlStatement)
        
        cur.close()

        if commit:    
            self.conn.commit()



    def var_insert(self, table_name, list_insert, commit = False, fail_on_unique=True):
        #cur.execute("insert into airlines values (6048, 19846, 'Test flight', '', '', null, null, null, 'Y')")
        # values = ('Test Flight', 'Y')
        # cur.execute("insert into airlines values (6049, 19847, ?, '', '', null, null, null, ?)", values)
        #self.cur.execute(statement)

        cur = self.conn.cursor()

        for new_row in list_insert:
            sql_statement = f'INSERT INTO {table_name} '
            sqlFields = '('
            sqlValues = '('

            for field in new_row.keys():
                sqlFields += field + ', '

                if type(new_row[field]) is str:
                    sqlValues += f'"{new_row[field]}", '
                elif new_row[field] is None:
                    sqlValues += 'NULL, '
                else:
                    sqlValues += f'{new_row[field]}, '

            sqlFields = sqlFields[:-2] + ')'
            sqlValues = sqlValues[:-2] + ')'
            sql_statement += sqlFields + ' VALUES ' + sqlValues + ';'

            # if fail_on_unique:
            #     cur.execute(sql_statement)
            # else:
            try:
                cur.execute(sql_statement)
            except sqlite3.Error as err:
                # print(f'DataloaderDB:var_insert:error: {err}   -   SQL: {sql_statement}')
                if fail_on_unique:
                    print('DataloaderDB:var_insert:failed on:', sql_statement)
                    raise err                
                else:
                    pass

        cur.close()

        if commit:    
            self.conn.commit()

    def pandas_insert(self, ticker, df_row, table, commit = False, or_replace = False):
        #cur.execute("insert into airlines values (6048, 19846, 'Test flight', '', '', null, null, null, 'Y')")
        # values = ('Test Flight', 'Y')
        # cur.execute("insert into airlines values (6049, 19847, ?, '', '', null, null, null, ?)", values)
        cur = self.conn.cursor()

        sql_statement = ''
        if or_replace:
            sql_statement += 'INSERT OR REPLACE '
        else:
            sql_statement += 'INSERT '
        sql_statement += f'INTO {table} '

        temp_date = df_row.name.strftime('%Y-%m-%d')
        sql_fields = f'(ticker, date, '
        sql_data = f'("{ticker}", "{temp_date}", '

        list_column_names = list(df_row.index)
        for colName in list_column_names:
            sql_fields += f'{colName}, '
            if type(df_row[colName]) == str or np.isnan(df_row[colName]):
                sql_data += f'"{df_row[colName]}", '
            else:
                sql_data += f'{df_row[colName]}, '
        sql_fields = sql_fields[:-2] + ')' 
        sql_data = sql_data[:-2] + ')' 

        sql_statement += f'{sql_fields} VALUES {sql_data}'
        cur.execute(sql_statement)
        cur.close()

        if commit:
            self.conn.commit()
        
    def commit_changes(self):
        self.conn.commit()

    def update(self, statement):
        # values = ('USA', 19847)
        # cur.execute("update airlines set country=? where id=?", values)     
        # self.conn.commit()
        pass

    def delete(self, statement):
        # need to delete Ticker 'I' from TimeSeriesPeriod
        # cur = conn.cursor()
        # values = (19847, )
        # ur.execute("delete from airlines where id=?", values)conn.commit()
        pass

    def sql_create_table(self, table_name, field_names, field_types):
        # cur.execute("create table daily_flights (id integer, departure date, arrival date, number text, route_id integer)")conn.commit()
        cur = self.conn.cursor()
 
        table_name = self.helpers.sql_safe_string(table_name)
        str_fields = '('
        for field_name, field_type in zip(field_names, field_types):
            str_fields += (
                field_name + 
                ' ' + 
                field_type + 
                ', ')
        str_fields = str_fields[:-2] + ')'
        #(id integer, departure date, arrival date, number text, route_id integer)"

        sql_statement = f"create table {table_name} {str_fields};"
        cur.execute(sql_statement)
        self.conn.commit()
 
        cur.close()
    
    def pandas_create_table(self, df):
        # df.to_sql("daily_flights", conn, if_exists="replace")
        pass

    def sql_alter_table(self, df):
        # cur.execute("alter table airlines add column airplanes integer;")
        # no need to commit alterations
        pass

    def pandas_alter_table(self, df):
        # df = pd.read_sql("select * from daily_flights", conn)
        # df["delay_minutes"] = None
        # df.to_sql("daily_flights", conn, if_exists="replace")
        pass


    def insert_or_replace(self, row):
        # code
        pass

    def get_table_columns(self, table_name):
        """Returns a dictionary of columns for a specificed table

        Args:
            table_name (String): Name of a table in the Database

        Returns:
            dict: dictionary of column names in the table.
        """
        cursor = self.conn.execute(f'SELECT * FROM {table_name} LIMIT 1')
        dict_curr_column = {}
        for x in cursor.description:
            dict_curr_column[x[0]] = x[0]
        cursor.close()

        return dict_curr_column


    def update_schema(self, statement_type):
        """Based on the rows in StatementElement Table this function will add columns to IncomeStatement, BalanceSheet table.

        Args:
            statement_type ([type]): [description]
        """
               
        if statement_type not in ['IncomeStatement', 'BalanceSheet', 'CashFlow']:
            return

        # currently only supports IncomeStatement and BalanceSheet, to support CashFlow in the future.
        if statement_type == 'IncomeStatement':
            df_element = self.df_incomeStatement_element
        elif statement_type == 'BalanceSheet':
            df_element = self.df_balanceSheet_element
        elif statement_type == 'CashFlow':
            df_element = self.df_cashFlow_element
        
        # Gets existing column names of the table
        dict_curr_column = self.get_table_columns(statement_type)

        # iterate over elements in StatementElement
        # if the StatementElement.elementName doesn't exist as column in statement_type Table
        # then add it.
        for row in df_element.itertuples():
            if row.elementName not in dict_curr_column:
                self.conn.execute(f'ALTER TABLE {statement_type} ADD COLUMN {row.elementName} REAL;')


    def schema_add_column(self, table_name, list_columns, exception_on_duplicate = True):
        """Add column to a specified table

        Args:
            table_name (str): Name of table to add the columns to
            list_columns (list -> dict): List of dictionaries, where the dictionary contains name and data type of column
        """

        # make sure none of columns already exist in the table
        # otherwise it might add half of them and not the rest.
        list_new_column = []

        dict_curr_column = self.get_table_columns(table_name)


        for new_column in list_columns:
            if new_column['name'] not in dict_curr_column:
                list_new_column.append(new_column)
            else:
                if exception_on_duplicate:
                    raise Exception(f'DataloaderDB:schema_add_column: tried to add a column {new_column}, but it already exists in {table_name}. Other columns to be added: {list_columns}.')

        for new_column in list_new_column:
            try:
                self.conn.execute(f'ALTER TABLE {table_name} ADD COLUMN {new_column["name"]} {new_column["data_type"]};')
            except sqlite3.Error as err:
                print(f'DataloaderDB:schema_add_column:failed to add {new_column} to {table_name}')
                raise err


    def insert_financial_record(self, statement_type, list_record, all = True):
        # list_record = [{}]

        # iterate over list_record
        # get any existing records that match new record
        # if the list is not empty then return
        # if the list is not empty iterate over list_record
        #   if the item from list_record is in the list, then do not add that item
        #   otherwise, add it.

        # adding items:
        #   add all records to  md
        #   get the result ids
        #   add the records to sqldb with corresponding mdb record id.
        
        set_composite_key = set()
        for new_record in list_record:
            ticker = new_record['ticker']
            date = new_record['date']
            filingType = new_record['filingType']
            dataType = new_record['dataType']
            source = new_record['source'] #not part of key, but required field

            if not (ticker and date and filingType and dataType and source):
                continue
            set_composite_key.add(ticker + date + filingType + dataType)

        sql_select_where = ','.join(f"'{key}'" for key in set_composite_key)
        sq_select_statement = f'''
            SELECT 
                (ticker || date || filingType || dataType) as compositeKey
            FROM 
                {statement_type} 
            WHERE
                compositeKey IN ({sql_select_where})
        '''

        df_existing_record = self.sql_select(sq_select_statement, index_column = 'compositeKey')
        
        list_new_record = []
        for new_record in list_record:
            ticker = new_record['ticker']
            date = new_record['date']
            filingType = new_record['filingType']
            dataType = new_record['dataType']
            # check if we already have this edge in DB then we don't have to add it
            if (ticker + date + filingType + dataType) in df_existing_record.index:
                continue

            list_new_record.append(new_record)

        if len(list_new_record) == 0:
            print('insert_financial_record:no new records inserted')
            return

        mdb_insert_result = self.mongodb[statement_type].insert_many(list_new_record)

        list_print_new_record = []
        for new_record, mdb_record_id in zip(list_new_record, mdb_insert_result.inserted_ids):
            self.var_insert(statement_type, [{
                "ticker": new_record['ticker'], 
                'date': new_record['date'], 
                'filingType': new_record['filingType'],
                'dataType': new_record['dataType'],
                'source': new_record['source'],
                'mongodbId': str(mdb_record_id)
            }], False)
                
            list_print_new_record.append(new_record)
        
        self.commit_changes()
        print('insert_financial_record:')
        print(list_print_new_record)



    def filter_out_existing_record(self, statement_type, list_record):
        pass

    def in_db(self, statement_type, record):

        pass


class DataloaderFile:
    """[summary]

    TODO:
        - Export StatementElement to JSON
        - Import JSON into StatementElement

        - Output Fundamental data into presentable format in JSON
        - Import data from JSON.
    """


    def __init__(self):
        self.dl_db = DataloaderDB()

    def export_to_csv(self, df, file_name, exe_path="/usr/bin/libreoffice"):
        """Open dataframe df in excel.

        exe_path - path to the program that will open the csv after it has been saved
        index=True - export the index of the dataframe as the first columns
        tmp_path    - directory to save the file in


        This creates a temporary file name, exports the dataframe to a csv of that file name,
        and then tells excel to open the file (in read only mode). (It uses df.to_csv instead
        of to_excel because if you don't have excel, you still get the csv.)

        Note - this does NOT delete the file when you exit. 
        """

        # df.to_csv(tmp_name, index=index)
        # cmd=[excel_path, '/r', '/e', tmp_name]
        df.to_csv(file_name + '.csv', index=True)
        # cmd=[excel_path, tmp_name]
        cmd=[exe_path, file_name + '.csv']
        
        try:
            ret_val=subprocess.Popen(cmd).pid
        except:
            print("open_in_excel(): failed to open excel")
            print("filename = ", file_name + '.csv')
            print("command line = ", cmd)
            print("Unexpected error:", sys.exc_info()[0])

        return



    def import_from_csv(self, file_name, table_name, save_to_db = False):
        df = pd.read_csv(file_name)
        if df.size > 0:
            df.set_index('date', inplace=True)
            df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
            df.sort_index(inplace=True)

            #potentially want to change this later, keep the ticker column in the table.
            # since, this creates two versions of the df
            # it currently works like this because alpha_vantage doesn't have the ticker in dictionary
            ticker = df['ticker'][0]
            df_to_import = df.drop(['ticker'], axis=1)
            
            if save_to_db:
                # ticker, df_row, table, commit = False, or_replace = False
                df_to_import.apply(lambda row: self.dl_db.pandas_insert(ticker, row, table_name, commit=False, or_replace=True), axis=1)
                self.dl_db.commit_changes()
            
            return df




from pymongo import MongoClient

class DataloaderMongoDB:
    def __init__(self):
        client = MongoClient('localhost', 27017)
        self.db = client['TLYS_db']
        self.IncomeStatement = self.db.IncomeStatement
        self.BalanceSheet = self.db.BalanceSheet
        self.CashFlow = self.db.CashFlow




from margay.connectEdgar.stock import Stock
from margay.connectEdgar.financials import FinancialReportEncoder
from margay.index_edgar_filing import download_index

class DataloaderEdgar:
    """[summary]
    TODO:
        - Need to check if data about to be inserted has the column in the Table
        - Validate Statement Rules
            - e.g. Revenue + Cost of Revenue = Gross profit.
    """

    def __init__(self):
        self.dl_db = DataloaderDB()
        self.dl_mongo = DataloaderMongoDB()


    def validate_statement_data(self, statement_type, data):
        """
        how to get back original data
        - have two copies for every entry?
        - and bool field - original?
        - will have to add it as a composite key.
            - ticker, date, form_type, original 
        """
        if isinstance(data, dict):
            df_data = pd.DataFrame([data])
        else:
            df_data = data

        df_data = df_data.dropna(how="all", axis=1)
        # print(df_data)

        if statement_type == 'IncomeStatement':
            self.validate_IncomeStatement_data(df_data)


    def validate_IncomeStatement_data(self, df_data):
        """IncomeStatement:
        NetIncomeLoss +
            IncomeLossFromContinuingOperationsBeforeIncomeTaxesExtraordinaryItemsNoncontrollingInterest +
                OperatingIncomeLoss +
                    GrossProfit + 
                        RevenueFromContractWithCustomerExcludingAssessedTax + 
                        CostOfGoodsAndServicesSold +      
                    OperatingExpenses + 
                        ResearchAndDevelopmentExpense +
                        Selling General and Administrative
                            SellingAndMarketingExpense + 
                            GeneralAndAdministrativeExpense +
                OtherNonoperatingIncomeExpense +
                InterestIncomeExpenseNonoperatingNet + 
            IncomeTaxExpenseBenefit +
        """
        set_expected_columns = {'NetIncomeLoss', 'OperatingExpenses'}
        if set_expected_columns <= set(df_data.columns):
            print('not all expected columns exist')

        return df_data


    def check_rules_against_network(self, network_name):
        sql_select_network_rules = f'''
            WITH RECURSIVE paths(nodeId, parentNodeId, originalParentNodeId, weight path) as (
                SELECT nodeId, parentNodeId, originalParentNodeId, weight, nodeId from Network WHERE parentNodeId = '{network_name}' and networkName = '{network_name}'
                
                UNION
                
                SELECT 
                    Network.nodeId, 
                    Network.parentNodeId, 
                    Network.originalParentNodeId, 
                    Network.weight, 
                    paths.path || '/' || Network.nodeId
                FROM Network 
                JOIN paths
                WHERE Network.parentNodeId = paths.nodeId and networkName = '{network_name}'
            )
            SELECT 
                NetworkRollUpRule.nodeId, 
                NetworkRollUpRule.parentNodeId, 
                NetworkRollUpRule.weight, 
                paths.path, 
                paths.parentNodeId as networkParentNodeId,
                paths.weight as networkWeight
            FROM NetworkRollUpRule
            LEFT JOIN paths
            ON paths.nodeId = NetworkRollUpRule.nodeId;
        '''
        df_network_rule = self.dl_db.sql_select(sql_select_network_rules, index_column = 'nodeId')
        df_diff_weight = df_network_rule[df_network_rule['weight'] != df_network_rule['networkWeight']]
        if df_diff_weight.shape[0] > 0:
            raise Exception('There has been change in weight - it should not change!!!!')

        # get a list of nodes without path, meaning they haven't been addded to network
        df_new_node = df_network_rule[df_network_rule['path'].isnull()]
        df_new_node_unique = df_new_node[~df_new_node['nodeId'].duplicated()]
        list_new_node = []
        for index, row in df_new_node.iterrrows():
            df_node_rule = df_new_node[df_new_node['nodeId'] == row['nodeId']]
            
            
            list_new_node.append({
                'nodeId': row['nodeId'],
                'parentNodeId': row['parentNodeId'],
                'networkName': network_name,
                'weight': row['weight'],
                'elementId': row['nodeId'],
                'originalParentNodeId': None
            })
        self.dl_db.var_insert('Network', list_new_node) #TODO - does this satisfy other rules??
        
        # get a df of nodes with path, meaning they are in a network
        # and should be checked if the parentNodeId is in path,
        # meaning nodeId/parenotNodeId rule is satisfied by the network
        df_existing_node = df_network_rule[df_network_rule['path'].notnull()]
        # list comprehension is faster than df.apply(.) by x2
        df_existing_node['satisfied'] = [parent in path for parent, path in zip(df_existing_node['parentNodeId'], df_existing_node['path'])]
        df_failed_rule = df_existing_node[df_existing_node['satisfied'] == False]

        for index, row in df_failed_rule.iterrrows():
            df_network_rule







        self.dl_db.var_insert('NetworkRollUpRule', [{
                "nodeId": new_edge['nodeId'], 
                'parentNodeId': new_edge['parentNodeId'], 
                'networkName': new_edge['networkName'],
                'weight': new_edge['weight'],
                'source': new_edge['source']
            }], False)




        # there will be new rules that have no been inserted into network, so they will have no path.
        # e.g. company specific non-gaap elements
        df_network_rule['path'] = df_network_rule['path'].fillna('')
        



    def update_network_node(self, nodeId, networkName, parentNodeId, ordinal = 0, commit = False):
        """Update node in a network
        TODO: Remove node from network - need to connect parent to children

        Args:
            nodeId ([type]): [description]
            parentNodeId ([type]): [description]
            ordinal ([type]): [description]
        """
        sqlStatement = f'''
            UPDATE Network 
            SET 
                parentNodeId = "{parentNodeId}",
                ordinal = "{ordinal}",
                originalParentNodeId = parentNodeId 
            WHERE
                nodeId = "{nodeId}" and
                networkName = "{networkName}";
        '''
        self.dl_db.sql_execute_statement(sqlStatement, commit)

    
    def edgar_into_db(self, ticker, statement_type, form_type = '10-K', year=2014):        
        df_filing = self._get_df_sec_filing(ticker = ticker, year=year)
        

        for path in df_filing['filingFilePath']:
            filing = Filing(company=ticker, url= 'https://www.sec.gov/Archives/' + path)

            if 'FilingSummary.xml' not in filing.documents:
                # TODO: not sure under what conditions this happens, but it does happen
                # Need to investigate
                print(f'{Fore.YELLOW}Could not find FilingSummary for Filing: {ticker}, {path} {Style.RESET_ALL}')
                continue
            
            filing_elements = filing.get_filing_elements()
            self._insert_elements_to_DB_from_soup(ticker, filing_elements, path)

            if statement_type == 'all':
                filing_cal = filing.get_filing_cal_network('IncomeStatement')
                self._insert_network_rules_to_DB_from_soup(filing_cal, 'soi-cal', path)
                filing_cal = filing.get_filing_cal_network('BalanceSheet')
                self._insert_network_rules_to_DB_from_soup(filing_cal, 'sfp-cls-cal', path)
                filing_cal = filing.get_filing_cal_network('CashFlow')
                self._insert_network_rules_to_DB_from_soup(filing_cal, 'scf-indir-cal', path)

                # self._insert_sec_filing_data_into_db(filing, ticker, 'IncomeStatement', form_type)
                # self._insert_sec_filing_data_into_db(filing, ticker, 'BalanceSheet', form_type)
                # self._insert_sec_filing_data_into_db(filing, ticker, 'CashFlow', form_type)

                self._insert_sec_filing_data_into_mongo_db(filing, ticker, 'IncomeStatement', path, form_type)
                self._insert_sec_filing_data_into_mongo_db(filing, ticker, 'BalanceSheet', path, form_type)
                self._insert_sec_filing_data_into_mongo_db(filing, ticker, 'CashFlow', path, form_type)
            else:
                network_name = ''
                if statement_type == 'IncomeStatement':
                    network_name = 'soi-cal'
                elif statement_type == 'BalanceSheet':
                    network_name = 'sfp-cls-cal'
                elif statement_type == 'CashFlow':
                    network_name = 'scf-indir-cal'

                filing_cal = filing.get_filing_cal_network(statement_type)
                self._insert_network_rules_to_DB_from_soup(filing_cal, network_name, path)
                # self._insert_sec_filing_data_into_db(filing, ticker, statement_type, form_type)
                # self._insert_sec_filing_data_into_mongo_db(filing, ticker, statement_type, path, form_type)
            

    
    def _insert_network_rules_to_DB_from_soup(self, soup, network_name, source):
        # get a list of elemnts used in this calculation network
        # store them in a dictionary with corresponding us-gaap element id
        dict_link_loc = {}
        list_soup_link_loc = soup.find_all('link:loc')
        for link_loc in list_soup_link_loc:
            link_loc_label = link_loc.get('xlink:label')
            link_loc_href = link_loc.get('xlink:href')
            _, element_id = link_loc_href.split('#')
            dict_link_loc[link_loc_label] = element_id.lower()


        # process calculationArc(s) and insert them into DB.
        set_new_edge_id = set()
        list_new_edge = []
        list_soup_calc_arc = soup.find_all('link:calculationarc')

        for link_arc in list_soup_calc_arc:
            # the data we need from the calculationArc.
            link_from = link_arc.get('xlink:from')
            link_to = link_arc.get('xlink:to')
            link_weight = link_arc.get('weight')
            
            # check if has the required variables.
            if link_from and link_to:
                link_from = dict_link_loc[link_from]
                link_to = dict_link_loc[link_to]
            else:
                continue

            set_new_edge_id.add(link_to + link_from) # child + parent

            list_new_edge.append({
                "nodeId": link_to, 
                'parentNodeId': link_from, 
                'networkName': network_name,
                'weight': link_weight,
                'source': source
            })

        # get existing edges from the database that are same as the edges we're about to add.
        sql_statement_where_edge_id = ','.join(f"'{edge_id}'" for edge_id in set_new_edge_id)
        sql_statement = f'''
            SELECT 
                (nodeId || parentNodeId) as edgeId
            FROM 
                NetworkRollUpRule 
            WHERE 
                edgeId IN ({sql_statement_where_edge_id})
        '''
        df_existing_edge = self.dl_db.sql_select(sql_statement, index_column = 'edgeId')
        
        list_print_new_edge = []
        for new_edge in list_new_edge:
            # check if we already have this edge in DB then we don't have to add it
            if new_edge['nodeId'] + new_edge['parentNodeId'] in df_existing_edge.index:
                continue

            self.dl_db.var_insert('NetworkRollUpRule', [{
                "nodeId": new_edge['nodeId'], 
                'parentNodeId': new_edge['parentNodeId'], 
                'networkName': new_edge['networkName'],
                'weight': new_edge['weight'],
                'source': new_edge['source']
            }], False)
                
            list_print_new_edge.append(new_edge)
        
        self.dl_db.commit_changes()
        print('_insert_network_rules_to_DB_from_soup:')
        print(list_print_new_edge)



    def _insert_elements_to_DB_from_soup(self, ticker, list_new_elements, source):
        """Given list of soup (BS4 object) insert elements into DB

        Args:
            ticker (str): ticker of the company
            list_new_elements (bs4.element.ResultSet): List of soup objects, the soup contains info about element.
            source (str): source of the soup, source of the elements
        """
        list_new_element = []
        set_new_element_id = set()

        for element in list_new_elements:
            element_name = element.get('name')
            filing_element_id = element.get('id')
            abstract = element.get('abstract')
            if type(abstract) is str:
                abstract = bool(abstract)

            prefix, _ = filing_element_id.split('_')

            # if the prefix is the same as company's ticker, then set it to non-gaap.
            # is prefix is the same ticker that means it's not part of us-gaap.
            # TODO - potentially we don't have to reference ticker, we can just check if prefix is not 'us-gaap'
            if prefix.lower() == ticker.lower():
                # non-gaap_CloudInfrastructure-As-A-Service - from ORCL 2014 IncomeStatement
                element_name = element_name.replace('-', '')
                # unsure about changng prefix to non-gaap.
                # element_id = 'non-gaap_' + element_name
                element_id = filing_element_id
                prefix = 'non-gaap'
            else:
                element_id = filing_element_id

            element_id = element_id.lower()

            # make sure we are not trying to add elements with same id.
            if element_id in set_new_element_id:
                print(f'{Fore.YELLOW}_add_elements_to_DB:Attempted to add elements with same id: {element_id} {Style.RESET_ALL}')
                continue
            set_new_element_id.add(element_id)

            list_new_element.append({
                "elementId": element_id, 
                'elementName': element_name, 
                'prefix': prefix,
                'source': source,
                'balance': element.get('xbrli:balance'),
                'periodType': element.get('xbrli:periodType'),
                'type': element.get('xbrli:type'),
                'abstract': abstract
            })

        # get existing elements with same Id.
        # so we do not elements with an id that already exist.
        sql_statement_where = ','.join(f"'{ele_id}'" for ele_id in set_new_element_id)
        sql_statement = f'SELECT elementId FROM Element WHERE elementId IN ({sql_statement_where})'
        df_existing_elements = self.dl_db.sql_select(sql_statement, index_column = 'elementId')

        # iterate over the elements to be added, if they don't already exist in the DB
        # then add them
        list_print_new_element = []
        for new_ele_id in list_new_element:
            if new_ele_id["elementId"] in df_existing_elements.index:
                continue
            
            self.dl_db.var_insert('Element', [{
                "elementId": new_ele_id['elementId'], 
                'elementName': new_ele_id['elementName'], 
                'prefix': new_ele_id['prefix'],
                'source': new_ele_id['source'],
                'balance': new_ele_id['balance'],
                'periodType': new_ele_id['periodType'],
                'type': new_ele_id['type'],
                'abstract': new_ele_id['abstract']
            }], False)

            list_print_new_element.append(new_ele_id)

        self.dl_db.commit_changes()
        print('_insert_elements_to_DB_from_soup:')
        print(list_print_new_element)



    def _insert_sec_filing_data_into_mongo_db(self, filing, ticker, statement_type, path, filing_type = '10-K'):
        
        # if statement_type == 'IncomeStatement':
        #     statement = filing.get_income_statements()
        # elif statement_type == 'BalanceSheet':
        #     statement = filing.get_balance_sheets()
        # elif statement_type == 'CashFlow':
        #     statement = filing.get_cash_flows()

        statement = filing.get_filing_statement(statement_type)

        statement_date = statement.reports[0].date.date()
        dict_data = statement.reports[0].map    
        new_post = {
           'ticker': ticker,
           'date': statement_date.strftime('%Y%m%d'),
           'filingType': filing_type,
           'dataType': 'original',
           'source': path
        }
        for element_id in dict_data:
            new_post[element_id] = dict_data[element_id].value

        self.dl_db.insert_financial_record(statement_type, [new_post])


    def _insert_sec_filing_data_into_db(self, filing, ticker, statement_type, form_type = '10-K'):
        """Store data into the database from Edgar
        Gets the SEC filing from Edgar, process it using connectEdgar and this function iterates over
        the data (stored as a dictionary) to store it approriate Database table (IncomeStatement, CashFlow, BalanceSheet).
        If necessary it will add columns to the respective table, as it as a child of root element of appropriate network.
        
        TODO: 
        - currently, there are work arounds for non-gaap elements where they might be capitalised differently, they do not follow PASCAL capitalisation
        - There might be an even bigger error if there are two different elements, but in case insensitive match they look equal, e.g. StAndard == 'StAndArd'

        Args:
            ticker (str): Ticker of a company for which the data will be retrieved.
            statement_type (str): Type of statement to retrieve/process.
            form_type (str, optional): If annual or quarterly data should retrieved. Defaults to 'annual'.
        """

        if  form_type == '10-K':
            report_frequency = 'annual'
        elif form_type == '':
            report_frequency = 'quarterly'
        
        if statement_type == 'IncomeStatement':
            statement = filing.get_income_statements()
            df_statement_element = self.dl_db.df_incomeStatement_element
            root_element = 'soi-cal'

        elif statement_type == 'BalanceSheet':
            statement = filing.get_balance_sheets()
            df_statement_element = self.dl_db.df_balanceSheet_element
            root_element = 'sfp-cls-cal'

        elif statement_type == 'CashFlow':
            statement = filing.get_cash_flows()
            df_statement_element = self.dl_db.df_cashFlow_element
            root_element = 'scf-indir-cal'

        elif statement_type == 'all':
            pass
        else:
            return

        df_element = self.dl_db.df_element
        list_new_node = []
        list_new_column = []

        statement_date = statement.reports[0].date.date()
        dict_data = statement.reports[0].map

        self.validate_statement_data(statement_type, dict_data)

        sql_statement = f'INSERT INTO {statement_type} '
        sql_field = '(ticker, date, formType, dataType'
        sql_value = f'("{ticker}", "{statement_date}", "{report_frequency}", "original", '

        for filing_elementId in dict_data:
            prefix, element_name = filing_elementId.split('_')
            if prefix.lower() == ticker.lower():
                # non-gaap_CloudInfrastructure-As-A-Service - from ORCL 2014 IncomeStatement
                element_name = element_name.replace('-', '')
                elementId = 'non-gaap_' + element_name
            else:
                elementId = filing_elementId


            if elementId.lower() in set([key.lower() for key in df_statement_element.index]):
                # sql_field += df_statement_element.loc[elementId, 'elementName'] + ', '
                existing_element_name = df_statement_element.loc[df_statement_element.index.str.lower() == elementId.lower(), 'elementName'][0]
                sql_field += existing_element_name + ', '

            # elif elementId in df_element.index:
            elif prefix != 'us-gaap' and ('us-gaap_' + element_name).lower() in set([key.lower() for key in df_statement_element.index]):
                # Peculiar case, GOOG and FB 2020 Filing, December 2019
                # statements have non-gaap element with the same name as a us-gaap element
                # PropertyPlantAndEquipmentAndFinanceLeaseRightofUseAssetAfterAccumulatedDepreciationAndAmortization
                # Need to consider if columns should have prefixes.
                elementId = 'us-gaap_' + element_name
                existing_element_name = df_statement_element.loc[df_statement_element.index.str.lower() == elementId.lower(), 'elementName'][0]
                sql_field += existing_element_name + ', '


            elif elementId.lower() in set([key.lower() for key in df_element.index]):
                # If elementId doesn't exist in the expected Network.
                # then check if the element exists in element Table.
                #
                # add the new node to the network based on which statement the element appeared in.
                # later this will have to be manually fixed, 
                #       - It's being added as a child of a root of the network
                #       - Though, it most likely the actual parent is not the root.
                #       - To fix: look at where this element appeared on the Financial Statement.
                
                existing_element_name = df_element.loc[df_element.index.str.lower() == elementId.lower(), 'elementName'][0]

                self.dl_db.var_insert('Network', [{
                    'nodeId': elementId,
                    'networkName': root_element, 
                    'parentNodeId': root_element,
                    'originalParentNodeId': None,
                    'elementId': elementId,
                    'originDescription': f'ticker: {ticker}, date: {statement_date}, statement: {statement_type}, reporting_frequency: {report_frequency}'
                }], False)

                # since Network has a new element, will have to add respective column to a respective table of network.
                list_new_column.append({'name': existing_element_name,'data_type': 'REAL'})
                
                sql_field += existing_element_name + ', '

                list_new_node.append(elementId)

            elif prefix.lower() == 'us-gaap':
                # There was a case where an element was us-gaap but it wasn't in taxonomy
                # since its a depricated element
                # us-gaap_PayablesToCustomers - from 2016

                self.dl_db.var_insert('Element', [{
                    'elementId': elementId,
                    'elementName': element_name,
                    'prefix': 'us-gaap',
                    'label': None,
                    'balance': None,
                    'deprecated': 'True' # should store the date 
                }], False)

                self.dl_db.var_insert('Network', [{
                    'nodeId': elementId,
                    'networkName': root_element, 
                    'parentNodeId': root_element,
                    'originalParentNodeId': None,
                    'elementId': elementId,
                    'originDescription': f'not in taxonomy; ticker: {ticker}, date: {statement_date}, statement: {statement_type}, reporting_frequency: {report_frequency}'
                }], False)

                # since Network has a new element, will have to add respective column to a respective table of network.
                list_new_column.append({'name': element_name, 'data_type': 'REAL'})
                
                sql_field += element_name + ', '

                print(f'{Fore.YELLOW}DEPRECATED ELEMENT: ')
                print(elementId)
                print(element_name)
                print(f'{Style.RESET_ALL}')
                list_new_node.append(elementId)


            elif prefix.lower() == ticker.lower():
            # else:
                # Elements table should have all GAAP elements, since its provided by fasb
                # Though, it doesn't have Non-GAAP elements
                # Could not find a taxonomy to import like gaap elements provided by fasb.
                
                # usually companies use their ticker as prefix for non-gaap.

                self.dl_db.var_insert('Element', [{
                    'elementId': 'non-gaap_' + element_name,
                    'elementName': element_name,
                    'prefix': 'non-gaap',
                    'label': None,
                    'balance': None
                }], False)

                self.dl_db.var_insert('Network', [{
                    'nodeId': 'non-gaap_' + element_name,
                    'networkName': root_element, 
                    'parentNodeId': root_element,
                    'originalParentNodeId': None,
                    'elementId': 'non-gaap_' + element_name,
                    'originDescription': f'ticker: {ticker}, date: {statement_date}, statement: {statement_type}, reporting_frequency: {report_frequency}'
                }], False)

                # since Network has a new element, will have to add respective column to a respective table of network.
                list_new_column.append({'name': element_name, 'data_type': 'REAL'})
                
                sql_field += element_name + ', '

                print(filing_elementId)
                print(element_name)
                list_new_node.append(elementId)
            else:
                raise Exception(f'DataloaderEdgar:edgar_into_db: the prefix for {elementId} is not ticker of the company')


            if type(dict_data[filing_elementId].value) is str:
                sql_value += f'"{dict_data[filing_elementId].value}", '
            elif dict_data[filing_elementId].value is None:
                sql_value += 'null, '
            else:
                sql_value += f'{dict_data[filing_elementId].value}, '

        sql_field = sql_field[:-2] + ')'
        sql_value = sql_value[:-2] + ')'
        sql_statement += sql_field + ' VALUES ' + sql_value + ';'

        # add the columns to the table
        print('sql_statement')
        print(sql_statement)
        print(f'{Fore.YELLOW}New Node: ', list_new_node, f'{Style.RESET_ALL}!')

        self.dl_db.schema_add_column(statement_type, list_new_column, exception_on_duplicate = False)

        try:
            self.dl_db.sql_execute_statement(sql_statement, True)
            
            if len(list_new_node) > 0:
                self.dl_db.renew_local_network_df(statement_type)
        except sqlite3.Error as err:
            print('DataloaderEdgar:_insert_sec_filing_data_into_db:failed on UNIQUE')


    def _get_df_sec_filing(self, ticker=None, cik=None, year = None, quater = None, form_type = "10-K"):
        """Retrieve the SEC filing, parse it and return it as a Filing Object

        Args:
            ticker (str): Ticker of a company to retrieve the SECR Filing for
            year (int): For which year should the filing be retrieved 
            period (str, optional): Quarterly or annual Filing. Defaults to 'annual'.

        Returns:
            [type]: [description]
        """
        sql_statement = "SELECT * FROM EdgarFilingIndex "

        sql_where = f"WHERE filingType = '{form_type}' "
        
        if year:
            date = datetime(year, 1, 1).date()
            sql_where += f"and date >= '{date}' "

        if cik:
            cik_stripped = cik.lstrip('0')
            sql_where += f"and cik = '{cik_stripped}' "
        elif ticker:
            sql_where += f"and cik = (SELECT cik FROM StockTicker WHERE ticker = '{ticker}') "
        else:
            return

        sql_statement += sql_where + "ORDER BY date ASC"        

        df_filing = self.dl_db.sql_select(sql_statement, None)
        return df_filing


    def insert_filing_index_from_edgar(self, since_year = 2010):
        path = './data/index_edgar_filing/'
        header = ['cik', 'companyName', 'filingType', 'date', 'filingFilePath', 'filingIndexPath']

        download_index(path, since_year)

        list_index_file = glob.glob(path + "*.tsv")
        list_index_file = list_index_file

        for index_file in list_index_file:
            print(index_file)
            df = pd.read_csv(index_file, sep='|', header=None, names=header)
            df.apply(lambda x: self._insert_edgar_filing(x), axis=1)
        self.dl_db.commit_changes()


    def _insert_edgar_filing(self, pd_series):
        if pd_series['filingType'] not in ['UPLOAD']: #['10-K', '8-K', '10-Q']:
            self.dl_db.var_insert('EdgarFilingIndex', [pd_series], commit=False, fail_on_unique=False)


    def get_tickers_and_cik(self):
        """Retrieves tickers from the url and saves it into StockTicker
        It will not update old or insert duplicates. It will only insert new onews.
        TODO: Update old records. 
        """
        ticker_url = "https://www.sec.gov/files/company_tickers.json"

        response= requests.get(ticker_url) 
        response_json = response.json() # maybe redundant
        df = pd.DataFrame.from_dict(response_json, orient = 'index')
        df = df.rename(columns={ 'cik_str': 'cik', 'title': 'companyName'})
        # df.iloc[:1].apply(lambda x: print(x), axis=1)
        df.apply(lambda x: self.dl_db.var_insert('StockTicker', [x], commit=False, fail_on_unique=False), axis=1)
        self.dl_db.commit_changes()