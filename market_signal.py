class MarketSignal(object):
    def __init__(self, market_signal, position = None):
        self.position = position
        self.market_signal = market_signal

    # def __str__(self):
    #     return (self.market_signal, self.position)
    #     return 'test'

    def __repr__(self):
        return str(self.market_signal) + ', ' + str(self.position)