import pandas as pd
import numpy as np
import requests
import matplotlib.pyplot as plt
from datetime import timedelta, date, datetime

#from code.market_signal import MarketSignal

#three bridges, platform 2, supervisor office
class HelperFunc:
    def __init__(self):
        self.time_series_data = None

    def sql_safe_string(self, name):
        return ''.join( chr for chr in name if (chr.isalnum() or chr is '.') )  

    def getData_sine_curve(self, fs, sample):
        f = 5
        x = np.arange(sample)
        y = np.sin(2 * np.pi * f * x / fs) + 1
        return y

    def normalise_reporting_period(self, dt, func='qtr'):
        dt = dt.date()
        normalised_date = []

        if func == 'qtr':
            normalised_date = [date(dt.year - 1, 12, 31), date(dt.year, 3, 31),
                date(dt.year, 6, 30), date(dt.year, 9, 30),
                date(dt.year, 12, 31)]
        elif func == 'ann':
            normalised_date = [date(dt.year - 1, 12, 31), date(dt.year, 12, 31)]

        return min(normalised_date, key=lambda d: abs(dt - d))
 
    def get_latest_row(self, df_data, date, field):
        #get the latest row for given data
        # sometimes there is no record for given date, so we want to get latest available row before given date.
        df_latest_rows = df_data.loc[date - timedelta(days=14):date]
        if df_latest_rows.size > 0:
            return df_latest_rows.iloc[-1]['adjusted_close']
        elif df_latest_rows[field].dtype == np.float64:
            return np.NaN
        else:
            return None
        return None



class algo:
	def __init__(self):
		self.helperFunc = HelperFunc()

	def crossover(self, df_data, short_moving_average = None, long_moving_average = None, ema = True):		#to be done, but it should be able to calculate the windows based on the provided data
		if(short_moving_average is None or long_moving_average is None):
			# tl = [9, 1000, 110000]
			# np.std(tl)/np.mean(tl)
			short_moving_average = 5
			long_moving_average = 15

		#create a new DF for signals
		df_signals = pd.DataFrame(index=df_data.index)
		df_signals['signal'] = 0.0
		#calculate the averages and store it in a new collumn
		if ema:
			df_signals['short_mavg'] = df_data['Close'].ewm(span=short_moving_average, min_periods=short_moving_average).mean()
			df_signals['long_mavg'] = df_data['Close'].ewm(span=long_moving_average, min_periods=long_moving_average).mean()
		else:
			df_signals['short_mavg'] = df_data['Close'].rolling(window=short_moving_average, min_periods=short_moving_average, center=False).mean()
			df_signals['long_mavg'] = df_data['Close'].rolling(window=long_moving_average, min_periods=long_moving_average, center=False).mean()
		#calculate the signals, create buy/sell signal if there is change in data
		df_signals['signal'][long_moving_average:] = np.where(df_signals['short_mavg'][long_moving_average:] > df_signals['long_mavg'][long_moving_average:], 1.0, 0.0 )
		df_signals['positions'] = df_signals['signal'].diff()

		return df_signals


	def auto_regression(self):
		pass


class CrossoverIndicator:
	def __init__(self, symbol, smavg, lmavg):
		self.symbol = symbol
		self.smavg = smavg
		self.lmavg = lmavg
		self.df_data = pd.DataFrame()
		self.ema = True
		
	
	def get_signals(self):
		#create a new DF for signals
		list_data_temp = self.df_data[self.symbol].dropna()
		df_signals = pd.DataFrame(index=list_data_temp.index)
		df_signals['signal'] = 0.0
		#calculate the averages and store it in a new collumn
		if self.ema:
			df_signals['short_mavg'] = list_data_temp.ewm(span = self.smavg, min_periods = self.smavg).mean()
			df_signals['long_mavg'] = list_data_temp.ewm(span=self.lmavg , min_periods=self.lmavg ).mean()
		else:
			df_signals['short_mavg'] = list_data_temp.rolling(window = self.smavg, min_periods = self.smavg, center=False).mean()
			df_signals['long_mavg'] = list_data_temp.rolling(window = self.lmavg , min_periods = self.lmavg , center=False).mean()
		#calculate the signals, create buy/sell signal if there is change in data
		df_signals['signal'][self.lmavg :] = np.where(df_signals['short_mavg'][self.lmavg :] > df_signals['long_mavg'][self.lmavg :], 1.0, 0.0 )
		df_signals['positions'] = df_signals['signal'].diff()

		return df_signals


	def set_params(self, args):
		self.symbol = args[0]


	def set_df_data(self, df):
		self.df_data = df
		




class BackTest:
	def __init__(self):
		pass

	def crossover(self, df_data, df_signals, initial_capital = float(10000), ):
		positions = pd.DataFrame(index=df_signals.index).fillna(0.0)
		positions['Position in Security'] = 1000*df_signals['signal']
		portfolio = positions.multiply(df_data['Close'], axis=0)
		pos_diff = positions.diff()
		portfolio['holdings'] = (positions.multiply(df_data['Close'], axis=0)).sum(axis=1)
		portfolio['cash'] = initial_capital - (pos_diff.multiply(df_data['Close'], axis=0)).sum(axis=1).cumsum()
		portfolio['total'] = portfolio['cash'] + portfolio['holdings']
		portfolio['returns'] = portfolio['total'].pct_change()

		del portfolio['Position in Security']
		return portfolio

	def something(self):
		pass


	def simulate_strategy(self, strategy, list_df_data, initial_capital = float(10000)):
		list_symbols = list_df_data.columns
		list_porfolios = []
		dict_portfolios = {}
		strategy.set_df_data(list_df_data)
		for symbol in list_symbols:
			strategy.set_params([symbol])
			df_signals = strategy.get_signals()

			positions = pd.DataFrame(index=df_signals.index).fillna(0.0)
			positions['Position in Security'] = 1000*df_signals['signal']
			portfolio = positions.multiply(list_df_data[symbol], axis=0)
			pos_diff = positions.diff()
			portfolio['holdings'] = (positions.multiply(list_df_data[symbol], axis=0)).sum(axis=1)
			portfolio['cash'] = initial_capital - (pos_diff.multiply(list_df_data[symbol], axis=0)).sum(axis=1).cumsum()
			portfolio['total'] = portfolio['cash'] + portfolio['holdings']
			portfolio['returns'] = portfolio['total'].pct_change()

			del portfolio['Position in Security']
			list_porfolios.append(portfolio.copy(deep=True))
			dict_portfolios[symbol] = portfolio.copy(deep=True)

		df_con = pd.concat(list_porfolios).groupby(level=0).mean()
		df_con['returns'] = df_con['total'].pct_change()
		dict_portfolios['avg'] = df_con['returns'] 
		return dict_portfolios



	def simulate_portfolio(self):
		pass

class Optimasation:
	def __init__(self):
		pass

