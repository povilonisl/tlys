import requests
import pandas as pd
import numpy as np
import sqlite3
import tempfile
import subprocess 
import sys
from datetime import timedelta, date, datetime
import re
from pandas.tseries.offsets import BDay

from helper_func import HelperFunc


class DataloaderCore:
    # DataloaderCore
    def __init__(self):
        self.dl_api = DataloaderAPI()
        self.dl_db = DataloaderDB()


    #[{val: 'IBM', opp: '=', name:'ticker'}]
    def db_get_data(self, list_identifier, db_table = 'TimeSeriesDaily', split = True):
        """
            IncomeStatement:
                SELECT * FROM IncomeStatement WHERE ticker = '{identifier}' ORDER BY datetime(date)
            CashFlow
                SELECT * FROM CashFlow WHERE ticker = '{identifier}' ORDER BY datetime(date)
            BalanceSheet
                SELECT * FROM BalanceSheet WHERE ticker = '{identifier}' ORDER BY datetime(date)
            TimeSeriesDaily
                SELECT * FROM TimeSeriesDaily WHERE ticker = '{identifier}' ORDER BY datetime(date)
            Ratios    
                SELECT * FROM RatiosQtr WHERE clusterName = '{identifier}' ORDER BY datetime(date)
                SELECT * FROM RatiosAnn WHERE clusterName = '{identifier}' ORDER BY datetime(date)
            Tickers
                SELECT * FROM Tickers
            Cluster
                SELECT * FROM Cluster WHERE clusterName = '{identifier}'
                SELECT * FROM Cluster
            ClusterMember
                SELECT * FROM ClusterMember WHERE clusterName = '{identifier}'
        """

        if split:
            pass
        else:
            str_list_identifier = ', '.join(f'"{indentifier}"' for indentifier in list_identifier)
            df_db = self.dl_db.sql_select(f"SELECT * FROM {db_table} WHERE ticker IN ({str_list_identifier}) ORDER BY datetime(date)")

        if db_table in ['IncomeStatement', 'BalanceSheet', 'CashFlow']:
            #set data types
            df_db = df_db.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
            df_temp = df_db[df_db.columns.difference(['reportedCurrency', 'reportFrequency', 'ticker'])]
            df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
            df_temp = df_temp.astype(np.float64)
            df_db[df_temp.columns] = df_temp

        # list_df = []
        # for identifier in list_identifier:
        #     if db_table in ['IncomeStatement', 'BalanceSheet', 'CashFlow']:
        #         df_db = self.dl_db.sql_select(f"SELECT * FROM {db_table} WHERE ticker = '{identifier}' ORDER BY datetime(date)")
        #         if df_db.size == 0:
        #             print(f"WARNING; db_get_data(tickers, data_type = 'TimeSeriesDaily'); DB has no data for ticker {ticker}")
        #             continue
                
        #         df_db = df_db.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
        #         df_temp = df_db[df_db.columns.difference(['reportedCurrency', 'reportFrequency', 'ticker'])]
        #         df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
        #         df_temp = df_temp.astype(np.float64)
        #         df_db[df_temp.columns] = df_temp
        #     # elif db_table in ['RatioQtr']:
        #     #     df_db = self.dl_db.sql_select(f"SELECT * FROM {db_table} WHERE clusterName = '{identifier}' ORDER BY datetime(date)")
        #     #     if df_db.size == 0:
        #     #         print(f"WARNING; db_get_data(tickers, data_type = 'TimeSeriesDaily'); DB has no data for ticker {ticker}")
        #     #         continue 

        return df_db


    def db_update_data(self):
        pass


    def get_data(self, tickers, data_type = 'TimeSeriesDaily', update_data = False):
        #not safe against SQL injection
        list_df = []

        for ticker in tickers:
            df_db = self.dl_db.sql_select(f"SELECT * FROM {data_type} WHERE ticker = '{ticker}' ORDER BY datetime(date)")
            
            if df_db.size > 0 and update_data:
                print(df_db.iloc[-1].name.date())
                df_api = self.dl_api.alphavantage(ticker, data_type)
                
                df_new = df_api.loc[df_db.index[-1].date() + timedelta(days=1):]

                df_new.apply(lambda x: self.dl_db.pandas_insert(ticker, x, data_type, False), axis=1)
                self.dl_db.commit_changes()
                df_db = pd.concat([df_db, df_new]) 
            elif df_db.size == 0 :
                df_api = self.dl_api.alphavantage(ticker, data_type)
                df_api.apply(lambda x: self.dl_db.pandas_insert(ticker, x, data_type, False), axis=1)
                self.dl_db.commit_changes()
                df_db = df_api
 
            if data_type in ['IncomeStatement', 'BalanceSheet', 'CashFlow']:
                #set data types
                df_db = df_db.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
                df_temp = df_db[df_db.columns.difference(['reportedCurrency', 'reportFrequency', 'ticker'])]
                df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
                df_temp = df_temp.astype(np.float64)
                df_db[df_temp.columns] = df_temp

            list_df.append(df_db)
        return list_df
        
    def get_income_statements(self, ticker):
        pass

    def get_cash_flow(self, ticker):
        pass

    def get_balance_sheet(self, ticker):
        pass

    def pd_sql_select(self, statement):
        return self.dl_db.sql_select(statement)

    def pd_df_insert(self, df):
        pass

    def pd_series_insert(self, df):
        pass

    def var_insert(self, table_name, fields, commit):
        self.dl_db.var_insert(table_name, fields, commit)

    def commit_changes(self):
        self.dl_db.commit_changes()

class DataloaderAPI:
    def __init__(self):
        self.api_data = {}
        pass

    def set_api_key(self, name, key):
        if(not key in self.api_data):
            self.api_data[key] = {}

        self.api_data[name]['key'] = key


    def alphavantage(self, ticker, data_type = 'TimeSeriesDaily'):
        print(f'dl_api.alphavantage - ticker:{ticker}, data_type:{data_type}')

        func = ''
        if data_type == 'TimeSeriesDaily':
            func = 'TIME_SERIES_DAILY_ADJUSTED'
        elif data_type == 'IncomeStatement':
            func = 'INCOME_STATEMENT'
        elif data_type == 'BalanceSheet':
            func = 'BALANCE_SHEET'
        elif data_type == 'CashFlow':
            func = 'CASH_FLOW'

        my_api_data = {}
        if 'alphavantage' in self.api_data:
            my_api_data = self.api_data['alphavantage']
        
        API_URL = "https://www.alphavantage.co/query" 
        api_key = 'VF8PBNPTSXB8RJLH' #VF8PBNPTSXB8RJLH
        if 'key' in my_api_data:
            api_key = my_api_data['api']
        
        # listDF = []
 
        data = { 
            "function": func, 
            "symbol": ticker,
            "outputsize" : "full",
            "datatype": "json", 
            "apikey": api_key} 

        response = requests.get(API_URL, data) 
        response_json = response.json() # maybe redundant
        
        # df.sort_index()

        if func == 'TIME_SERIES_DAILY' :
            df = pd.DataFrame.from_dict(response_json['Time Series (Daily)'], orient= 'index')
            df.index =  pd.to_datetime(df.index, format='%Y-%m-%d')
            df = df.sort_index()

            df = df.rename(columns={ '1. open': 'Open', '2. high': 'High', '3. low': 'Low', '4. close': 'Close', '5. volume': 'Volume'})
            df = df.astype({'Open': 'float64', 'High': 'float64', 'Low': 'float64','Close': 'float64','Volume': 'float64',})
            df = df[[ 'Open', 'High', 'Low', 'Close', 'Volume']]
        elif func == 'TIME_SERIES_DAILY_ADJUSTED':
            df = pd.DataFrame.from_dict(response_json['Time Series (Daily)'], orient= 'index')
            df.index =  pd.to_datetime(df.index, format='%Y-%m-%d')
            df = df.sort_index()

            df = df.rename(columns={ 
                '1. open': 'open', 
                '2. high': 'high', 
                '3. low': 'low', 
                '4. close': 'close', 
                '5. adjusted close': 'adjusted_close', 
                '6. volume': 'volume',
                '7. dividend amount': 'dividend_amount',
                '8. split coefficient': 'split_coefficient'})
            df = df.astype({
                'open': 'float64', 
                'high': 'float64', 
                'low': 'float64',
                'close': 'float64',
                'adjusted_close': 'float64',
                'volume': 'float64',
                'dividend_amount': 'float64',
                'split_coefficient': 'float64'})
            df = df[[ 'open', 'high', 'low', 'close', 'adjusted_close', 'volume', 'dividend_amount', 'split_coefficient']]
        elif func in ['INCOME_STATEMENT', 'BALANCE_SHEET', 'CASH_FLOW']:
            #get the data and put it into a df
            df_annual = pd.DataFrame(response_json['annualReports'])
            df_annual['reportFrequency'] = 'annual'
            df_quarterly = pd.DataFrame(response_json['quarterlyReports'])
            df_quarterly['reportFrequency'] = 'quarterly'
            df = pd.concat([df_annual, df_quarterly])

            #set index
            df.set_index('fiscalDateEnding', inplace=True)
            df.index =  pd.to_datetime(df.index, format='%Y-%m-%d')
            df = df.sort_index()

            #set data types
            df = df.astype({'reportedCurrency': 'string', 'reportFrequency': 'string'})
            df_temp = df[df.columns.difference(['reportedCurrency', 'reportFrequency'])]
            df_temp = df_temp.replace('None|NaN|nan', np.NaN, regex=True)
            df_temp = df_temp.astype(np.float64)
            df[df_temp.columns] = df_temp
        else:
            print('else statement')
            
            # df = df.rename(columns={ 
            #     '5. adjusted close': symbol, })
            # df = df.astype({
            #     symbol: 'float64',})
            # listDF.append(df[[symbol]])

        #result_df = pd.concat(listDF, axis=1, sort=True)
        #return result_df
        return df

    def edgar(self):
        pass


class DataloaderDB:
    def __init__(self):
        self.helpers = HelperFunc()
        self.conn = sqlite3.connect("/home/ovoido/myProjects/Projects/TradeLikeYouShould/code/local_market.db")
        #self.cur = self.conn.cursor()

    def close_db(self):
        #self.cur.close()
        self.conn.close()

    def sql_select(self, statement):
        df =  pd.read_sql_query(statement, self.conn)
        if 'date' in df.columns:
            df.date = pd.to_datetime(df.date, format='%Y-%m-%d')
            df.set_index('date', inplace=True)
            df = df.sort_index()

        
        #df = df[[ 'open', 'high', 'low', 'close', 'adjusted_close', 'volume', 'dividend_amount', 'split_coefficient']]
        return df


    def var_select(self, table_name, conditions):
        # self.cur.execute("select * from airlines limit 5;")
        # results = self.cur.fetchall()

        #df = pd.read_sql_query(statement, self.conn)
        #return df
        pass

    def sql_insert(self, statement, commit):
        #cur.execute("insert into airlines values (6048, 19846, 'Test flight', '', '', null, null, null, 'Y')")
        # values = ('Test Flight', 'Y')
        # cur.execute("insert into airlines values (6049, 19847, ?, '', '', null, null, null, ?)", values)
        #self.cur.execute(statement)

        cur = self.conn.cursor()
        cur.execute(statement)
        cur.close()

        if commit:    
            self.conn.commit()

    def var_insert(self, table_name, fields, commit):
        #cur.execute("insert into airlines values (6048, 19846, 'Test flight', '', '', null, null, null, 'Y')")
        # values = ('Test Flight', 'Y')
        # cur.execute("insert into airlines values (6049, 19847, ?, '', '', null, null, null, ?)", values)
        #self.cur.execute(statement)

        cur = self.conn.cursor()

        sql_statement = f'insert into {table_name} values ('
        for field in fields:
            if type(field) is str:
                sql_statement += f'"{field}", '
            else:
                sql_statement += f'{field}, '
        sql_statement = sql_statement[:-2] + ')'

        cur.execute(sql_statement)
        cur.close()

        if commit:    
            self.conn.commit()

    def pandas_insert(self, ticker, df_row, table, commit = False, or_replace = False):
        #cur.execute("insert into airlines values (6048, 19846, 'Test flight', '', '', null, null, null, 'Y')")
        # values = ('Test Flight', 'Y')
        # cur.execute("insert into airlines values (6049, 19847, ?, '', '', null, null, null, ?)", values)
        cur = self.conn.cursor()

        sql_statement = ''
        if or_replace:
            sql_statement += 'INSERT OR REPLACE '
        else:
            sql_statement += 'INSERT '
        sql_statement += f'INTO {table} '

        temp_date = df_row.name.strftime('%Y-%m-%d')
        sql_fields = f'(ticker, date, '
        sql_data = f'("{ticker}", "{temp_date}", '

        list_column_names = list(df_row.index)
        for colName in list_column_names:
            sql_fields += f'{colName}, '
            if type(df_row[colName]) == str or np.isnan(df_row[colName]):
                sql_data += f'"{df_row[colName]}", '
            else:
                sql_data += f'{df_row[colName]}, '
        sql_fields = sql_fields[:-2] + ')' 
        sql_data = sql_data[:-2] + ')' 

        sql_statement += f'{sql_fields} VALUES {sql_data}'
        cur.execute(sql_statement)
        cur.close()

        if commit:
            self.conn.commit()
        
    def commit_changes(self):
        self.conn.commit()

    def update(self, statement):
        # values = ('USA', 19847)
        # cur.execute("update airlines set country=? where id=?", values)     
        # self.conn.commit()
        pass

    def delete(self, statement):
        # need to delete Ticker 'I' from TimeSeriesPeriod
        # cur = conn.cursor()
        # values = (19847, )
        # ur.execute("delete from airlines where id=?", values)conn.commit()
        pass

    def sql_create_table(self, table_name, field_names, field_types):
        # cur.execute("create table daily_flights (id integer, departure date, arrival date, number text, route_id integer)")conn.commit()
        cur = self.conn.cursor()
 
        table_name = self.helpers.sql_safe_string(table_name)
        str_fields = '('
        for field_name, field_type in zip(field_names, field_types):
            str_fields += (
                field_name + 
                ' ' + 
                field_type + 
                ', ')
        str_fields = str_fields[:-2] + ')'
        #(id integer, departure date, arrival date, number text, route_id integer)"

        sql_statement = f"create table {table_name} {str_fields};"
        cur.execute(sql_statement)
        self.conn.commit()
 
        cur.close()
    
    def pandas_create_table(self, df):
        # df.to_sql("daily_flights", conn, if_exists="replace")
        pass

    def sql_alter_table(self, df):
        # cur.execute("alter table airlines add column airplanes integer;")
        # no need to commit alterations
        pass

    def pandas_alter_table(self, df):
        # df = pd.read_sql("select * from daily_flights", conn)
        # df["delay_minutes"] = None
        # df.to_sql("daily_flights", conn, if_exists="replace")
        pass


    def insert_or_replace(self, row):
        # code
        
        pass

class DataloaderFile:

    def __init__(self):
        self.dl_db = DataloaderDB()

    def export_to_csv(self, df, file_name, exe_path="/usr/bin/libreoffice"):
        """Open dataframe df in excel.

        exe_path - path to the program that will open the csv after it has been saved
        index=True - export the index of the dataframe as the first columns
        tmp_path    - directory to save the file in


        This creates a temporary file name, exports the dataframe to a csv of that file name,
        and then tells excel to open the file (in read only mode). (It uses df.to_csv instead
        of to_excel because if you don't have excel, you still get the csv.)

        Note - this does NOT delete the file when you exit. 
        """

        # df.to_csv(tmp_name, index=index)
        # cmd=[excel_path, '/r', '/e', tmp_name]
        df.to_csv(file_name + '.csv', index=True)
        # cmd=[excel_path, tmp_name]
        cmd=[exe_path, file_name + '.csv']
        
        try:
            ret_val=subprocess.Popen(cmd).pid
        except:
            print("open_in_excel(): failed to open excel")
            print("filename = ", file_name + '.csv')
            print("command line = ", cmd)
            print("Unexpected error:", sys.exc_info()[0])

        return



    def import_from_csv(self, file_name, table_name, save_to_db = False):
        df = pd.read_csv(file_name)
        if df.size > 0:
            df.set_index('date', inplace=True)
            df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
            df.sort_index(inplace=True)

            #potentially want to change this later, keep the ticker column in the table.
            # since, this creates two versions of the df
            # it currently works like this because alpha_vantage doesn't have the ticker in dictionary
            ticker = df['ticker'][0]
            df_to_import = df.drop(['ticker'], axis=1)
            
            if save_to_db:
                # ticker, df_row, table, commit = False, or_replace = False
                df_to_import.apply(lambda row: self.dl_db.pandas_insert(ticker, row, table_name, commit=False, or_replace=True), axis=1)
                self.dl_db.commit_changes()
            
            return df

