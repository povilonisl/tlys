#v 0.0.0

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import timedelta
from dataloader import DataloaderCore
from helper_func import HelperFunc
import matplotlib.pyplot as plt
import matplotlib.dates
import math


class Margay:
    def __init__(self):
        self.tickers = {}
        self.groups = {}
        self.clusters = {}
        self.dl_core = DataloaderCore() 
        self.helper = HelperFunc()

    def set_tickers(self, tickers, get_data = False, update_data = False):
        for ticker in tickers:
            if not ticker in self.tickers:
                self.tickers[ticker] = {
                    'TimeSeriesDaily': None, 
                    'IncomeStatement': None, 
                    'BalanceSheet': None, 
                    'CashFlow': None}

            if get_data:
                self.tickers[ticker]['ticker'] = ticker
                self.tickers[ticker]['TimeSeriesDaily'] = self.dl_core.get_data([ticker], 'TimeSeriesDaily', update_data)[0]
                self.tickers[ticker]['IncomeStatement'] = self.dl_core.get_data([ticker], 'IncomeStatement', update_data)[0]
                self.tickers[ticker]['BalanceSheet'] = self.dl_core.get_data([ticker], 'BalanceSheet', update_data)[0]
                self.tickers[ticker]['CashFlow'] = self.dl_core.get_data([ticker], 'CashFlow', update_data)[0]
                self.tickers[ticker]['IncomeStatementAnn'] = self.tickers[ticker]['IncomeStatement'].loc[self.tickers[ticker]['IncomeStatement']['reportFrequency'] == 'annual']
                self.tickers[ticker]['IncomeStatementQtr'] = self.tickers[ticker]['IncomeStatement'].loc[self.tickers[ticker]['IncomeStatement']['reportFrequency'] == 'quarterly']
                self.tickers[ticker]['BalanceSheetAnn'] = self.tickers[ticker]['BalanceSheet'].loc[self.tickers[ticker]['BalanceSheet']['reportFrequency'] == 'annual']
                self.tickers[ticker]['BalanceSheetQtr'] = self.tickers[ticker]['BalanceSheet'].loc[self.tickers[ticker]['BalanceSheet']['reportFrequency'] == 'quarterly']
                self.tickers[ticker]['CashFlowAnn'] = self.tickers[ticker]['CashFlow'].loc[self.tickers[ticker]['CashFlow']['reportFrequency'] == 'annual']
                self.tickers[ticker]['CashFlowQtr'] = self.tickers[ticker]['CashFlow'].loc[self.tickers[ticker]['CashFlow']['reportFrequency'] == 'quarterly']
   


    def ticker_update_ratios(self, ticker):
        if not ticker  in self.tickers:
            print(f"ERROR  - {ticker} and its corresponding data has not been retrieved")
            return
        
        # print(f'margay.ticker_update_ratios - ticker:{ticker}')


        #annnual and quarterly are split up due to possible index clashing
        #and dual index is not used due to probable lack of support
        df_income_ann = self.tickers[ticker]['IncomeStatementAnn']
        df_income_qtr = self.tickers[ticker]['IncomeStatementQtr']
        df_balance_ann = self.tickers[ticker]['BalanceSheetAnn']
        df_balance_qtr = self.tickers[ticker]['BalanceSheetQtr']
        df_market = self.tickers[ticker]['TimeSeriesDaily']

        df_ratios_qtr = pd.DataFrame(index = df_income_qtr.index)
        df_ratios_ann = pd.DataFrame(index = df_balance_ann.index)
        df_ratios_qtr.index = pd.to_datetime(df_ratios_qtr.index, format='%Y-%m-%d')
        df_ratios_ann.index = pd.to_datetime(df_ratios_ann.index, format='%Y-%m-%d')


        df_market['return'] = (df_market['adjusted_close'] - df_market['adjusted_close'].shift(1)).div(df_market['adjusted_close'].shift(1))
        df_market['risk_free_return'] = 0
        df_market['excess_return'] = df_market['return'] - df_market['risk_free_return']
        #df_market['sharpe'] = df_market['excess_return'].expanding().mean().div(df_market['excess_return'].expanding().std()) #1 = good, 2 = very good, 3 = excellent....

        df_income_qtr['ttm_netIncome'] = df_income_qtr['netIncome'].rolling(window=4, min_periods=4).sum()
        df_income_qtr['ttm_ebit'] = df_income_qtr['ebit'].rolling(4).sum()
        df_income_qtr['my_ebit'] = df_income_qtr['totalRevenue'] - df_income_qtr['costOfRevenue'] - df_income_qtr['grossProfit'] + df_income_qtr['operatingIncome']
        df_income_qtr['ttm_my_ebit'] = df_income_qtr['my_ebit'].rolling(4).sum()
        df_income_qtr['ttm_revenue'] = df_income_qtr['totalRevenue'].rolling(4).sum()

        df_ratios_qtr['marketPrice'] = np.NaN
        df_ratios_qtr['marketPrice'] = df_ratios_qtr.apply(lambda x: self.helper.get_latest_row(df_market, x.name.date(), 'adjusted_close'), axis=1)
        df_ratios_qtr['ttm_eps'] = df_income_qtr['ttm_netIncome'].div(df_balance_qtr['commonStockSharesOutstanding'], axis=0)
        df_ratios_qtr['ttm_rps'] = df_income_qtr['ttm_revenue'].div(df_balance_qtr['commonStockSharesOutstanding'], axis=0)
        df_ratios_qtr['ttm_p/e'] = df_ratios_qtr['marketPrice'].div(df_ratios_qtr['ttm_eps'], axis=0)
        df_ratios_qtr['ttm_p/rps'] = df_ratios_qtr['marketPrice'].div(df_ratios_qtr['ttm_rps'], axis=0)
        df_ratios_qtr['bv'] = df_balance_qtr['totalShareholderEquity'].div(df_balance_qtr['commonStockSharesOutstanding'], axis=0)
        df_ratios_qtr['p/bv'] = df_ratios_qtr['marketPrice'].div(df_ratios_qtr['bv'], axis=0)
        #needs to be ttm, need to calculate ttm ebit
        df_ratios_qtr['roce'] = df_income_qtr['ttm_my_ebit'].div(df_balance_qtr['totalAssets'] - df_balance_qtr['totalCurrentLiabilities'])

        # sharpe ratio
        # df_ratios_qtr['curr_date'] = df_ratios_qtr.index
        # df_ratios_qtr['prev_date'] = df_ratios_qtr.index
        # df_ratios_qtr['prev_date'] = df_ratios_qtr['prev_date'].shift(1)
        # df_ratios_qtr['sharpe'] = df_ratios_qtr.apply(lambda x: calc_sharpe_ratio(df_market['excess_return'].loc[x['prev_date'] : x.name]),axis=1)
        
        df_ratios_ann['marketPrice'] = np.NaN
        df_ratios_ann['marketPrice'] = df_ratios_ann.apply(lambda x: self.helper.get_latest_row(df_market, x.name.date(), 'adjusted_close'), axis=1)
        df_ratios_ann['eps'] = df_income_ann['netIncome'].div(df_balance_ann['commonStockSharesOutstanding'], axis=0)
        df_ratios_ann['rps'] = df_income_ann['totalRevenue'].div(df_balance_ann['commonStockSharesOutstanding'], axis=0)
        df_ratios_ann['p/e'] = df_ratios_ann['marketPrice'].div(df_ratios_ann['eps'], axis=0)
        df_ratios_ann['p/rps'] = df_ratios_ann['marketPrice'].div(df_ratios_ann['rps'], axis=0)
        df_ratios_ann['bv'] = df_balance_ann['totalShareholderEquity'].div(df_balance_ann['commonStockSharesOutstanding'], axis=0)
        df_ratios_ann['p/bv'] = df_ratios_ann['marketPrice'].div(df_ratios_ann['bv'], axis=0)
        df_ratios_ann['roce'] = df_income_ann['ebit'].div(df_balance_ann['totalAssets'] - df_balance_ann['totalCurrentLiabilities']) #NOTE: it's not using my ebit, but the one given by data source instead


        #Another way to calculate EBIT, not sure which one is better
        # df_income['my_ebit'] = df_income['netIncome'] + df_income['interestExpense'] + df_income['incomeTaxExpense']
        # df_income['ttm_my_ebit'] = df_income.loc[df_income['reportFrequency'] == 'quarterly']['my_ebit'].rolling(4).sum()

        # print('----------------EBIT Test-------------------')
        # print(df_income[['reportFrequency', 'ebit', 'ttm_ebit', 'my_ebit', 'ttm_my_ebit', 'my_ebit_2', 'ttm_my_ebit_2']])
        # print('---------------------------------------------------')

        self.tickers[ticker]['RatiosAnn'] = df_ratios_ann
        self.tickers[ticker]['RatiosQtr'] = df_ratios_qtr


    def group_add_tickers(self, group, tickers):
        if not group in self.groups:
            self.groups[group] = {'tickers': []}
        
        for ticker in tickers:
            if not ticker in self.tickers:
                self.set_tickers([ticker])
                self.ticker_update_ratios(ticker)

        self.groups[group]['tickers'] = self.groups[group]['tickers'] + tickers 
        self.group_update_data(group)
    

    def calc_sharpe_ratio(self, df):
        return (df.mean()/df.std())*(df.size**0.5)


    def group_remove_tickers(self, group, tickers):
        if not group in self.groups or len(self.groups[group]) == 0:
            self.groups[group] = []
            return

        self.groups[group] = self.groups[group] + tickers 
        self.group_update_data(group)



    def group_update_data(self, group):
        list_df_qtr = []
        list_df_ann = []

        #normalise the dates and put the df into a list
        for ticker in self.groups[group]['tickers']:
            df_ratios_ann= self.tickers[ticker]['ratios_ann']
            df_ratios_ann.index = df_ratios_ann.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'ann'), axis = 1)
            list_df_ann.append(df_ratios_ann)

            df_ratios_qtr = self.tickers[ticker]['ratios_qtr']
            df_ratios_qtr.index = df_ratios_qtr.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'qtr'), axis = 1)
            list_df_qtr.append(df_ratios_qtr)


        #list_df_ann = [self.tickers[x]['ratios_ann'] for x in self.groups[group]]
        #list_df_qtr = [self.tickers[x]['ratios_qtr'] for x in self.groups[group]]

        #calculate averages - maybe it should be medians
        df_ann_avg = pd.concat(list_df_ann)
        df_qtr_avg = pd.concat(list_df_qtr)
        df_ann_mean = df_ann_avg.groupby(df_ann_avg.index).mean()
        df_qtr_mean = df_qtr_avg.groupby(df_qtr_avg.index).mean()
        
        print('-------------------Avg Ann DF----------------------')
        print(df_ann_mean)
        print('---------------------------------------------------')
        print('-------------------Avg Qtr DF----------------------')
        print(df_qtr_mean)
        print('---------------------------------------------------')

        self.groups[group]['ratios_ann_mean'] = df_ann_mean
        self.groups[group]['ratios_qtr_mean'] = df_qtr_mean
        


    def cluster_get_v1(self, cluster_name):
        if not cluster_name:
            return

        df_cluster_name = self.dl_core.pd_sql_select(f"SELECT * FROM Cluster WHERE clusterName='{cluster_name}'")
        if df_cluster_name.size == 0:
            print(f"ERROR; get_cluster(cluster_name); Cluster {cluster_name} does not exist")
            return

        df_cluster_members = self.dl_core.pd_sql_select(f"SELECT ticker FROM ClusterMember WHERE clusterName='{cluster_name}'")
        
        if df_cluster_members.size == 0:
            print(f"ERROR; get_cluster(cluster_name); Cluster {cluster_name} does not have any members")
            return

        list_tickers = df_cluster_members['ticker'].tolist()

        df_all_TimeSeriesDaily = self.dl_core.db_get_data(list_tickers, 'TimeSeriesDaily', False)
        df_all_IncomeStatement = self.dl_core.db_get_data(list_tickers, 'IncomeStatement', False)
        df_all_BalanceSheet = self.dl_core.db_get_data(list_tickers, 'BalanceSheet', False)
        df_all_CashFlow = self.dl_core.db_get_data(list_tickers, 'CashFlow', False)

        df_all_IncomeStatementAnn = df_all_IncomeStatement.loc[df_all_IncomeStatement['reportFrequency'] == 'annual']
        df_all_IncomeStatementQtr = df_all_IncomeStatement.loc[df_all_IncomeStatement['reportFrequency'] == 'quarterly']
        df_all_BalanceSheetAnn = df_all_BalanceSheet.loc[df_all_BalanceSheet['reportFrequency'] == 'annual']
        df_all_BalanceSheetQtr = df_all_BalanceSheet.loc[df_all_BalanceSheet['reportFrequency'] == 'quarterly']
        df_all_CashFlowAnn = df_all_CashFlow.loc[df_all_CashFlow['reportFrequency'] == 'annual']
        df_all_CashFlowQtr = df_all_CashFlow.loc[df_all_CashFlow['reportFrequency'] == 'quarterly']

        list_df_ann = []
        list_df_qtr = []

        for ticker in list_tickers:
            self.tickers[ticker] = {
                'ticker': ticker,
                'TimeSeriesDaily': df_all_TimeSeriesDaily.loc[df_all_TimeSeriesDaily['ticker'] == ticker],
                'IncomeStatement': None,
                'BalanceSheet': None,
                'CashFlow': None,
                'IncomeStatementAnn': df_all_IncomeStatementAnn.loc[df_all_IncomeStatementAnn['ticker'] == ticker],
                'IncomeStatementQtr': df_all_IncomeStatementQtr.loc[df_all_IncomeStatementQtr['ticker'] == ticker],
                'BalanceSheetAnn': df_all_BalanceSheetAnn.loc[df_all_BalanceSheetAnn['ticker'] == ticker],
                'BalanceSheetQtr': df_all_BalanceSheetQtr.loc[df_all_BalanceSheetQtr['ticker'] == ticker],
                'CashFlowAnn': df_all_CashFlowAnn.loc[df_all_CashFlowAnn['ticker'] == ticker],
                'CashFlowQtr': df_all_CashFlowQtr.loc[df_all_CashFlowQtr['ticker'] == ticker]
            }
            self.ticker_update_ratios(ticker)

            df_ratios_ann = self.tickers[ticker]['RatiosAnn'].copy()
            df_ratios_ann.index = df_ratios_ann.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'ann'), axis = 1)
            list_df_ann.append(df_ratios_ann)

            df_ratios_qtr = self.tickers[ticker]['RatiosQtr'].copy()
            df_ratios_qtr.index = df_ratios_qtr.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'qtr'), axis = 1)
            list_df_qtr.append(df_ratios_qtr)

        df_ann_avg = pd.concat(list_df_ann)
        df_qtr_avg = pd.concat(list_df_qtr)
        df_ann_mean = df_ann_avg.groupby(df_ann_avg.index).mean()
        df_qtr_mean = df_qtr_avg.groupby(df_qtr_avg.index).mean()

        self.clusters[cluster_name] = {
            'clusterName': cluster_name,
            'clusterMembers': list_tickers,
            'RatiosAnnMean': df_ann_mean,
            'RatiosQtrMean': df_qtr_mean
        }


    def cluster_get(self, cluster_name, store_tickers=False):
        if not cluster_name:
            return

        df_cluster_name = self.dl_core.pd_sql_select(f"SELECT * FROM Cluster WHERE clusterName='{cluster_name}'")
        if df_cluster_name.size == 0:
            print(f"ERROR; get_cluster(cluster_name); Cluster {cluster_name} does not exist")
            return

        df_cluster_members = self.dl_core.pd_sql_select(f"SELECT ticker FROM ClusterMember WHERE clusterName='{cluster_name}'")
        
        if df_cluster_members.size == 0:
            print(f"ERROR; get_cluster(cluster_name); Cluster {cluster_name} does not have any members")
            return

        list_tickers = df_cluster_members['ticker'].tolist()

        df_cluster_TimeSeriesDaily = pd.concat(self.dl_core.get_data(list_tickers, 'TimeSeriesDaily', False))
        df_cluster_IncomeStatement = pd.concat(self.dl_core.get_data(list_tickers, 'IncomeStatement', False))
        df_cluster_BalanceSheet = pd.concat(self.dl_core.get_data(list_tickers, 'BalanceSheet', False))
        df_cluster_CashFlow = pd.concat(self.dl_core.get_data(list_tickers, 'CashFlow', False))

        #split quaterly and annual data.
        df_cluster_IncomeStatementAnn = df_cluster_IncomeStatement.loc[df_cluster_IncomeStatement['reportFrequency'] == 'annual']
        df_cluster_IncomeStatementQtr = df_cluster_IncomeStatement.loc[df_cluster_IncomeStatement['reportFrequency'] == 'quarterly']
        df_cluster_BalanceSheetAnn = df_cluster_BalanceSheet.loc[df_cluster_BalanceSheet['reportFrequency'] == 'annual']
        df_cluster_BalanceSheetQtr = df_cluster_BalanceSheet.loc[df_cluster_BalanceSheet['reportFrequency'] == 'quarterly']
        df_cluster_CashFlowAnn = df_cluster_CashFlow.loc[df_cluster_CashFlow['reportFrequency'] == 'annual']
        df_cluster_CashFlowQtr = df_cluster_CashFlow.loc[df_cluster_CashFlow['reportFrequency'] == 'quarterly']


        list_df_ratios = []


        #normalise the dates and put the df into a list
        df_cluster_IncomeStatementAnn.index = df_cluster_IncomeStatementAnn.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'ann'), axis = 1)
        df_cluster_IncomeStatementQtr.index = df_cluster_IncomeStatementQtr.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'qtr'), axis = 1)
        df_cluster_BalanceSheetAnn.index = df_cluster_BalanceSheetAnn.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'ann'), axis = 1)
        df_cluster_BalanceSheetQtr.index = df_cluster_BalanceSheetQtr.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'qtr'), axis = 1)
        df_cluster_CashFlowAnn.index = df_cluster_CashFlowAnn.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'ann'), axis = 1)
        df_cluster_CashFlowQtr.index = df_cluster_CashFlowQtr.apply(lambda x: self.helper.normalise_reporting_period(x.name, 'qtr'), axis = 1)

        print('------------------------------ test -------------------------------')
        print(df_cluster_TimeSeriesDaily)
        print('-------------------------------------------------------------------')
        #get mean of the data.
        df_cluster_IncomeStatementAnn = df_cluster_IncomeStatementAnn.groupby(df_cluster_IncomeStatementAnn.index).mean()
        df_cluster_IncomeStatementQtr = df_cluster_IncomeStatementQtr.groupby(df_cluster_IncomeStatementQtr.index).mean()
        df_cluster_BalanceSheetAnn = df_cluster_BalanceSheetAnn.groupby(df_cluster_BalanceSheetAnn.index).mean()
        df_cluster_BalanceSheetQtr = df_cluster_BalanceSheetQtr.groupby(df_cluster_BalanceSheetQtr.index).mean()
        df_cluster_CashFlowAnn = df_cluster_CashFlowAnn.groupby(df_cluster_CashFlowAnn.index).mean()
        df_cluster_CashFlowQtr = df_cluster_CashFlowQtr.groupby(df_cluster_CashFlowQtr.index).mean()
        df_cluster_TimeSeriesDaily = df_cluster_TimeSeriesDaily.groupby(df_cluster_TimeSeriesDaily.index).mean()

        self.tickers[cluster_name] = {
            'cluster_name': cluster_name,
            'TimeSeriesDaily': df_cluster_TimeSeriesDaily,
            'IncomeStatementAnn': df_cluster_IncomeStatementAnn,
            'IncomeStatementQtr': df_cluster_IncomeStatementQtr,
            'BalanceSheetAnn': df_cluster_BalanceSheetAnn,
            'BalanceSheetQtr': df_cluster_BalanceSheetQtr,
            'CashFlowAnn': df_cluster_CashFlowAnn,
            'CashFlowQtr': df_cluster_CashFlowQtr
            }


    def cluster_create(self, cluster_name, list_tickers):
        df_cluster_name = self.dl_core.pd_sql_select(f"SELECT * FROM Cluster WHERE clusterName='{cluster_name}'")
        if df_cluster_name.size > 0:
            print(f"ERROR; cluster_create(cluster_name, list_tickers); Cluster {cluster_name} already exist")
            return
        self.dl_core.var_insert('Cluster', [cluster_name, ''], commit = False)

        for ticker in list_tickers:
            self.dl_core.var_insert('ClusterMember', [cluster_name, ticker], commit = False)

        self.dl_core.commit_changes()


    def cluster_add(self, cluster_name, list_tickers):
        df_cluster_name = self.dl_core.pd_sql_select(f"SELECT * FROM Cluster WHERE clusterName='{cluster_name}'")
        if df_cluster_name.size == 0:
            print(f"ERROR; cluster_create(cluster_name, list_tickers); Cluster {cluster_name} doesn't exist")
            return

        for ticker in list_tickers:
            self.dl_core.var_insert('ClusterMember', [cluster_name, ticker], commit = False)

        self.dl_core.commit_changes()

    def cluster_update_data(self):
        pass




    def plot_ratios(self, list_clusters, list_tickers, table, list_field_names):
        # fig = plt.figure(figsize=(10, 6))
        fig = plt.figure(figsize=(10, 6))
        fig.subplots_adjust(hspace=0, bottom= 0.15, top = 1, left=0.09, right=0.98)
        plt.rcParams.update({'font.size': 7})
        #len_tickers = len(self.groups[group_name]['tickers']) + 1
        len_field_names = len(list_field_names) 

        dict_axes = {}

        formatter = matplotlib.dates.DateFormatter("%Y-%m-%d")
        locator = matplotlib.dates.MonthLocator(interval=3)
        
        for idx_field in range(1, len_field_names+1):
            field_name = list_field_names[idx_field - 1]

            if idx_field > 1:
                dict_axes[f'axes{len_field_names}1{idx_field}'] = fig.add_subplot(
                    f'{len_field_names}1{idx_field}', 
                    sharex = dict_axes[f'axes{len_field_names}11'])
            else:
                dict_axes[f'axes{len_field_names}1{idx_field}'] = fig.add_subplot(f'{len_field_names}1{idx_field}') 

            lns = []
            for clusterName in list_clusters:
                df_cluster = self.clusters[clusterName][table + 'Mean']
                p1, = dict_axes[f'axes{len_field_names}1{idx_field}'].plot(df_cluster[field_name], label=clusterName)
                lns.append(p1)

            for tickerName in list_tickers:
                df_ticker = self.tickers[tickerName][table]
                p1, = dict_axes[f'axes{len_field_names}1{idx_field}'].plot(df_ticker[field_name], label=tickerName)
                lns.append(p1)

            dict_axes[f'axes{len_field_names}1{idx_field}'].grid()
            dict_axes[f'axes{len_field_names}1{idx_field}'].set_ylabel(field_name)

            if idx_field < len_field_names:
                pass
                #dict_axes[f'axes{len_field_names}1{idx_field}'].xaxis.set_visible(False)
            else:
                dict_axes[f'axes{len_field_names}1{idx_field}'].legend(handles=lns, loc='best')

                dict_axes[f'axes{len_field_names}1{idx_field}'].xaxis.set_major_formatter(formatter)
                dict_axes[f'axes{len_field_names}1{idx_field}'].xaxis.set_major_locator(locator)
                dict_axes[f'axes{len_field_names}1{idx_field}'].xaxis.set_tick_params(rotation=40)
        
        plt.show()

    def plot_ticker(self, ticker, table_name, list_field_names):
        fig = plt.figure(figsize=(10, 6))
        fig.subplots_adjust(hspace = 0, bottom = 0.15, top = 1, left=0.09, right=0.98)
        plt.rcParams.update({'font.size': 7})

        ax1 = fig.add_subplot(f'111')
        lns = [] 
        for field in list_field_names:
            p1, = ax1.plot(self.tickers[ticker][table_name][field] , label=field)
            lns.append(p1)
        ax1.grid()

        formatter = matplotlib.dates.DateFormatter("%Y-%m-%d")
        locator = matplotlib.dates.MonthLocator(interval=3)

        ax1.legend(handles=lns, loc='best')
        ax1.xaxis.set_major_formatter(formatter)
        ax1.xaxis.set_major_locator(locator)
        ax1.xaxis.set_tick_params(rotation=40)
        plt.show()

    def ticker_simple_analysis(self, ticker):
        """
            Quick Ratio
            The current ratio measures your liquidity—how easily your current assets 
            can be converted to cash in order to cover your short-term liabilities. 
            The higher the ratio, the more liquid your assets.        
        """
        df_quickRatio = self.tickers['AYX']['BalanceSheetQtr']['totalCurrentAssets']/self.tickers['AYX']['BalanceSheetQtr']['totalCurrentLiabilities']
        tempMessage = ''
        if df_quickRatio.iloc[-1] < 1:
            tempMessage = 'dangerous'
        elif df_quickRatio.iloc[-1] < 2:
            tempMessage = 'worrying'
        print(f'CurrentRatio: {df_quickRatio.iloc[-1]} - {tempMessage}')

